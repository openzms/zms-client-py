from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.radio_port import RadioPort


T = TypeVar("T", bound="GrantRadioPort")


@_attrs_define
class GrantRadioPort:
    """A transmitter associated with a spectrum grant.

    Attributes:
        id (Union[Unset, str]): The id of the grant radio port.
        grant_id (Union[Unset, str]): The id of the spectrum grant.
        radio_port_id (Union[Unset, str]): The radio port id associated with the grant.
        radio_port (Union[Unset, RadioPort]): Defines a transmitter, receiver, or transceiver by binding an antenna to
            an RF connector on a radio, and describing its location.
    """

    id: Union[Unset, str] = UNSET
    grant_id: Union[Unset, str] = UNSET
    radio_port_id: Union[Unset, str] = UNSET
    radio_port: Union[Unset, "RadioPort"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "grant_id": {"put": True, "post": True},
        "radio_port_id": {"put": True, "post": None},
        "radio_port": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        grant_id = self.grant_id

        radio_port_id = self.radio_port_id

        radio_port: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.radio_port, Unset):
            radio_port = self.radio_port.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if radio_port_id is not UNSET:
            field_dict["radio_port_id"] = radio_port_id
        if radio_port is not UNSET:
            field_dict["radio_port"] = radio_port

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.radio_port import RadioPort

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        grant_id = d.pop("grant_id", UNSET)

        radio_port_id = d.pop("radio_port_id", UNSET)

        _radio_port = d.pop("radio_port", UNSET)
        radio_port: Union[Unset, RadioPort]
        if isinstance(_radio_port, Unset):
            radio_port = UNSET
        else:
            radio_port = RadioPort.from_dict(_radio_port)

        grant_radio_port = cls(
            id=id,
            grant_id=grant_id,
            radio_port_id=radio_port_id,
            radio_port=radio_port,
        )

        grant_radio_port.additional_properties = d
        return grant_radio_port

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
