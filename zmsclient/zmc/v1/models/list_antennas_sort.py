from enum import Enum


class ListAntennasSort(str, Enum):
    BEAM_WIDTH = "beam_width"
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    ELEMENT_ID = "element_id"
    ENABLED = "enabled"
    GAIN = "gain"
    GAIN_PROFILE_FORMAT = "gain_profile_format"
    GAIN_PROFILE_URL = "gain_profile_url"
    MODEL = "model"
    NAME = "name"
    TYPE = "type"
    UPDATED_AT = "updated_at"
    VENDOR = "vendor"

    def __str__(self) -> str:
        return str(self.value)
