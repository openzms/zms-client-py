import datetime
from typing import (
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Version")


@_attrs_define
class Version:
    """
    Attributes:
        major (int):
        minor (int):  Example: 1.
        patch (int):
        version (str):  Example: 0.1.0.
        branch (Union[None, Unset, str]):  Example: master.
        build_timestamp (Union[None, Unset, datetime.datetime]):  Example: 2020-12-18T21:06:28.000Z.
        commit (Union[None, Unset, str]):  Example: deadbeef.
    """

    major: int
    minor: int
    patch: int
    version: str
    branch: Union[None, Unset, str] = UNSET
    build_timestamp: Union[None, Unset, datetime.datetime] = UNSET
    commit: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "branch": {"put": None, "post": None},
        "build_timestamp": {"put": None, "post": None},
        "commit": {"put": None, "post": None},
        "major": {"put": None, "post": None},
        "minor": {"put": None, "post": None},
        "patch": {"put": None, "post": None},
        "version": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        major = self.major

        minor = self.minor

        patch = self.patch

        version = self.version

        branch: Union[None, Unset, str]
        if isinstance(self.branch, Unset):
            branch = UNSET
        else:
            branch = self.branch

        build_timestamp: Union[None, Unset, str]
        if isinstance(self.build_timestamp, Unset):
            build_timestamp = UNSET
        elif isinstance(self.build_timestamp, datetime.datetime):
            build_timestamp = self.build_timestamp.isoformat()
        else:
            build_timestamp = self.build_timestamp

        commit: Union[None, Unset, str]
        if isinstance(self.commit, Unset):
            commit = UNSET
        else:
            commit = self.commit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "major": major,
                "minor": minor,
                "patch": patch,
                "version": version,
            }
        )
        if branch is not UNSET:
            field_dict["branch"] = branch
        if build_timestamp is not UNSET:
            field_dict["build_timestamp"] = build_timestamp
        if commit is not UNSET:
            field_dict["commit"] = commit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        major = d.pop("major")

        minor = d.pop("minor")

        patch = d.pop("patch")

        version = d.pop("version")

        def _parse_branch(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        branch = _parse_branch(d.pop("branch", UNSET))

        def _parse_build_timestamp(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                build_timestamp_type_0 = isoparse(data)

                return build_timestamp_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        build_timestamp = _parse_build_timestamp(d.pop("build_timestamp", UNSET))

        def _parse_commit(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        commit = _parse_commit(d.pop("commit", UNSET))

        version = cls(
            major=major,
            minor=minor,
            patch=patch,
            version=version,
            branch=branch,
            build_timestamp=build_timestamp,
            commit=commit,
        )

        version.additional_properties = d
        return version

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
