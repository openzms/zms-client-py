from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.area import Area


T = TypeVar("T", bound="Constraint")


@_attrs_define
class Constraint:
    """A spectrum constraint.

    Attributes:
        min_freq (int): The start of the frequency range (inclusive) that this grant includes (Hz).
        max_freq (int): The end of the frequency range (inclusive) that this grant includes (Hz).
        max_eirp (float): The maximum EIRP (effective isotropic radiated power) that may be used by transmitter.
        id (Union[Unset, str]): The id of the spectrum constraint.
        bandwidth (Union[None, Unset, int]): If set, a bandwidth smaller than the above min_freq/max_freq range; ZMS is
            free to choose any such bandwidth if it allows the grant.
        min_eirp (Union[None, Unset, float]): The minimum acceptable EIRP (effective isotropic radiated power) that can
            be used by transmitter in this scenario.
        exclusive (Union[Unset, bool]): True if the grant applies to the entire ZMS.
        area_id (Union[None, Unset, str]): The Area id where the granted spectrum may be used, if any.
        area (Union[None, Unset, Area]): An OGC-style polygon that encodes an area.  Supports both planar and geodetic
            systems.
    """

    min_freq: int
    max_freq: int
    max_eirp: float
    id: Union[Unset, str] = UNSET
    bandwidth: Union[None, Unset, int] = UNSET
    min_eirp: Union[None, Unset, float] = UNSET
    exclusive: Union[Unset, bool] = UNSET
    area_id: Union[None, Unset, str] = UNSET
    area: Union[None, Unset, "Area"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "min_freq": {"put": None, "post": None},
        "max_freq": {"put": None, "post": None},
        "bandwidth": {"put": None, "post": None},
        "max_eirp": {"put": None, "post": None},
        "min_eirp": {"put": None, "post": None},
        "exclusive": {"put": None, "post": None},
        "area_id": {"put": None, "post": None},
        "area": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        min_freq = self.min_freq

        max_freq = self.max_freq

        max_eirp = self.max_eirp

        id = self.id

        bandwidth: Union[None, Unset, int]
        if isinstance(self.bandwidth, Unset):
            bandwidth = UNSET
        else:
            bandwidth = self.bandwidth

        min_eirp: Union[None, Unset, float]
        if isinstance(self.min_eirp, Unset):
            min_eirp = UNSET
        else:
            min_eirp = self.min_eirp

        exclusive = self.exclusive

        area_id: Union[None, Unset, str]
        if isinstance(self.area_id, Unset):
            area_id = UNSET
        else:
            area_id = self.area_id

        area: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.area is None:
            area = None
        elif not isinstance(self.area, Unset):
            area = self.area.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "min_freq": min_freq,
                "max_freq": max_freq,
                "max_eirp": max_eirp,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if bandwidth is not UNSET:
            field_dict["bandwidth"] = bandwidth
        if min_eirp is not UNSET:
            field_dict["min_eirp"] = min_eirp
        if exclusive is not UNSET:
            field_dict["exclusive"] = exclusive
        if area_id is not UNSET:
            field_dict["area_id"] = area_id
        if area is not UNSET:
            field_dict["area"] = area

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.area import Area

        d = src_dict.copy()
        min_freq = d.pop("min_freq")

        max_freq = d.pop("max_freq")

        max_eirp = d.pop("max_eirp")

        id = d.pop("id", UNSET)

        def _parse_bandwidth(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        bandwidth = _parse_bandwidth(d.pop("bandwidth", UNSET))

        def _parse_min_eirp(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        min_eirp = _parse_min_eirp(d.pop("min_eirp", UNSET))

        exclusive = d.pop("exclusive", UNSET)

        def _parse_area_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        area_id = _parse_area_id(d.pop("area_id", UNSET))

        _area = d.pop("area", UNSET)
        area: Union[None, Unset, Area]
        if isinstance(_area, Unset):
            area = UNSET
        elif _area is None:
            area = None
        else:
            area = Area.from_dict(_area)

        constraint = cls(
            min_freq=min_freq,
            max_freq=max_freq,
            max_eirp=max_eirp,
            id=id,
            bandwidth=bandwidth,
            min_eirp=min_eirp,
            exclusive=exclusive,
            area_id=area_id,
            area=area,
        )

        constraint.additional_properties = d
        return constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
