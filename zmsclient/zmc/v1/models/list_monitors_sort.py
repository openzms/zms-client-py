from enum import Enum


class ListMonitorsSort(str, Enum):
    DELETED_AT = "deleted_at"
    ENABLED = "enabled"
    EXCLUSIVE = "exclusive"
    FORMATS = "formats"
    MONITOR = "monitor"
    TYPES = "types"

    def __str__(self) -> str:
        return str(self.value)
