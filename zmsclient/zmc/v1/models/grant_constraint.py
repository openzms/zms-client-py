from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.constraint import Constraint


T = TypeVar("T", bound="GrantConstraint")


@_attrs_define
class GrantConstraint:
    """Approved spectrum constraints for a grant.

    Attributes:
        id (Union[Unset, str]): The id of the grant constraint.
        grant_id (Union[Unset, str]): The id of the spectrum grant.
        constraint_id (Union[Unset, str]): The constraint id associated with the grant.
        constraint (Union[None, Unset, Constraint]): A spectrum constraint.
    """

    id: Union[Unset, str] = UNSET
    grant_id: Union[Unset, str] = UNSET
    constraint_id: Union[Unset, str] = UNSET
    constraint: Union[None, Unset, "Constraint"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "grant_id": {"put": True, "post": None},
        "constraint_id": {"put": True, "post": None},
        "constraint": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        grant_id = self.grant_id

        constraint_id = self.constraint_id

        constraint: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.constraint is None:
            constraint = None
        elif not isinstance(self.constraint, Unset):
            constraint = self.constraint.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if constraint_id is not UNSET:
            field_dict["constraint_id"] = constraint_id
        if constraint is not UNSET:
            field_dict["constraint"] = constraint

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.constraint import Constraint

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        grant_id = d.pop("grant_id", UNSET)

        constraint_id = d.pop("constraint_id", UNSET)

        _constraint = d.pop("constraint", UNSET)
        constraint: Union[None, Unset, Constraint]
        if isinstance(_constraint, Unset):
            constraint = UNSET
        elif _constraint is None:
            constraint = None
        else:
            constraint = Constraint.from_dict(_constraint)

        grant_constraint = cls(
            id=id,
            grant_id=grant_id,
            constraint_id=constraint_id,
            constraint=constraint,
        )

        grant_constraint.additional_properties = d
        return grant_constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
