import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.grant_op_status import GrantOpStatus
from ..models.grant_status import GrantStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.grant_constraint import GrantConstraint
    from ..models.grant_int_constraint import GrantIntConstraint
    from ..models.grant_log import GrantLog
    from ..models.grant_radio_port import GrantRadioPort
    from ..models.grant_replacement import GrantReplacement
    from ..models.grant_rt_int_constraint import GrantRtIntConstraint


T = TypeVar("T", bound="Grant")


@_attrs_define
class Grant:
    """A spectrum grant.

    Attributes:
        element_id (str): The element id associated with the grant.
        name (str): The grant name.
        description (str): The grant description.
        starts_at (datetime.datetime): Start time.
        id (Union[Unset, str]): The id of the spectrum grant.
        ext_id (Union[None, Unset, str]): An external ID associated with this grant by the requestor; opaque to OpenZMS.
        html_url (Union[None, Unset, str]): A URL associated with this grant that is meaningful to the requestor; opaque
            to OpenZMS
        priority (Union[Unset, int]): The priority of the grant.  Smaller numbers have lower priority; 0 is default
            priority.  Users may set a desired priority, but this value must be equal to or lower than the maximum allocated
            to their Element via Policy.
        allow_skip_acks (Union[Unset, bool]): If set true, the grantee is allowed to not implement the heartbeat
            protocol by sending op_status updates to OpenZMS.  This may be disallowed by policy, and may not be changed once
            the grant is submitted for scheduling.
        allow_inactive (Union[Unset, bool]): If set true, the grantee will accept an immediate grant that is `paused`.
            This is useful if the grantee exists in a primary/secondary use case, where primary (or higher-priority) use is
            expected to come and go.  In those cases, this grant may be activated and paused periodically when higher-
            priority use is not allowed.  This feature is not enabled by default.  This may be disallowed by policy, and may
            not be changed once the grant is submitted for scheduling.
        allow_conflicts (Union[Unset, bool]): If set true, the grantee will accept conflicts with other grants within
            its Element.  This feature is not enabled by default.  This may be disallowed by policy, and may not be changed
            once the grant is submitted for scheduling.
        status (Union[Unset, GrantStatus]): The status of the grant.  This field is determined by the OpenZMS grant
            controller, and cannot be set by non-administrator users.
        status_ack_by (Union[None, Unset, datetime.datetime]): The time by which the next heartbeat for this grant is
            required, if any.  When heartbeats are enforced, OpenZMS will require periodic `op_status` updates as described
            in https://gitlab.flux.utah.edu/openzms/zms-zmc/-/blob/main/docs/heartbeats.md .  This field is determined by
            the OpenZMS grant controller, and cannot be set by users.
        op_status (Union[Unset, GrantOpStatus]): The operational status of the grant, as updated by the grantee.  Other
            than the initial state (`submitted`), which is set on grant creation, it is set as part of the OpenZMS heartbeat
            protocol, described in depth at https://gitlab.flux.utah.edu/openzms/zms-zmc/-/blob/main/docs/heartbeats.md .
            Administrators may modify this field directly via grant update.
        op_status_updated_at (Union[None, Unset, datetime.datetime]): The time of the last heartbeat update, if any.
            This field is determined by the OpenZMS grant controller, and cannot be set by users.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        approved_at (Union[None, Unset, datetime.datetime]): Approval time.
        owner_approved_at (Union[None, Unset, datetime.datetime]): Spectrum-owning element approval time, if necessary.
        denied_at (Union[None, Unset, datetime.datetime]): Denial time.
        expires_at (Union[None, Unset, datetime.datetime]): Expiration time.
        revoked_at (Union[None, Unset, datetime.datetime]): Revocation time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        spectrum_id (Union[None, Unset, str]): The id of the managed spectrum range.  This may be set by the grant
            creator to force allocation within a specific spectrum range; otherwise it will be set upon approval.
        is_claim (Union[Unset, bool]): True if this grant is representing a Claim; set by system, not on grant request.
        claim_id (Union[None, Unset, str]): If is_claim, the claim id that owns this grant.
        constraints (Union[List['GrantConstraint'], None, Unset]): A list of constraints applying to this grant.
        int_constraints (Union[List['GrantIntConstraint'], None, Unset]): A list of interference constraints applying to
            this grant.
        rt_int_constraints (Union[List['GrantRtIntConstraint'], None, Unset]): A list of runtime interference
            constraints applying to this grant.
        radio_ports (Union[List['GrantRadioPort'], None, Unset]): A list of specifically granted transmitter ports.
        logs (Union[List['GrantLog'], None, Unset]): A list of grant logs, e.g. including status transitions.
        replacement (Union[None, Unset, GrantReplacement]): A grant replacement record.
        replaces (Union[None, Unset, GrantReplacement]): A grant replacement record.
    """

    element_id: str
    name: str
    description: str
    starts_at: datetime.datetime
    id: Union[Unset, str] = UNSET
    ext_id: Union[None, Unset, str] = UNSET
    html_url: Union[None, Unset, str] = UNSET
    priority: Union[Unset, int] = UNSET
    allow_skip_acks: Union[Unset, bool] = UNSET
    allow_inactive: Union[Unset, bool] = UNSET
    allow_conflicts: Union[Unset, bool] = UNSET
    status: Union[Unset, GrantStatus] = UNSET
    status_ack_by: Union[None, Unset, datetime.datetime] = UNSET
    op_status: Union[Unset, GrantOpStatus] = UNSET
    op_status_updated_at: Union[None, Unset, datetime.datetime] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    approved_at: Union[None, Unset, datetime.datetime] = UNSET
    owner_approved_at: Union[None, Unset, datetime.datetime] = UNSET
    denied_at: Union[None, Unset, datetime.datetime] = UNSET
    expires_at: Union[None, Unset, datetime.datetime] = UNSET
    revoked_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    spectrum_id: Union[None, Unset, str] = UNSET
    is_claim: Union[Unset, bool] = UNSET
    claim_id: Union[None, Unset, str] = UNSET
    constraints: Union[List["GrantConstraint"], None, Unset] = UNSET
    int_constraints: Union[List["GrantIntConstraint"], None, Unset] = UNSET
    rt_int_constraints: Union[List["GrantRtIntConstraint"], None, Unset] = UNSET
    radio_ports: Union[List["GrantRadioPort"], None, Unset] = UNSET
    logs: Union[List["GrantLog"], None, Unset] = UNSET
    replacement: Union[None, Unset, "GrantReplacement"] = UNSET
    replaces: Union[None, Unset, "GrantReplacement"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "ext_id": {"put": None, "post": None},
        "html_url": {"put": None, "post": None},
        "priority": {"put": True, "post": None},
        "allow_skip_acks": {"put": True, "post": None},
        "allow_inactive": {"put": True, "post": None},
        "allow_conflicts": {"put": True, "post": None},
        "status": {"put": True, "post": True},
        "status_ack_by": {"put": True, "post": True},
        "op_status": {"put": None, "post": True},
        "op_status_updated_at": {"put": True, "post": True},
        "creator_id": {"put": True, "post": None},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "approved_at": {"put": None, "post": True},
        "owner_approved_at": {"put": None, "post": True},
        "denied_at": {"put": True, "post": True},
        "starts_at": {"put": True, "post": None},
        "expires_at": {"put": True, "post": None},
        "revoked_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "spectrum_id": {"put": True, "post": None},
        "is_claim": {"put": True, "post": True},
        "claim_id": {"put": True, "post": True},
        "constraints": {"put": True, "post": None},
        "int_constraints": {"put": True, "post": None},
        "rt_int_constraints": {"put": True, "post": None},
        "radio_ports": {"put": True, "post": None},
        "logs": {"put": True, "post": True},
        "replacement": {"put": True, "post": None},
        "replaces": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        description = self.description

        starts_at = self.starts_at.isoformat()

        id = self.id

        ext_id: Union[None, Unset, str]
        if isinstance(self.ext_id, Unset):
            ext_id = UNSET
        else:
            ext_id = self.ext_id

        html_url: Union[None, Unset, str]
        if isinstance(self.html_url, Unset):
            html_url = UNSET
        else:
            html_url = self.html_url

        priority = self.priority

        allow_skip_acks = self.allow_skip_acks

        allow_inactive = self.allow_inactive

        allow_conflicts = self.allow_conflicts

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        status_ack_by: Union[None, Unset, str]
        if isinstance(self.status_ack_by, Unset):
            status_ack_by = UNSET
        elif isinstance(self.status_ack_by, datetime.datetime):
            status_ack_by = self.status_ack_by.isoformat()
        else:
            status_ack_by = self.status_ack_by

        op_status: Union[Unset, str] = UNSET
        if not isinstance(self.op_status, Unset):
            op_status = self.op_status.value

        op_status_updated_at: Union[None, Unset, str]
        if isinstance(self.op_status_updated_at, Unset):
            op_status_updated_at = UNSET
        elif isinstance(self.op_status_updated_at, datetime.datetime):
            op_status_updated_at = self.op_status_updated_at.isoformat()
        else:
            op_status_updated_at = self.op_status_updated_at

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        approved_at: Union[None, Unset, str]
        if isinstance(self.approved_at, Unset):
            approved_at = UNSET
        elif isinstance(self.approved_at, datetime.datetime):
            approved_at = self.approved_at.isoformat()
        else:
            approved_at = self.approved_at

        owner_approved_at: Union[None, Unset, str]
        if isinstance(self.owner_approved_at, Unset):
            owner_approved_at = UNSET
        elif isinstance(self.owner_approved_at, datetime.datetime):
            owner_approved_at = self.owner_approved_at.isoformat()
        else:
            owner_approved_at = self.owner_approved_at

        denied_at: Union[None, Unset, str]
        if isinstance(self.denied_at, Unset):
            denied_at = UNSET
        elif isinstance(self.denied_at, datetime.datetime):
            denied_at = self.denied_at.isoformat()
        else:
            denied_at = self.denied_at

        expires_at: Union[None, Unset, str]
        if isinstance(self.expires_at, Unset):
            expires_at = UNSET
        elif isinstance(self.expires_at, datetime.datetime):
            expires_at = self.expires_at.isoformat()
        else:
            expires_at = self.expires_at

        revoked_at: Union[None, Unset, str]
        if isinstance(self.revoked_at, Unset):
            revoked_at = UNSET
        elif isinstance(self.revoked_at, datetime.datetime):
            revoked_at = self.revoked_at.isoformat()
        else:
            revoked_at = self.revoked_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        spectrum_id: Union[None, Unset, str]
        if isinstance(self.spectrum_id, Unset):
            spectrum_id = UNSET
        else:
            spectrum_id = self.spectrum_id

        is_claim = self.is_claim

        claim_id: Union[None, Unset, str]
        if isinstance(self.claim_id, Unset):
            claim_id = UNSET
        else:
            claim_id = self.claim_id

        constraints: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.constraints, Unset):
            constraints = UNSET
        elif isinstance(self.constraints, list):
            constraints = []
            for constraints_type_0_item_data in self.constraints:
                constraints_type_0_item = constraints_type_0_item_data.to_dict()
                constraints.append(constraints_type_0_item)

        else:
            constraints = self.constraints

        int_constraints: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.int_constraints, Unset):
            int_constraints = UNSET
        elif isinstance(self.int_constraints, list):
            int_constraints = []
            for int_constraints_type_0_item_data in self.int_constraints:
                int_constraints_type_0_item = int_constraints_type_0_item_data.to_dict()
                int_constraints.append(int_constraints_type_0_item)

        else:
            int_constraints = self.int_constraints

        rt_int_constraints: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.rt_int_constraints, Unset):
            rt_int_constraints = UNSET
        elif isinstance(self.rt_int_constraints, list):
            rt_int_constraints = []
            for rt_int_constraints_type_0_item_data in self.rt_int_constraints:
                rt_int_constraints_type_0_item = (
                    rt_int_constraints_type_0_item_data.to_dict()
                )
                rt_int_constraints.append(rt_int_constraints_type_0_item)

        else:
            rt_int_constraints = self.rt_int_constraints

        radio_ports: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.radio_ports, Unset):
            radio_ports = UNSET
        elif isinstance(self.radio_ports, list):
            radio_ports = []
            for radio_ports_type_0_item_data in self.radio_ports:
                radio_ports_type_0_item = radio_ports_type_0_item_data.to_dict()
                radio_ports.append(radio_ports_type_0_item)

        else:
            radio_ports = self.radio_ports

        logs: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.logs, Unset):
            logs = UNSET
        elif isinstance(self.logs, list):
            logs = []
            for logs_type_0_item_data in self.logs:
                logs_type_0_item = logs_type_0_item_data.to_dict()
                logs.append(logs_type_0_item)

        else:
            logs = self.logs

        replacement: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.replacement is None:
            replacement = None
        elif not isinstance(self.replacement, Unset):
            replacement = self.replacement.to_dict()

        replaces: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.replaces is None:
            replaces = None
        elif not isinstance(self.replaces, Unset):
            replaces = self.replaces.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
                "description": description,
                "starts_at": starts_at,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if ext_id is not UNSET:
            field_dict["ext_id"] = ext_id
        if html_url is not UNSET:
            field_dict["html_url"] = html_url
        if priority is not UNSET:
            field_dict["priority"] = priority
        if allow_skip_acks is not UNSET:
            field_dict["allow_skip_acks"] = allow_skip_acks
        if allow_inactive is not UNSET:
            field_dict["allow_inactive"] = allow_inactive
        if allow_conflicts is not UNSET:
            field_dict["allow_conflicts"] = allow_conflicts
        if status is not UNSET:
            field_dict["status"] = status
        if status_ack_by is not UNSET:
            field_dict["status_ack_by"] = status_ack_by
        if op_status is not UNSET:
            field_dict["op_status"] = op_status
        if op_status_updated_at is not UNSET:
            field_dict["op_status_updated_at"] = op_status_updated_at
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if approved_at is not UNSET:
            field_dict["approved_at"] = approved_at
        if owner_approved_at is not UNSET:
            field_dict["owner_approved_at"] = owner_approved_at
        if denied_at is not UNSET:
            field_dict["denied_at"] = denied_at
        if expires_at is not UNSET:
            field_dict["expires_at"] = expires_at
        if revoked_at is not UNSET:
            field_dict["revoked_at"] = revoked_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if spectrum_id is not UNSET:
            field_dict["spectrum_id"] = spectrum_id
        if is_claim is not UNSET:
            field_dict["is_claim"] = is_claim
        if claim_id is not UNSET:
            field_dict["claim_id"] = claim_id
        if constraints is not UNSET:
            field_dict["constraints"] = constraints
        if int_constraints is not UNSET:
            field_dict["int_constraints"] = int_constraints
        if rt_int_constraints is not UNSET:
            field_dict["rt_int_constraints"] = rt_int_constraints
        if radio_ports is not UNSET:
            field_dict["radio_ports"] = radio_ports
        if logs is not UNSET:
            field_dict["logs"] = logs
        if replacement is not UNSET:
            field_dict["replacement"] = replacement
        if replaces is not UNSET:
            field_dict["replaces"] = replaces

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.grant_constraint import GrantConstraint
        from ..models.grant_int_constraint import GrantIntConstraint
        from ..models.grant_log import GrantLog
        from ..models.grant_radio_port import GrantRadioPort
        from ..models.grant_replacement import GrantReplacement
        from ..models.grant_rt_int_constraint import GrantRtIntConstraint

        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        description = d.pop("description")

        starts_at = isoparse(d.pop("starts_at"))

        id = d.pop("id", UNSET)

        def _parse_ext_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        ext_id = _parse_ext_id(d.pop("ext_id", UNSET))

        def _parse_html_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        html_url = _parse_html_url(d.pop("html_url", UNSET))

        priority = d.pop("priority", UNSET)

        allow_skip_acks = d.pop("allow_skip_acks", UNSET)

        allow_inactive = d.pop("allow_inactive", UNSET)

        allow_conflicts = d.pop("allow_conflicts", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[Unset, GrantStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = GrantStatus(_status)

        def _parse_status_ack_by(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_ack_by_type_0 = isoparse(data)

                return status_ack_by_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        status_ack_by = _parse_status_ack_by(d.pop("status_ack_by", UNSET))

        _op_status = d.pop("op_status", UNSET)
        op_status: Union[Unset, GrantOpStatus]
        if isinstance(_op_status, Unset):
            op_status = UNSET
        else:
            op_status = GrantOpStatus(_op_status)

        def _parse_op_status_updated_at(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                op_status_updated_at_type_0 = isoparse(data)

                return op_status_updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        op_status_updated_at = _parse_op_status_updated_at(
            d.pop("op_status_updated_at", UNSET)
        )

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_approved_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                approved_at_type_0 = isoparse(data)

                return approved_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        approved_at = _parse_approved_at(d.pop("approved_at", UNSET))

        def _parse_owner_approved_at(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                owner_approved_at_type_0 = isoparse(data)

                return owner_approved_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        owner_approved_at = _parse_owner_approved_at(d.pop("owner_approved_at", UNSET))

        def _parse_denied_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                denied_at_type_0 = isoparse(data)

                return denied_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        denied_at = _parse_denied_at(d.pop("denied_at", UNSET))

        def _parse_expires_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                expires_at_type_0 = isoparse(data)

                return expires_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        expires_at = _parse_expires_at(d.pop("expires_at", UNSET))

        def _parse_revoked_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                revoked_at_type_0 = isoparse(data)

                return revoked_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        revoked_at = _parse_revoked_at(d.pop("revoked_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        def _parse_spectrum_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        spectrum_id = _parse_spectrum_id(d.pop("spectrum_id", UNSET))

        is_claim = d.pop("is_claim", UNSET)

        def _parse_claim_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        claim_id = _parse_claim_id(d.pop("claim_id", UNSET))

        def _parse_constraints(
            data: object,
        ) -> Union[List["GrantConstraint"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                constraints_type_0 = []
                _constraints_type_0 = data
                for constraints_type_0_item_data in _constraints_type_0:
                    constraints_type_0_item = GrantConstraint.from_dict(
                        constraints_type_0_item_data
                    )

                    constraints_type_0.append(constraints_type_0_item)

                return constraints_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["GrantConstraint"], None, Unset], data)

        constraints = _parse_constraints(d.pop("constraints", UNSET))

        def _parse_int_constraints(
            data: object,
        ) -> Union[List["GrantIntConstraint"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                int_constraints_type_0 = []
                _int_constraints_type_0 = data
                for int_constraints_type_0_item_data in _int_constraints_type_0:
                    int_constraints_type_0_item = GrantIntConstraint.from_dict(
                        int_constraints_type_0_item_data
                    )

                    int_constraints_type_0.append(int_constraints_type_0_item)

                return int_constraints_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["GrantIntConstraint"], None, Unset], data)

        int_constraints = _parse_int_constraints(d.pop("int_constraints", UNSET))

        def _parse_rt_int_constraints(
            data: object,
        ) -> Union[List["GrantRtIntConstraint"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                rt_int_constraints_type_0 = []
                _rt_int_constraints_type_0 = data
                for rt_int_constraints_type_0_item_data in _rt_int_constraints_type_0:
                    rt_int_constraints_type_0_item = GrantRtIntConstraint.from_dict(
                        rt_int_constraints_type_0_item_data
                    )

                    rt_int_constraints_type_0.append(rt_int_constraints_type_0_item)

                return rt_int_constraints_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["GrantRtIntConstraint"], None, Unset], data)

        rt_int_constraints = _parse_rt_int_constraints(
            d.pop("rt_int_constraints", UNSET)
        )

        def _parse_radio_ports(
            data: object,
        ) -> Union[List["GrantRadioPort"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                radio_ports_type_0 = []
                _radio_ports_type_0 = data
                for radio_ports_type_0_item_data in _radio_ports_type_0:
                    radio_ports_type_0_item = GrantRadioPort.from_dict(
                        radio_ports_type_0_item_data
                    )

                    radio_ports_type_0.append(radio_ports_type_0_item)

                return radio_ports_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["GrantRadioPort"], None, Unset], data)

        radio_ports = _parse_radio_ports(d.pop("radio_ports", UNSET))

        def _parse_logs(data: object) -> Union[List["GrantLog"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                logs_type_0 = []
                _logs_type_0 = data
                for logs_type_0_item_data in _logs_type_0:
                    logs_type_0_item = GrantLog.from_dict(logs_type_0_item_data)

                    logs_type_0.append(logs_type_0_item)

                return logs_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["GrantLog"], None, Unset], data)

        logs = _parse_logs(d.pop("logs", UNSET))

        _replacement = d.pop("replacement", UNSET)
        replacement: Union[None, Unset, GrantReplacement]
        if isinstance(_replacement, Unset):
            replacement = UNSET
        elif _replacement is None:
            replacement = None
        else:
            replacement = GrantReplacement.from_dict(_replacement)

        _replaces = d.pop("replaces", UNSET)
        replaces: Union[None, Unset, GrantReplacement]
        if isinstance(_replaces, Unset):
            replaces = UNSET
        elif _replaces is None:
            replaces = None
        else:
            replaces = GrantReplacement.from_dict(_replaces)

        grant = cls(
            element_id=element_id,
            name=name,
            description=description,
            starts_at=starts_at,
            id=id,
            ext_id=ext_id,
            html_url=html_url,
            priority=priority,
            allow_skip_acks=allow_skip_acks,
            allow_inactive=allow_inactive,
            allow_conflicts=allow_conflicts,
            status=status,
            status_ack_by=status_ack_by,
            op_status=op_status,
            op_status_updated_at=op_status_updated_at,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            approved_at=approved_at,
            owner_approved_at=owner_approved_at,
            denied_at=denied_at,
            expires_at=expires_at,
            revoked_at=revoked_at,
            deleted_at=deleted_at,
            spectrum_id=spectrum_id,
            is_claim=is_claim,
            claim_id=claim_id,
            constraints=constraints,
            int_constraints=int_constraints,
            rt_int_constraints=rt_int_constraints,
            radio_ports=radio_ports,
            logs=logs,
            replacement=replacement,
            replaces=replaces,
        )

        grant.additional_properties = d
        return grant

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
