from enum import Enum


class GrantOpStatus(str, Enum):
    ACCEPTED = "accepted"
    ACTIVE = "active"
    PAUSED = "paused"
    SUBMITTED = "submitted"
    UNKNOWN = "unknown"

    def __str__(self) -> str:
        return str(self.value)
