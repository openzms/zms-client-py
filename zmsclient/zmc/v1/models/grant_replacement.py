import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="GrantReplacement")


@_attrs_define
class GrantReplacement:
    """A grant replacement record.

    Attributes:
        grant_id (str): The id of the replaced grant.
        new_grant_id (str): The id of the replacing grant.
        description (str):
        id (Union[Unset, str]): The id of the grant replacement.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        constraint_change_id (Union[None, Unset, str]): The id of the triggering constraint change request, if any.
        observation_id (Union[None, Unset, str]): The id of the associated observation, if any.
    """

    grant_id: str
    new_grant_id: str
    description: str
    id: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    constraint_change_id: Union[None, Unset, str] = UNSET
    observation_id: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "grant_id": {"put": True, "post": None},
        "new_grant_id": {"put": True, "post": None},
        "description": {"put": True, "post": None},
        "created_at": {"put": True, "post": True},
        "constraint_change_id": {"put": True, "post": True},
        "observation_id": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        grant_id = self.grant_id

        new_grant_id = self.new_grant_id

        description = self.description

        id = self.id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        constraint_change_id: Union[None, Unset, str]
        if isinstance(self.constraint_change_id, Unset):
            constraint_change_id = UNSET
        else:
            constraint_change_id = self.constraint_change_id

        observation_id: Union[None, Unset, str]
        if isinstance(self.observation_id, Unset):
            observation_id = UNSET
        else:
            observation_id = self.observation_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "grant_id": grant_id,
                "new_grant_id": new_grant_id,
                "description": description,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if constraint_change_id is not UNSET:
            field_dict["constraint_change_id"] = constraint_change_id
        if observation_id is not UNSET:
            field_dict["observation_id"] = observation_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        grant_id = d.pop("grant_id")

        new_grant_id = d.pop("new_grant_id")

        description = d.pop("description")

        id = d.pop("id", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_constraint_change_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        constraint_change_id = _parse_constraint_change_id(
            d.pop("constraint_change_id", UNSET)
        )

        def _parse_observation_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        observation_id = _parse_observation_id(d.pop("observation_id", UNSET))

        grant_replacement = cls(
            grant_id=grant_id,
            new_grant_id=new_grant_id,
            description=description,
            id=id,
            created_at=created_at,
            constraint_change_id=constraint_change_id,
            observation_id=observation_id,
        )

        grant_replacement.additional_properties = d
        return grant_replacement

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
