import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.any_object import AnyObject
    from ..models.radio_port import RadioPort


T = TypeVar("T", bound="Monitor")


@_attrs_define
class Monitor:
    """
    Attributes:
        name (str): The name of the monitor
        types (str): Comma-separated list of supported types of monitoring (e.g. inline, ambient, sweep, fixed).
            Example: inline,sweep.
        formats (str): The types of observations generated (e.g. psd-csv, raw-hdf5, etc).
        id (Union[Unset, str]): The id of the monitor.
        radio_port_id (Union[Unset, str]): The id of the associated receiver.
        monitored_radio_port_id (Union[None, Unset, str]): If the monitor is an inline (coupled) monitor, it captures a
            signal from a live transmitter.  This field should point to the associated monitored transmitter.
        element_id (Union[None, Unset, str]): The element id of the radio_port's radio, for informational purposes.
            This is not required on monitor create, since it is inferred from radio_port and its radio, but if provided,
            must match the radio_port/radio's element.
        description (Union[None, Unset, str]): A description of the monitor; optional.
        device_id (Union[None, Unset, str]): A secondary device id, if any.
        html_url (Union[None, Unset, str]): A URL associated with this monitor that is meaningful to the requestor;
            opaque to OpenZMS.
        config (Union[None, Unset, AnyObject]):
        exclusive (Union[Unset, bool]): If true, this monitor can be used by only one MonitorTask at a time.  If False,
            multiple tasks can be assigned to this monitor.
        enabled (Union[Unset, bool]): If true, this monitor can be used.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        radio_port (Union[None, Unset, RadioPort]): Defines a transmitter, receiver, or transceiver by binding an
            antenna to an RF connector on a radio, and describing its location.
        monitored_radio_port (Union[None, Unset, RadioPort]): Defines a transmitter, receiver, or transceiver by binding
            an antenna to an RF connector on a radio, and describing its location.
    """

    name: str
    types: str
    formats: str
    id: Union[Unset, str] = UNSET
    radio_port_id: Union[Unset, str] = UNSET
    monitored_radio_port_id: Union[None, Unset, str] = UNSET
    element_id: Union[None, Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    device_id: Union[None, Unset, str] = UNSET
    html_url: Union[None, Unset, str] = UNSET
    config: Union[None, Unset, "AnyObject"] = UNSET
    exclusive: Union[Unset, bool] = UNSET
    enabled: Union[Unset, bool] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    radio_port: Union[None, Unset, "RadioPort"] = UNSET
    monitored_radio_port: Union[None, Unset, "RadioPort"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "radio_port_id": {"put": True, "post": None},
        "monitored_radio_port_id": {"put": True, "post": None},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "device_id": {"put": None, "post": None},
        "html_url": {"put": None, "post": None},
        "types": {"put": None, "post": None},
        "formats": {"put": None, "post": None},
        "config": {"put": None, "post": None},
        "exclusive": {"put": None, "post": None},
        "enabled": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "radio_port": {"put": True, "post": None},
        "monitored_radio_port": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        types = self.types

        formats = self.formats

        id = self.id

        radio_port_id = self.radio_port_id

        monitored_radio_port_id: Union[None, Unset, str]
        if isinstance(self.monitored_radio_port_id, Unset):
            monitored_radio_port_id = UNSET
        else:
            monitored_radio_port_id = self.monitored_radio_port_id

        element_id: Union[None, Unset, str]
        if isinstance(self.element_id, Unset):
            element_id = UNSET
        else:
            element_id = self.element_id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        device_id: Union[None, Unset, str]
        if isinstance(self.device_id, Unset):
            device_id = UNSET
        else:
            device_id = self.device_id

        html_url: Union[None, Unset, str]
        if isinstance(self.html_url, Unset):
            html_url = UNSET
        else:
            html_url = self.html_url

        config: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.config is None:
            config = None
        elif not isinstance(self.config, Unset):
            config = self.config.to_dict()

        exclusive = self.exclusive

        enabled = self.enabled

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        radio_port: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.radio_port is None:
            radio_port = None
        elif not isinstance(self.radio_port, Unset):
            radio_port = self.radio_port.to_dict()

        monitored_radio_port: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.monitored_radio_port is None:
            monitored_radio_port = None
        elif not isinstance(self.monitored_radio_port, Unset):
            monitored_radio_port = self.monitored_radio_port.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "types": types,
                "formats": formats,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if radio_port_id is not UNSET:
            field_dict["radio_port_id"] = radio_port_id
        if monitored_radio_port_id is not UNSET:
            field_dict["monitored_radio_port_id"] = monitored_radio_port_id
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if description is not UNSET:
            field_dict["description"] = description
        if device_id is not UNSET:
            field_dict["device_id"] = device_id
        if html_url is not UNSET:
            field_dict["html_url"] = html_url
        if config is not UNSET:
            field_dict["config"] = config
        if exclusive is not UNSET:
            field_dict["exclusive"] = exclusive
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if radio_port is not UNSET:
            field_dict["radio_port"] = radio_port
        if monitored_radio_port is not UNSET:
            field_dict["monitored_radio_port"] = monitored_radio_port

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.any_object import AnyObject
        from ..models.radio_port import RadioPort

        d = src_dict.copy()
        name = d.pop("name")

        types = d.pop("types")

        formats = d.pop("formats")

        id = d.pop("id", UNSET)

        radio_port_id = d.pop("radio_port_id", UNSET)

        def _parse_monitored_radio_port_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        monitored_radio_port_id = _parse_monitored_radio_port_id(
            d.pop("monitored_radio_port_id", UNSET)
        )

        def _parse_element_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        element_id = _parse_element_id(d.pop("element_id", UNSET))

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_device_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        device_id = _parse_device_id(d.pop("device_id", UNSET))

        def _parse_html_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        html_url = _parse_html_url(d.pop("html_url", UNSET))

        _config = d.pop("config", UNSET)
        config: Union[None, Unset, AnyObject]
        if isinstance(_config, Unset):
            config = UNSET
        elif _config is None:
            config = None
        else:
            config = AnyObject.from_dict(_config)

        exclusive = d.pop("exclusive", UNSET)

        enabled = d.pop("enabled", UNSET)

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        _radio_port = d.pop("radio_port", UNSET)
        radio_port: Union[None, Unset, RadioPort]
        if isinstance(_radio_port, Unset):
            radio_port = UNSET
        elif _radio_port is None:
            radio_port = None
        else:
            radio_port = RadioPort.from_dict(_radio_port)

        _monitored_radio_port = d.pop("monitored_radio_port", UNSET)
        monitored_radio_port: Union[None, Unset, RadioPort]
        if isinstance(_monitored_radio_port, Unset):
            monitored_radio_port = UNSET
        elif _monitored_radio_port is None:
            monitored_radio_port = None
        else:
            monitored_radio_port = RadioPort.from_dict(_monitored_radio_port)

        monitor = cls(
            name=name,
            types=types,
            formats=formats,
            id=id,
            radio_port_id=radio_port_id,
            monitored_radio_port_id=monitored_radio_port_id,
            element_id=element_id,
            description=description,
            device_id=device_id,
            html_url=html_url,
            config=config,
            exclusive=exclusive,
            enabled=enabled,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            deleted_at=deleted_at,
            radio_port=radio_port,
            monitored_radio_port=monitored_radio_port,
        )

        monitor.additional_properties = d
        return monitor

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
