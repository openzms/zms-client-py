import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="EventHeader")


@_attrs_define
class EventHeader:
    """
    Attributes:
        type (int): The type of event; defined generally in https://gitlab.flux.utah.edu/openzms/zms-
            api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#zms-event-v1-EventType .
        code (int): The code of event; service-defined.
        time (datetime.datetime): The event timestamp.
        source_type (Union[Unset, int]): The type of event; defined generally in
            https://gitlab.flux.utah.edu/openzms/zms-api/-/blob/main/docs/zms/event/v1/event.md?ref_type=heads#zms-
            event-v1-EventSourceType .
        source_id (Union[Unset, str]): The ID of the source service.
        id (Union[Unset, str]): The UUID of the event.
        object_id (Union[None, Unset, str]): The UUID of the object on which the event occurred.
        user_id (Union[None, Unset, str]): The UUID of the user with with the event object is associated, if any.
        element_id (Union[None, Unset, str]): The UUID of the element with with the event object is associated, if any.
    """

    type: int
    code: int
    time: datetime.datetime
    source_type: Union[Unset, int] = UNSET
    source_id: Union[Unset, str] = UNSET
    id: Union[Unset, str] = UNSET
    object_id: Union[None, Unset, str] = UNSET
    user_id: Union[None, Unset, str] = UNSET
    element_id: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "type": {"put": True, "post": True},
        "code": {"put": True, "post": True},
        "source_type": {"put": True, "post": True},
        "source_id": {"put": True, "post": True},
        "id": {"put": True, "post": True},
        "time": {"put": True, "post": True},
        "object_id": {"put": True, "post": True},
        "user_id": {"put": True, "post": True},
        "element_id": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        type = self.type

        code = self.code

        time = self.time.isoformat()

        source_type = self.source_type

        source_id = self.source_id

        id = self.id

        object_id: Union[None, Unset, str]
        if isinstance(self.object_id, Unset):
            object_id = UNSET
        else:
            object_id = self.object_id

        user_id: Union[None, Unset, str]
        if isinstance(self.user_id, Unset):
            user_id = UNSET
        else:
            user_id = self.user_id

        element_id: Union[None, Unset, str]
        if isinstance(self.element_id, Unset):
            element_id = UNSET
        else:
            element_id = self.element_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "type": type,
                "code": code,
                "time": time,
            }
        )
        if source_type is not UNSET:
            field_dict["source_type"] = source_type
        if source_id is not UNSET:
            field_dict["source_id"] = source_id
        if id is not UNSET:
            field_dict["id"] = id
        if object_id is not UNSET:
            field_dict["object_id"] = object_id
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if element_id is not UNSET:
            field_dict["element_id"] = element_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        type = d.pop("type")

        code = d.pop("code")

        time = isoparse(d.pop("time"))

        source_type = d.pop("source_type", UNSET)

        source_id = d.pop("source_id", UNSET)

        id = d.pop("id", UNSET)

        def _parse_object_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        object_id = _parse_object_id(d.pop("object_id", UNSET))

        def _parse_user_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        user_id = _parse_user_id(d.pop("user_id", UNSET))

        def _parse_element_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        element_id = _parse_element_id(d.pop("element_id", UNSET))

        event_header = cls(
            type=type,
            code=code,
            time=time,
            source_type=source_type,
            source_id=source_id,
            id=id,
            object_id=object_id,
            user_id=user_id,
            element_id=element_id,
        )

        event_header.additional_properties = d
        return event_header

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
