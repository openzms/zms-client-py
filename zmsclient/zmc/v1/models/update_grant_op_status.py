from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.grant_op_status import GrantOpStatus
from ..types import UNSET, Unset

T = TypeVar("T", bound="UpdateGrantOpStatus")


@_attrs_define
class UpdateGrantOpStatus:
    """A message that updates a grant's `op_status` field, allowing a client to implement the heartbeat protocol.

    Attributes:
        op_status (GrantOpStatus): The operational status of the grant, as updated by the grantee.  Other than the
            initial state (`submitted`), which is set on grant creation, it is set as part of the OpenZMS heartbeat
            protocol, described in depth at https://gitlab.flux.utah.edu/openzms/zms-zmc/-/blob/main/docs/heartbeats.md .
            Administrators may modify this field directly via grant update.
        message (Union[None, Unset, str]): A description of the heartbeat update that should be added to the grant audit
            log.  Reserve this for exceptional state transitions from the grantee side.
    """

    op_status: GrantOpStatus
    message: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "op_status": {"put": None, "post": None},
        "message": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        op_status = self.op_status.value

        message: Union[None, Unset, str]
        if isinstance(self.message, Unset):
            message = UNSET
        else:
            message = self.message

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "op_status": op_status,
            }
        )
        if message is not UNSET:
            field_dict["message"] = message

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        op_status = GrantOpStatus(d.pop("op_status"))

        def _parse_message(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        message = _parse_message(d.pop("message", UNSET))

        update_grant_op_status = cls(
            op_status=op_status,
            message=message,
        )

        update_grant_op_status.additional_properties = d
        return update_grant_op_status

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
