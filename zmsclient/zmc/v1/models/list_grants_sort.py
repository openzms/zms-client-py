from enum import Enum


class ListGrantsSort(str, Enum):
    APPROVED_AT = "approved_at"
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    DENIED_AT = "denied_at"
    ELEMENT_ID = "element_id"
    EXPIRES_AT = "expires_at"
    NAME = "name"
    OP_STATUS = "op_status"
    OWNER_APPROVED_AT = "owner_approved_at"
    REVOKED_AT = "revoked_at"
    SPECTRUM_ID = "spectrum_id"
    STARTS_AT = "starts_at"
    STATUS = "status"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
