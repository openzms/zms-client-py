from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.area_point import AreaPoint


T = TypeVar("T", bound="Area")


@_attrs_define
class Area:
    """An OGC-style polygon that encodes an area.  Supports both planar and geodetic systems.

    Attributes:
        element_id (str): The id of the element that created this area.
        name (str): The name of the area.
        srid (int): OGC SRID of the coordinate system (e.g. 4326 for WGS84). Example: 4326.
        id (Union[Unset, str]): The area ID.
        description (Union[None, Unset, str]): A description of the area; optional.
        points (Union[List['AreaPoint'], None, Unset]): An array of AreaPoint, according to the OGC polygon
            specification.
    """

    element_id: str
    name: str
    srid: int
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    points: Union[List["AreaPoint"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "srid": {"put": True, "post": None},
        "points": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        srid = self.srid

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        points: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.points, Unset):
            points = UNSET
        elif isinstance(self.points, list):
            points = []
            for points_type_0_item_data in self.points:
                points_type_0_item = points_type_0_item_data.to_dict()
                points.append(points_type_0_item)

        else:
            points = self.points

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
                "srid": srid,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if points is not UNSET:
            field_dict["points"] = points

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.area_point import AreaPoint

        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        srid = d.pop("srid")

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_points(data: object) -> Union[List["AreaPoint"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                points_type_0 = []
                _points_type_0 = data
                for points_type_0_item_data in _points_type_0:
                    points_type_0_item = AreaPoint.from_dict(points_type_0_item_data)

                    points_type_0.append(points_type_0_item)

                return points_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["AreaPoint"], None, Unset], data)

        points = _parse_points(d.pop("points", UNSET))

        area = cls(
            element_id=element_id,
            name=name,
            srid=srid,
            id=id,
            description=description,
            points=points,
        )

        area.additional_properties = d
        return area

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
