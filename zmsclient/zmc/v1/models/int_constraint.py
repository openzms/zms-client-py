from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.area import Area


T = TypeVar("T", bound="IntConstraint")


@_attrs_define
class IntConstraint:
    """An interference constraint.

    Attributes:
        name (str): The name of the constraint.
        max_power (float): The maximum received power allowed in this grant's operating area.
        id (Union[Unset, str]): The id of the interference constraint.
        description (Union[None, Unset, str]): A description of the interference constraint.
        area_id (Union[None, Unset, str]): The Area id to which the constraint applies.
        area (Union[None, Unset, Area]): An OGC-style polygon that encodes an area.  Supports both planar and geodetic
            systems.
    """

    name: str
    max_power: float
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    area_id: Union[None, Unset, str] = UNSET
    area: Union[None, Unset, "Area"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "max_power": {"put": None, "post": None},
        "area_id": {"put": None, "post": None},
        "area": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        max_power = self.max_power

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        area_id: Union[None, Unset, str]
        if isinstance(self.area_id, Unset):
            area_id = UNSET
        else:
            area_id = self.area_id

        area: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.area is None:
            area = None
        elif not isinstance(self.area, Unset):
            area = self.area.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "max_power": max_power,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if area_id is not UNSET:
            field_dict["area_id"] = area_id
        if area is not UNSET:
            field_dict["area"] = area

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.area import Area

        d = src_dict.copy()
        name = d.pop("name")

        max_power = d.pop("max_power")

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_area_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        area_id = _parse_area_id(d.pop("area_id", UNSET))

        _area = d.pop("area", UNSET)
        area: Union[None, Unset, Area]
        if isinstance(_area, Unset):
            area = UNSET
        elif _area is None:
            area = None
        else:
            area = Area.from_dict(_area)

        int_constraint = cls(
            name=name,
            max_power=max_power,
            id=id,
            description=description,
            area_id=area_id,
            area=area,
        )

        int_constraint.additional_properties = d
        return int_constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
