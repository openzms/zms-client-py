import datetime
from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.grant_op_status import GrantOpStatus
from ..models.grant_status import GrantStatus
from ..types import UNSET, Unset

T = TypeVar("T", bound="GrantLog")


@_attrs_define
class GrantLog:
    """A log entry for a grant.

    Attributes:
        id (Union[Unset, str]): The id of the grant log.
        grant_id (Union[Unset, str]): The id of the spectrum grant.
        status (Union[Unset, GrantStatus]): The status of the grant.  This field is determined by the OpenZMS grant
            controller, and cannot be set by non-administrator users.
        op_status (Union[Unset, GrantOpStatus]): The operational status of the grant, as updated by the grantee.  Other
            than the initial state (`submitted`), which is set on grant creation, it is set as part of the OpenZMS heartbeat
            protocol, described in depth at https://gitlab.flux.utah.edu/openzms/zms-zmc/-/blob/main/docs/heartbeats.md .
            Administrators may modify this field directly via grant update.
        message (Union[Unset, str]): A log message.
        created_at (Union[Unset, datetime.datetime]): Creation time.
    """

    id: Union[Unset, str] = UNSET
    grant_id: Union[Unset, str] = UNSET
    status: Union[Unset, GrantStatus] = UNSET
    op_status: Union[Unset, GrantOpStatus] = UNSET
    message: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "grant_id": {"put": True, "post": None},
        "status": {"put": True, "post": True},
        "op_status": {"put": None, "post": True},
        "message": {"put": True, "post": None},
        "created_at": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        grant_id = self.grant_id

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        op_status: Union[Unset, str] = UNSET
        if not isinstance(self.op_status, Unset):
            op_status = self.op_status.value

        message = self.message

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if status is not UNSET:
            field_dict["status"] = status
        if op_status is not UNSET:
            field_dict["op_status"] = op_status
        if message is not UNSET:
            field_dict["message"] = message
        if created_at is not UNSET:
            field_dict["created_at"] = created_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        grant_id = d.pop("grant_id", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[Unset, GrantStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = GrantStatus(_status)

        _op_status = d.pop("op_status", UNSET)
        op_status: Union[Unset, GrantOpStatus]
        if isinstance(_op_status, Unset):
            op_status = UNSET
        else:
            op_status = GrantOpStatus(_op_status)

        message = d.pop("message", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        grant_log = cls(
            id=id,
            grant_id=grant_id,
            status=status,
            op_status=op_status,
            message=message,
            created_at=created_at,
        )

        grant_log.additional_properties = d
        return grant_log

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
