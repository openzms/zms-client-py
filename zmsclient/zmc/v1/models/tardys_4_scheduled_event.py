import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Tardys4ScheduledEvent")


@_attrs_define
class Tardys4ScheduledEvent:
    """
    Attributes:
        event_id (str): Event ID
        date_time_start (datetime.datetime): Start time
        date_time_end (datetime.datetime): End time
        coord_type (str): Coordinate type
        event_status (str): Event status
        freq_start (int): Start frequency
        freq_stop (int): Stop frequency
        region_size (float): Region size
        region_x (float): Region X
        region_y (float): Region Y
        dpa_id (Union[Unset, str]): DPA ID
        loc_long (Union[None, Unset, float]): Longitude
        loc_lat (Union[None, Unset, float]): Latitude
        loc_elevation (Union[None, Unset, float]): Elevation
        loc_radius (Union[None, Unset, float]): Radius
    """

    event_id: str
    date_time_start: datetime.datetime
    date_time_end: datetime.datetime
    coord_type: str
    event_status: str
    freq_start: int
    freq_stop: int
    region_size: float
    region_x: float
    region_y: float
    dpa_id: Union[Unset, str] = UNSET
    loc_long: Union[None, Unset, float] = UNSET
    loc_lat: Union[None, Unset, float] = UNSET
    loc_elevation: Union[None, Unset, float] = UNSET
    loc_radius: Union[None, Unset, float] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "eventId": {"put": None, "post": None},
        "dpaId": {"put": None, "post": None},
        "dateTimeStart": {"put": None, "post": None},
        "dateTimeEnd": {"put": None, "post": None},
        "locLong": {"put": None, "post": None},
        "locLat": {"put": None, "post": None},
        "locElevation": {"put": None, "post": None},
        "locRadius": {"put": None, "post": None},
        "coordType": {"put": None, "post": None},
        "eventStatus": {"put": None, "post": None},
        "freqStart": {"put": None, "post": None},
        "freqStop": {"put": None, "post": None},
        "regionSize": {"put": None, "post": None},
        "regionX": {"put": None, "post": None},
        "regionY": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        event_id = self.event_id

        date_time_start = self.date_time_start.isoformat()

        date_time_end = self.date_time_end.isoformat()

        coord_type = self.coord_type

        event_status = self.event_status

        freq_start = self.freq_start

        freq_stop = self.freq_stop

        region_size = self.region_size

        region_x = self.region_x

        region_y = self.region_y

        dpa_id = self.dpa_id

        loc_long: Union[None, Unset, float]
        if isinstance(self.loc_long, Unset):
            loc_long = UNSET
        else:
            loc_long = self.loc_long

        loc_lat: Union[None, Unset, float]
        if isinstance(self.loc_lat, Unset):
            loc_lat = UNSET
        else:
            loc_lat = self.loc_lat

        loc_elevation: Union[None, Unset, float]
        if isinstance(self.loc_elevation, Unset):
            loc_elevation = UNSET
        else:
            loc_elevation = self.loc_elevation

        loc_radius: Union[None, Unset, float]
        if isinstance(self.loc_radius, Unset):
            loc_radius = UNSET
        else:
            loc_radius = self.loc_radius

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "eventId": event_id,
                "dateTimeStart": date_time_start,
                "dateTimeEnd": date_time_end,
                "coordType": coord_type,
                "eventStatus": event_status,
                "freqStart": freq_start,
                "freqStop": freq_stop,
                "regionSize": region_size,
                "regionX": region_x,
                "regionY": region_y,
            }
        )
        if dpa_id is not UNSET:
            field_dict["dpaId"] = dpa_id
        if loc_long is not UNSET:
            field_dict["locLong"] = loc_long
        if loc_lat is not UNSET:
            field_dict["locLat"] = loc_lat
        if loc_elevation is not UNSET:
            field_dict["locElevation"] = loc_elevation
        if loc_radius is not UNSET:
            field_dict["locRadius"] = loc_radius

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        event_id = d.pop("eventId")

        date_time_start = isoparse(d.pop("dateTimeStart"))

        date_time_end = isoparse(d.pop("dateTimeEnd"))

        coord_type = d.pop("coordType")

        event_status = d.pop("eventStatus")

        freq_start = d.pop("freqStart")

        freq_stop = d.pop("freqStop")

        region_size = d.pop("regionSize")

        region_x = d.pop("regionX")

        region_y = d.pop("regionY")

        dpa_id = d.pop("dpaId", UNSET)

        def _parse_loc_long(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        loc_long = _parse_loc_long(d.pop("locLong", UNSET))

        def _parse_loc_lat(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        loc_lat = _parse_loc_lat(d.pop("locLat", UNSET))

        def _parse_loc_elevation(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        loc_elevation = _parse_loc_elevation(d.pop("locElevation", UNSET))

        def _parse_loc_radius(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        loc_radius = _parse_loc_radius(d.pop("locRadius", UNSET))

        tardys_4_scheduled_event = cls(
            event_id=event_id,
            date_time_start=date_time_start,
            date_time_end=date_time_end,
            coord_type=coord_type,
            event_status=event_status,
            freq_start=freq_start,
            freq_stop=freq_stop,
            region_size=region_size,
            region_x=region_x,
            region_y=region_y,
            dpa_id=dpa_id,
            loc_long=loc_long,
            loc_lat=loc_lat,
            loc_elevation=loc_elevation,
            loc_radius=loc_radius,
        )

        tardys_4_scheduled_event.additional_properties = d
        return tardys_4_scheduled_event

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
