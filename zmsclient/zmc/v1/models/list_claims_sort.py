from enum import Enum


class ListClaimsSort(str, Enum):
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    DENIED_AT = "denied_at"
    ELEMENT_ID = "element_id"
    EXT_ID = "ext_id"
    HTML_URL = "html_url"
    NAME = "name"
    SOURCE = "source"
    TYPE = "type"
    UPDATED_AT = "updated_at"
    VERIFIED_AT = "verified_at"

    def __str__(self) -> str:
        return str(self.value)
