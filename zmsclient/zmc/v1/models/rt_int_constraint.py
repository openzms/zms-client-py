from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="RtIntConstraint")


@_attrs_define
class RtIntConstraint:
    """A runtime interference constraint.

    Attributes:
        name (str): The name of the constraint.
        metric (str): The statistical metric used to compute an alarm associated with the constraint.
        comparator (str): A comparator used to determine if the alarm associated with the constraint is alarming or not.
        value (float): The threshold used to determine, in combination with the comparator, if the alarm is alarming.
        id (Union[Unset, str]): The id of the runtime interference constraint.
        description (Union[None, Unset, str]): A description of the interference constraint.
        aggregator (Union[None, Unset, str]): The statistical metric used to aggregate the value of the alarm associated
            with the constraint.
        period (Union[None, Unset, int]): The period (seconds) of aggregation.
        criticality (Union[None, Unset, int]): An integer indicating the criticality of the runtime interference
            violation.
    """

    name: str
    metric: str
    comparator: str
    value: float
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    aggregator: Union[None, Unset, str] = UNSET
    period: Union[None, Unset, int] = UNSET
    criticality: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "metric": {"put": None, "post": None},
        "comparator": {"put": None, "post": None},
        "value": {"put": None, "post": None},
        "aggregator": {"put": None, "post": None},
        "period": {"put": None, "post": None},
        "criticality": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        metric = self.metric

        comparator = self.comparator

        value = self.value

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        aggregator: Union[None, Unset, str]
        if isinstance(self.aggregator, Unset):
            aggregator = UNSET
        else:
            aggregator = self.aggregator

        period: Union[None, Unset, int]
        if isinstance(self.period, Unset):
            period = UNSET
        else:
            period = self.period

        criticality: Union[None, Unset, int]
        if isinstance(self.criticality, Unset):
            criticality = UNSET
        else:
            criticality = self.criticality

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "metric": metric,
                "comparator": comparator,
                "value": value,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if aggregator is not UNSET:
            field_dict["aggregator"] = aggregator
        if period is not UNSET:
            field_dict["period"] = period
        if criticality is not UNSET:
            field_dict["criticality"] = criticality

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        metric = d.pop("metric")

        comparator = d.pop("comparator")

        value = d.pop("value")

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_aggregator(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        aggregator = _parse_aggregator(d.pop("aggregator", UNSET))

        def _parse_period(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        period = _parse_period(d.pop("period", UNSET))

        def _parse_criticality(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        criticality = _parse_criticality(d.pop("criticality", UNSET))

        rt_int_constraint = cls(
            name=name,
            metric=metric,
            comparator=comparator,
            value=value,
            id=id,
            description=description,
            aggregator=aggregator,
            period=period,
            criticality=criticality,
        )

        rt_int_constraint.additional_properties = d
        return rt_int_constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
