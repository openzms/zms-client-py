from enum import Enum


class GrantStatus(str, Enum):
    ACTIVE = "active"
    APPROVED = "approved"
    APPROVING = "approving"
    CREATED = "created"
    DELETED = "deleted"
    DENIED = "denied"
    PAUSED = "paused"
    PENDING = "pending"
    REPLACING = "replacing"
    REVOKED = "revoked"
    SCHEDULING = "scheduling"
    UNKNOWN = "unknown"

    def __str__(self) -> str:
        return str(self.value)
