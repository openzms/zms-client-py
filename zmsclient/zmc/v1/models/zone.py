import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.area import Area


T = TypeVar("T", bound="Zone")


@_attrs_define
class Zone:
    """A Zone descriptor: provides metadata about a zone.  Can be created and modified only by admins.  Typically there will be only a single zone.

    Attributes:
        element_id (str): The id of the element that created this zone.
        name (str): The name of the zone.
        id (Union[Unset, str]): The zone ID.
        description (Union[Unset, str]): A description of the zone.
        area_id (Union[None, Unset, str]): The Area id encompassing the Zone.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        area (Union[None, Unset, Area]): An OGC-style polygon that encodes an area.  Supports both planar and geodetic
            systems.
    """

    element_id: str
    name: str
    id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    area_id: Union[None, Unset, str] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    area: Union[None, Unset, "Area"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "area_id": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "area": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        id = self.id

        description = self.description

        area_id: Union[None, Unset, str]
        if isinstance(self.area_id, Unset):
            area_id = UNSET
        else:
            area_id = self.area_id

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        area: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.area is None:
            area = None
        elif not isinstance(self.area, Unset):
            area = self.area.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if area_id is not UNSET:
            field_dict["area_id"] = area_id
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if area is not UNSET:
            field_dict["area"] = area

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.area import Area

        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        id = d.pop("id", UNSET)

        description = d.pop("description", UNSET)

        def _parse_area_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        area_id = _parse_area_id(d.pop("area_id", UNSET))

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        _area = d.pop("area", UNSET)
        area: Union[None, Unset, Area]
        if isinstance(_area, Unset):
            area = UNSET
        elif _area is None:
            area = None
        else:
            area = Area.from_dict(_area)

        zone = cls(
            element_id=element_id,
            name=name,
            id=id,
            description=description,
            area_id=area_id,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            deleted_at=deleted_at,
            area=area,
        )

        zone.additional_properties = d
        return zone

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
