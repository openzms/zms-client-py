from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="Policy")


@_attrs_define
class Policy:
    """A spectrum usage policy.  Evaluated only at grant scheduling time.

    Attributes:
        allowed (bool): True if the element has access to the associated spectrum.
        id (Union[Unset, str]): The id of the spectrum policy.
        spectrum_id (Union[Unset, str]): The spectrum to which this policy applies.
        element_id (Union[None, Unset, str]): The element to which this policy applies; null signifies the default
            policy.
        auto_approve (Union[Unset, bool]): True if grants for this element should be automatically approved.
        priority (Union[Unset, int]): The scheduling priority given to this policy; higher values give this element
            higher priority access.
        max_duration (Union[Unset, int]): The maximum lifetime (seconds) of grants under this policy.
        when_unoccupied (Union[Unset, bool]): This policy grants spectrum use only when spectrum is unoccupied.
        disable_emit_check (Union[Unset, bool]): This policy disables propagation simulation-based conflict checks.
        allow_skip_acks (Union[Unset, bool]): This policy allows a grantee to not implement the heartbeat protocol.
        allow_inactive (Union[Unset, bool]): This policy allows approval of a grant that is immediately paused because
            it conflicts with another higher-priority grant.  This is useful in a few cases, but is not enabled by default.
            The grantee must set the corresponding `allow_inactive` bit in their grant request.
        allow_conflicts (Union[Unset, bool]): This policy allows approval that conflicts with other grants.  This is
            useful in a few cases, but is not enabled by default.  The grantee must set the corresponding `allow_conflicts`
            bit in their grant request.
    """

    allowed: bool
    id: Union[Unset, str] = UNSET
    spectrum_id: Union[Unset, str] = UNSET
    element_id: Union[None, Unset, str] = UNSET
    auto_approve: Union[Unset, bool] = UNSET
    priority: Union[Unset, int] = UNSET
    max_duration: Union[Unset, int] = UNSET
    when_unoccupied: Union[Unset, bool] = UNSET
    disable_emit_check: Union[Unset, bool] = UNSET
    allow_skip_acks: Union[Unset, bool] = UNSET
    allow_inactive: Union[Unset, bool] = UNSET
    allow_conflicts: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "spectrum_id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "allowed": {"put": None, "post": None},
        "auto_approve": {"put": None, "post": None},
        "priority": {"put": None, "post": None},
        "max_duration": {"put": None, "post": None},
        "when_unoccupied": {"put": None, "post": None},
        "disable_emit_check": {"put": None, "post": None},
        "allow_skip_acks": {"put": None, "post": None},
        "allow_inactive": {"put": None, "post": None},
        "allow_conflicts": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        allowed = self.allowed

        id = self.id

        spectrum_id = self.spectrum_id

        element_id: Union[None, Unset, str]
        if isinstance(self.element_id, Unset):
            element_id = UNSET
        else:
            element_id = self.element_id

        auto_approve = self.auto_approve

        priority = self.priority

        max_duration = self.max_duration

        when_unoccupied = self.when_unoccupied

        disable_emit_check = self.disable_emit_check

        allow_skip_acks = self.allow_skip_acks

        allow_inactive = self.allow_inactive

        allow_conflicts = self.allow_conflicts

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "allowed": allowed,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if spectrum_id is not UNSET:
            field_dict["spectrum_id"] = spectrum_id
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if auto_approve is not UNSET:
            field_dict["auto_approve"] = auto_approve
        if priority is not UNSET:
            field_dict["priority"] = priority
        if max_duration is not UNSET:
            field_dict["max_duration"] = max_duration
        if when_unoccupied is not UNSET:
            field_dict["when_unoccupied"] = when_unoccupied
        if disable_emit_check is not UNSET:
            field_dict["disable_emit_check"] = disable_emit_check
        if allow_skip_acks is not UNSET:
            field_dict["allow_skip_acks"] = allow_skip_acks
        if allow_inactive is not UNSET:
            field_dict["allow_inactive"] = allow_inactive
        if allow_conflicts is not UNSET:
            field_dict["allow_conflicts"] = allow_conflicts

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        allowed = d.pop("allowed")

        id = d.pop("id", UNSET)

        spectrum_id = d.pop("spectrum_id", UNSET)

        def _parse_element_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        element_id = _parse_element_id(d.pop("element_id", UNSET))

        auto_approve = d.pop("auto_approve", UNSET)

        priority = d.pop("priority", UNSET)

        max_duration = d.pop("max_duration", UNSET)

        when_unoccupied = d.pop("when_unoccupied", UNSET)

        disable_emit_check = d.pop("disable_emit_check", UNSET)

        allow_skip_acks = d.pop("allow_skip_acks", UNSET)

        allow_inactive = d.pop("allow_inactive", UNSET)

        allow_conflicts = d.pop("allow_conflicts", UNSET)

        policy = cls(
            allowed=allowed,
            id=id,
            spectrum_id=spectrum_id,
            element_id=element_id,
            auto_approve=auto_approve,
            priority=priority,
            max_duration=max_duration,
            when_unoccupied=when_unoccupied,
            disable_emit_check=disable_emit_check,
            allow_skip_acks=allow_skip_acks,
            allow_inactive=allow_inactive,
            allow_conflicts=allow_conflicts,
        )

        policy.additional_properties = d
        return policy

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
