import datetime
from io import BytesIO
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, File, FileJsonType, Unset

T = TypeVar("T", bound="Antenna")


@_attrs_define
class Antenna:
    """A description of an antenna (type, kind, gain profile, etc).  May be linked to a Radio via a RadioPort to model a transmitter or receiver.  This object describes the properties of an Antenna, not its physical deployment.  Those deployment properties are associated with one or more RadioPort.

    Attributes:
        element_id (str): The id of the element that created this antenna.
        name (str): The antenna name.
        id (Union[Unset, str]): The id of the antenna.
        description (Union[None, Unset, str]): The antenna description; optional.
        type (Union[None, Unset, str]): The antenna type.
        vendor (Union[None, Unset, str]): The antenna vendor.
        model (Union[None, Unset, str]): The antenna model.
        url (Union[None, Unset, str]): Antenna product or documentation link.
        gain (Union[None, Unset, float]): The antenna gain, if not better-described by a gain profile.
        beam_width (Union[None, Unset, int]): The antenna beam width, if not better-described by a gain profile.
        gain_profile_data (Union[File, None, Unset]): The antenna gain profile raw data in base64.
        gain_profile_format (Union[None, Unset, str]): The format of the antenna profile (e.g. `msi`).
        gain_profile_url (Union[None, Unset, str]): The URL at which the antenna gain profile data may be accessed (e.g.
            if raw data is too large).
        creator_id (Union[Unset, str]): The id of the creator.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updater_id (Union[None, Unset, str]): The id of the updater.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
    """

    element_id: str
    name: str
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    type: Union[None, Unset, str] = UNSET
    vendor: Union[None, Unset, str] = UNSET
    model: Union[None, Unset, str] = UNSET
    url: Union[None, Unset, str] = UNSET
    gain: Union[None, Unset, float] = UNSET
    beam_width: Union[None, Unset, int] = UNSET
    gain_profile_data: Union[File, None, Unset] = UNSET
    gain_profile_format: Union[None, Unset, str] = UNSET
    gain_profile_url: Union[None, Unset, str] = UNSET
    creator_id: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "type": {"put": None, "post": None},
        "vendor": {"put": None, "post": None},
        "model": {"put": None, "post": None},
        "url": {"put": None, "post": None},
        "gain": {"put": None, "post": None},
        "beam_width": {"put": None, "post": None},
        "gain_profile_data": {"put": None, "post": None},
        "gain_profile_format": {"put": None, "post": None},
        "gain_profile_url": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        type: Union[None, Unset, str]
        if isinstance(self.type, Unset):
            type = UNSET
        else:
            type = self.type

        vendor: Union[None, Unset, str]
        if isinstance(self.vendor, Unset):
            vendor = UNSET
        else:
            vendor = self.vendor

        model: Union[None, Unset, str]
        if isinstance(self.model, Unset):
            model = UNSET
        else:
            model = self.model

        url: Union[None, Unset, str]
        if isinstance(self.url, Unset):
            url = UNSET
        else:
            url = self.url

        gain: Union[None, Unset, float]
        if isinstance(self.gain, Unset):
            gain = UNSET
        else:
            gain = self.gain

        beam_width: Union[None, Unset, int]
        if isinstance(self.beam_width, Unset):
            beam_width = UNSET
        else:
            beam_width = self.beam_width

        gain_profile_data: Union[FileJsonType, None, Unset]
        if isinstance(self.gain_profile_data, Unset):
            gain_profile_data = UNSET
        elif isinstance(self.gain_profile_data, File):
            gain_profile_data = self.gain_profile_data.to_tuple()

        else:
            gain_profile_data = self.gain_profile_data

        gain_profile_format: Union[None, Unset, str]
        if isinstance(self.gain_profile_format, Unset):
            gain_profile_format = UNSET
        else:
            gain_profile_format = self.gain_profile_format

        gain_profile_url: Union[None, Unset, str]
        if isinstance(self.gain_profile_url, Unset):
            gain_profile_url = UNSET
        else:
            gain_profile_url = self.gain_profile_url

        creator_id = self.creator_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if type is not UNSET:
            field_dict["type"] = type
        if vendor is not UNSET:
            field_dict["vendor"] = vendor
        if model is not UNSET:
            field_dict["model"] = model
        if url is not UNSET:
            field_dict["url"] = url
        if gain is not UNSET:
            field_dict["gain"] = gain
        if beam_width is not UNSET:
            field_dict["beam_width"] = beam_width
        if gain_profile_data is not UNSET:
            field_dict["gain_profile_data"] = gain_profile_data
        if gain_profile_format is not UNSET:
            field_dict["gain_profile_format"] = gain_profile_format
        if gain_profile_url is not UNSET:
            field_dict["gain_profile_url"] = gain_profile_url
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        type = _parse_type(d.pop("type", UNSET))

        def _parse_vendor(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        vendor = _parse_vendor(d.pop("vendor", UNSET))

        def _parse_model(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        model = _parse_model(d.pop("model", UNSET))

        def _parse_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        url = _parse_url(d.pop("url", UNSET))

        def _parse_gain(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        gain = _parse_gain(d.pop("gain", UNSET))

        def _parse_beam_width(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        beam_width = _parse_beam_width(d.pop("beam_width", UNSET))

        def _parse_gain_profile_data(data: object) -> Union[File, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, bytes):
                    raise TypeError()
                gain_profile_data_type_0 = File(payload=BytesIO(data))

                return gain_profile_data_type_0
            except:  # noqa: E722
                pass
            return cast(Union[File, None, Unset], data)

        gain_profile_data = _parse_gain_profile_data(d.pop("gain_profile_data", UNSET))

        def _parse_gain_profile_format(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        gain_profile_format = _parse_gain_profile_format(
            d.pop("gain_profile_format", UNSET)
        )

        def _parse_gain_profile_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        gain_profile_url = _parse_gain_profile_url(d.pop("gain_profile_url", UNSET))

        creator_id = d.pop("creator_id", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        antenna = cls(
            element_id=element_id,
            name=name,
            id=id,
            description=description,
            type=type,
            vendor=vendor,
            model=model,
            url=url,
            gain=gain,
            beam_width=beam_width,
            gain_profile_data=gain_profile_data,
            gain_profile_format=gain_profile_format,
            gain_profile_url=gain_profile_url,
            creator_id=creator_id,
            created_at=created_at,
            updater_id=updater_id,
            updated_at=updated_at,
            deleted_at=deleted_at,
        )

        antenna.additional_properties = d
        return antenna

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
