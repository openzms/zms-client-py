import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.policy import Policy
    from ..models.spectrum_constraint import SpectrumConstraint


T = TypeVar("T", bound="Spectrum")


@_attrs_define
class Spectrum:
    """A constrained frequency range provided to the ZMS as shareable, managed spectrum by one Element.

    Attributes:
        element_id (str): The id of the element that provides this spectrum.
        name (str): A name for the provided spectrum.
        starts_at (datetime.datetime): Start time.
        id (Union[Unset, str]): The id of element-provided spectrum, to be managed by the ZMS.
        description (Union[None, Unset, str]): A description of the provided spectrum.
        ext_id (Union[None, Unset, str]): An external ID associated with this spectrum by the delegating user; opaque to
            OpenZMS.
        url (Union[Unset, str]): A URL describing this spectrum, establishing permission/legality to operate in this
            spectrum, etc.
        enabled (Union[Unset, bool]): True if this spectrum may be used.  If in use or scheduled, may not be set False
            unless forced. Default: False.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        approved_at (Union[None, Unset, datetime.datetime]): Approval time.
        denied_at (Union[None, Unset, datetime.datetime]): Denial time.
        expires_at (Union[None, Unset, datetime.datetime]): Expiration time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        constraints (Union[List['SpectrumConstraint'], None, Unset]): A list of constraints on the use of this spectrum.
        policies (Union[List['Policy'], None, Unset]): A list of per-element access policies governing the use of this
            spectrum.
    """

    element_id: str
    name: str
    starts_at: datetime.datetime
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    ext_id: Union[None, Unset, str] = UNSET
    url: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = False
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    approved_at: Union[None, Unset, datetime.datetime] = UNSET
    denied_at: Union[None, Unset, datetime.datetime] = UNSET
    expires_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    constraints: Union[List["SpectrumConstraint"], None, Unset] = UNSET
    policies: Union[List["Policy"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "ext_id": {"put": None, "post": None},
        "url": {"put": None, "post": None},
        "enabled": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "approved_at": {"put": None, "post": True},
        "denied_at": {"put": None, "post": True},
        "starts_at": {"put": None, "post": None},
        "expires_at": {"put": None, "post": None},
        "deleted_at": {"put": True, "post": True},
        "constraints": {"put": True, "post": None},
        "policies": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        starts_at = self.starts_at.isoformat()

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        ext_id: Union[None, Unset, str]
        if isinstance(self.ext_id, Unset):
            ext_id = UNSET
        else:
            ext_id = self.ext_id

        url = self.url

        enabled = self.enabled

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        approved_at: Union[None, Unset, str]
        if isinstance(self.approved_at, Unset):
            approved_at = UNSET
        elif isinstance(self.approved_at, datetime.datetime):
            approved_at = self.approved_at.isoformat()
        else:
            approved_at = self.approved_at

        denied_at: Union[None, Unset, str]
        if isinstance(self.denied_at, Unset):
            denied_at = UNSET
        elif isinstance(self.denied_at, datetime.datetime):
            denied_at = self.denied_at.isoformat()
        else:
            denied_at = self.denied_at

        expires_at: Union[None, Unset, str]
        if isinstance(self.expires_at, Unset):
            expires_at = UNSET
        elif isinstance(self.expires_at, datetime.datetime):
            expires_at = self.expires_at.isoformat()
        else:
            expires_at = self.expires_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        constraints: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.constraints, Unset):
            constraints = UNSET
        elif isinstance(self.constraints, list):
            constraints = []
            for constraints_type_0_item_data in self.constraints:
                constraints_type_0_item = constraints_type_0_item_data.to_dict()
                constraints.append(constraints_type_0_item)

        else:
            constraints = self.constraints

        policies: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.policies, Unset):
            policies = UNSET
        elif isinstance(self.policies, list):
            policies = []
            for policies_type_0_item_data in self.policies:
                policies_type_0_item = policies_type_0_item_data.to_dict()
                policies.append(policies_type_0_item)

        else:
            policies = self.policies

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
                "starts_at": starts_at,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if ext_id is not UNSET:
            field_dict["ext_id"] = ext_id
        if url is not UNSET:
            field_dict["url"] = url
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if approved_at is not UNSET:
            field_dict["approved_at"] = approved_at
        if denied_at is not UNSET:
            field_dict["denied_at"] = denied_at
        if expires_at is not UNSET:
            field_dict["expires_at"] = expires_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if constraints is not UNSET:
            field_dict["constraints"] = constraints
        if policies is not UNSET:
            field_dict["policies"] = policies

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.policy import Policy
        from ..models.spectrum_constraint import SpectrumConstraint

        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        starts_at = isoparse(d.pop("starts_at"))

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_ext_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        ext_id = _parse_ext_id(d.pop("ext_id", UNSET))

        url = d.pop("url", UNSET)

        enabled = d.pop("enabled", UNSET)

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_approved_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                approved_at_type_0 = isoparse(data)

                return approved_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        approved_at = _parse_approved_at(d.pop("approved_at", UNSET))

        def _parse_denied_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                denied_at_type_0 = isoparse(data)

                return denied_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        denied_at = _parse_denied_at(d.pop("denied_at", UNSET))

        def _parse_expires_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                expires_at_type_0 = isoparse(data)

                return expires_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        expires_at = _parse_expires_at(d.pop("expires_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        def _parse_constraints(
            data: object,
        ) -> Union[List["SpectrumConstraint"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                constraints_type_0 = []
                _constraints_type_0 = data
                for constraints_type_0_item_data in _constraints_type_0:
                    constraints_type_0_item = SpectrumConstraint.from_dict(
                        constraints_type_0_item_data
                    )

                    constraints_type_0.append(constraints_type_0_item)

                return constraints_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["SpectrumConstraint"], None, Unset], data)

        constraints = _parse_constraints(d.pop("constraints", UNSET))

        def _parse_policies(data: object) -> Union[List["Policy"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                policies_type_0 = []
                _policies_type_0 = data
                for policies_type_0_item_data in _policies_type_0:
                    policies_type_0_item = Policy.from_dict(policies_type_0_item_data)

                    policies_type_0.append(policies_type_0_item)

                return policies_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Policy"], None, Unset], data)

        policies = _parse_policies(d.pop("policies", UNSET))

        spectrum = cls(
            element_id=element_id,
            name=name,
            starts_at=starts_at,
            id=id,
            description=description,
            ext_id=ext_id,
            url=url,
            enabled=enabled,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            approved_at=approved_at,
            denied_at=denied_at,
            expires_at=expires_at,
            deleted_at=deleted_at,
            constraints=constraints,
            policies=policies,
        )

        spectrum.additional_properties = d
        return spectrum

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
