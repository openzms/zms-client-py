import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.grant import Grant


T = TypeVar("T", bound="Claim")


@_attrs_define
class Claim:
    """A spectrum claim (e.g. from some external management system, like CBRS).

    Attributes:
        element_id (str): The element id associated with the claim.
        ext_id (str): The claim id within the external spectrum management system.
        type (str): The type of external management system (e.g., cbrs-sas).
        source (str): The source external management system (e.g., cbrs-google).
        name (str): The name of the claim within the external management system.
        id (Union[Unset, str]): The id of the spectrum claim.
        html_url (Union[None, Unset, str]): The URL of the claim in the external management system portal.
        description (Union[None, Unset, str]): The description of the claim within the external management system.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        verified_at (Union[None, Unset, datetime.datetime]): Claim verification time.
        denied_at (Union[None, Unset, datetime.datetime]): Denial time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        grant_id (Union[None, Unset, str]): The id of the associated grant.  For convenience and similarity of concept,
            a Claim points to a Grant to describe its operational parameters, so that OpenZMS can attempt to coexist with
            grants using the same radio operating parameters.
        grant (Union[None, Unset, Grant]): A spectrum grant.
    """

    element_id: str
    ext_id: str
    type: str
    source: str
    name: str
    id: Union[Unset, str] = UNSET
    html_url: Union[None, Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    verified_at: Union[None, Unset, datetime.datetime] = UNSET
    denied_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    grant_id: Union[None, Unset, str] = UNSET
    grant: Union[None, Unset, "Grant"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "ext_id": {"put": True, "post": None},
        "type": {"put": True, "post": None},
        "source": {"put": True, "post": None},
        "name": {"put": True, "post": None},
        "html_url": {"put": True, "post": None},
        "description": {"put": True, "post": None},
        "creator_id": {"put": True, "post": None},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "verified_at": {"put": None, "post": None},
        "denied_at": {"put": None, "post": True},
        "deleted_at": {"put": True, "post": True},
        "grant_id": {"put": True, "post": None},
        "grant": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        ext_id = self.ext_id

        type = self.type

        source = self.source

        name = self.name

        id = self.id

        html_url: Union[None, Unset, str]
        if isinstance(self.html_url, Unset):
            html_url = UNSET
        else:
            html_url = self.html_url

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        verified_at: Union[None, Unset, str]
        if isinstance(self.verified_at, Unset):
            verified_at = UNSET
        elif isinstance(self.verified_at, datetime.datetime):
            verified_at = self.verified_at.isoformat()
        else:
            verified_at = self.verified_at

        denied_at: Union[None, Unset, str]
        if isinstance(self.denied_at, Unset):
            denied_at = UNSET
        elif isinstance(self.denied_at, datetime.datetime):
            denied_at = self.denied_at.isoformat()
        else:
            denied_at = self.denied_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        grant_id: Union[None, Unset, str]
        if isinstance(self.grant_id, Unset):
            grant_id = UNSET
        else:
            grant_id = self.grant_id

        grant: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.grant is None:
            grant = None
        elif not isinstance(self.grant, Unset):
            grant = self.grant.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "ext_id": ext_id,
                "type": type,
                "source": source,
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if html_url is not UNSET:
            field_dict["html_url"] = html_url
        if description is not UNSET:
            field_dict["description"] = description
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if verified_at is not UNSET:
            field_dict["verified_at"] = verified_at
        if denied_at is not UNSET:
            field_dict["denied_at"] = denied_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if grant is not UNSET:
            field_dict["grant"] = grant

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.grant import Grant

        d = src_dict.copy()
        element_id = d.pop("element_id")

        ext_id = d.pop("ext_id")

        type = d.pop("type")

        source = d.pop("source")

        name = d.pop("name")

        id = d.pop("id", UNSET)

        def _parse_html_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        html_url = _parse_html_url(d.pop("html_url", UNSET))

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_verified_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                verified_at_type_0 = isoparse(data)

                return verified_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        verified_at = _parse_verified_at(d.pop("verified_at", UNSET))

        def _parse_denied_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                denied_at_type_0 = isoparse(data)

                return denied_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        denied_at = _parse_denied_at(d.pop("denied_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        def _parse_grant_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        grant_id = _parse_grant_id(d.pop("grant_id", UNSET))

        _grant = d.pop("grant", UNSET)
        grant: Union[None, Unset, Grant]
        if isinstance(_grant, Unset):
            grant = UNSET
        elif _grant is None:
            grant = None
        else:
            grant = Grant.from_dict(_grant)

        claim = cls(
            element_id=element_id,
            ext_id=ext_id,
            type=type,
            source=source,
            name=name,
            id=id,
            html_url=html_url,
            description=description,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            verified_at=verified_at,
            denied_at=denied_at,
            deleted_at=deleted_at,
            grant_id=grant_id,
            grant=grant,
        )

        claim.additional_properties = d
        return claim

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
