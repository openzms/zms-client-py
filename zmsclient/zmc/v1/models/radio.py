import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.location import Location
    from ..models.radio_port import RadioPort


T = TypeVar("T", bound="Radio")


@_attrs_define
class Radio:
    """A radio operated by an element.

    Attributes:
        element_id (str): The id of the element that operates this transmitter.
        name (str): The radio name.
        id (Union[Unset, str]): The id of the radio.
        description (Union[None, Unset, str]): The radio description; optional.
        fcc_id (Union[None, Unset, str]): The FCC id of the radio.
        device_id (Union[None, Unset, str]): A secondary device id, if any.
        serial_number (Union[None, Unset, str]): The serial number of the radio.
        html_url (Union[None, Unset, str]): A URL associated with this radio that is meaningful to the requestor; opaque
            to OpenZMS.
        location_id (Union[None, Unset, str]): The radio's location; informational.  Only antenna locations are used for
            simulation and analysis.
        enabled (Union[Unset, bool]): True if this radio may be used.  If in use or scheduled, may not be set False
            unless forced.
        creator_id (Union[Unset, str]): The id of the creator.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updater_id (Union[None, Unset, str]): The id of the updater.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        ports (Union[List['RadioPort'], None, Unset]): A list of radio ports.
        location (Union[None, Unset, Location]): A point that encodes a location.  Locations are owned by elements and
            can only be modified by users with an Element Operator role or greater.
    """

    element_id: str
    name: str
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    fcc_id: Union[None, Unset, str] = UNSET
    device_id: Union[None, Unset, str] = UNSET
    serial_number: Union[None, Unset, str] = UNSET
    html_url: Union[None, Unset, str] = UNSET
    location_id: Union[None, Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    creator_id: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    ports: Union[List["RadioPort"], None, Unset] = UNSET
    location: Union[None, Unset, "Location"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "fcc_id": {"put": None, "post": None},
        "device_id": {"put": None, "post": None},
        "serial_number": {"put": None, "post": None},
        "html_url": {"put": None, "post": None},
        "location_id": {"put": None, "post": None},
        "enabled": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "ports": {"put": True, "post": None},
        "location": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        element_id = self.element_id

        name = self.name

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        fcc_id: Union[None, Unset, str]
        if isinstance(self.fcc_id, Unset):
            fcc_id = UNSET
        else:
            fcc_id = self.fcc_id

        device_id: Union[None, Unset, str]
        if isinstance(self.device_id, Unset):
            device_id = UNSET
        else:
            device_id = self.device_id

        serial_number: Union[None, Unset, str]
        if isinstance(self.serial_number, Unset):
            serial_number = UNSET
        else:
            serial_number = self.serial_number

        html_url: Union[None, Unset, str]
        if isinstance(self.html_url, Unset):
            html_url = UNSET
        else:
            html_url = self.html_url

        location_id: Union[None, Unset, str]
        if isinstance(self.location_id, Unset):
            location_id = UNSET
        else:
            location_id = self.location_id

        enabled = self.enabled

        creator_id = self.creator_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        ports: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.ports, Unset):
            ports = UNSET
        elif isinstance(self.ports, list):
            ports = []
            for ports_type_0_item_data in self.ports:
                ports_type_0_item = ports_type_0_item_data.to_dict()
                ports.append(ports_type_0_item)

        else:
            ports = self.ports

        location: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.location is None:
            location = None
        elif not isinstance(self.location, Unset):
            location = self.location.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "element_id": element_id,
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if fcc_id is not UNSET:
            field_dict["fcc_id"] = fcc_id
        if device_id is not UNSET:
            field_dict["device_id"] = device_id
        if serial_number is not UNSET:
            field_dict["serial_number"] = serial_number
        if html_url is not UNSET:
            field_dict["html_url"] = html_url
        if location_id is not UNSET:
            field_dict["location_id"] = location_id
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if ports is not UNSET:
            field_dict["ports"] = ports
        if location is not UNSET:
            field_dict["location"] = location

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.location import Location
        from ..models.radio_port import RadioPort

        d = src_dict.copy()
        element_id = d.pop("element_id")

        name = d.pop("name")

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_fcc_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        fcc_id = _parse_fcc_id(d.pop("fcc_id", UNSET))

        def _parse_device_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        device_id = _parse_device_id(d.pop("device_id", UNSET))

        def _parse_serial_number(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        serial_number = _parse_serial_number(d.pop("serial_number", UNSET))

        def _parse_html_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        html_url = _parse_html_url(d.pop("html_url", UNSET))

        def _parse_location_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        location_id = _parse_location_id(d.pop("location_id", UNSET))

        enabled = d.pop("enabled", UNSET)

        creator_id = d.pop("creator_id", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        def _parse_ports(data: object) -> Union[List["RadioPort"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                ports_type_0 = []
                _ports_type_0 = data
                for ports_type_0_item_data in _ports_type_0:
                    ports_type_0_item = RadioPort.from_dict(ports_type_0_item_data)

                    ports_type_0.append(ports_type_0_item)

                return ports_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["RadioPort"], None, Unset], data)

        ports = _parse_ports(d.pop("ports", UNSET))

        _location = d.pop("location", UNSET)
        location: Union[None, Unset, Location]
        if isinstance(_location, Unset):
            location = UNSET
        elif _location is None:
            location = None
        else:
            location = Location.from_dict(_location)

        radio = cls(
            element_id=element_id,
            name=name,
            id=id,
            description=description,
            fcc_id=fcc_id,
            device_id=device_id,
            serial_number=serial_number,
            html_url=html_url,
            location_id=location_id,
            enabled=enabled,
            creator_id=creator_id,
            created_at=created_at,
            updater_id=updater_id,
            updated_at=updated_at,
            deleted_at=deleted_at,
            ports=ports,
            location=location,
        )

        radio.additional_properties = d
        return radio

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
