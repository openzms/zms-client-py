from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.int_constraint import IntConstraint


T = TypeVar("T", bound="GrantIntConstraint")


@_attrs_define
class GrantIntConstraint:
    """Interference constraint for a grant.

    Attributes:
        id (Union[Unset, str]): The id of the grant int constraint.
        grant_id (Union[Unset, str]): The id of the spectrum grant.
        int_constraint_id (Union[Unset, str]): The interference constraint id associated with the grant.
        int_constraint (Union[None, Unset, IntConstraint]): An interference constraint.
    """

    id: Union[Unset, str] = UNSET
    grant_id: Union[Unset, str] = UNSET
    int_constraint_id: Union[Unset, str] = UNSET
    int_constraint: Union[None, Unset, "IntConstraint"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "grant_id": {"put": True, "post": None},
        "int_constraint_id": {"put": True, "post": None},
        "int_constraint": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        grant_id = self.grant_id

        int_constraint_id = self.int_constraint_id

        int_constraint: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.int_constraint is None:
            int_constraint = None
        elif not isinstance(self.int_constraint, Unset):
            int_constraint = self.int_constraint.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if int_constraint_id is not UNSET:
            field_dict["int_constraint_id"] = int_constraint_id
        if int_constraint is not UNSET:
            field_dict["int_constraint"] = int_constraint

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.int_constraint import IntConstraint

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        grant_id = d.pop("grant_id", UNSET)

        int_constraint_id = d.pop("int_constraint_id", UNSET)

        _int_constraint = d.pop("int_constraint", UNSET)
        int_constraint: Union[None, Unset, IntConstraint]
        if isinstance(_int_constraint, Unset):
            int_constraint = UNSET
        elif _int_constraint is None:
            int_constraint = None
        else:
            int_constraint = IntConstraint.from_dict(_int_constraint)

        grant_int_constraint = cls(
            id=id,
            grant_id=grant_id,
            int_constraint_id=int_constraint_id,
            int_constraint=int_constraint,
        )

        grant_int_constraint.additional_properties = d
        return grant_int_constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
