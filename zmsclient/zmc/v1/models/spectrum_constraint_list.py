from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.spectrum_constraint import SpectrumConstraint


T = TypeVar("T", bound="SpectrumConstraintList")


@_attrs_define
class SpectrumConstraintList:
    """
    Attributes:
        spectrum (Union[Unset, List['SpectrumConstraint']]):
    """

    spectrum: Union[Unset, List["SpectrumConstraint"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {"spectrum": {"put": None, "post": None}}

    def to_dict(self) -> Dict[str, Any]:
        spectrum: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.spectrum, Unset):
            spectrum = []
            for spectrum_item_data in self.spectrum:
                spectrum_item = spectrum_item_data.to_dict()
                spectrum.append(spectrum_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if spectrum is not UNSET:
            field_dict["spectrum"] = spectrum

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.spectrum_constraint import SpectrumConstraint

        d = src_dict.copy()
        spectrum = []
        _spectrum = d.pop("spectrum", UNSET)
        for spectrum_item_data in _spectrum or []:
            spectrum_item = SpectrumConstraint.from_dict(spectrum_item_data)

            spectrum.append(spectrum_item)

        spectrum_constraint_list = cls(
            spectrum=spectrum,
        )

        spectrum_constraint_list.additional_properties = d
        return spectrum_constraint_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
