from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.radio_port import RadioPort


T = TypeVar("T", bound="RadioPortList")


@_attrs_define
class RadioPortList:
    """
    Attributes:
        page (Union[Unset, int]): The current page of results.
        total (Union[Unset, int]): The total number of records available.
        pages (Union[Unset, int]): The total number of pages available.
        radio_ports (Union[Unset, List['RadioPort']]):
    """

    page: Union[Unset, int] = UNSET
    total: Union[Unset, int] = UNSET
    pages: Union[Unset, int] = UNSET
    radio_ports: Union[Unset, List["RadioPort"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {}

    def to_dict(self) -> Dict[str, Any]:
        page = self.page

        total = self.total

        pages = self.pages

        radio_ports: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.radio_ports, Unset):
            radio_ports = []
            for radio_ports_item_data in self.radio_ports:
                radio_ports_item = radio_ports_item_data.to_dict()
                radio_ports.append(radio_ports_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if page is not UNSET:
            field_dict["page"] = page
        if total is not UNSET:
            field_dict["total"] = total
        if pages is not UNSET:
            field_dict["pages"] = pages
        if radio_ports is not UNSET:
            field_dict["radio_ports"] = radio_ports

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.radio_port import RadioPort

        d = src_dict.copy()
        page = d.pop("page", UNSET)

        total = d.pop("total", UNSET)

        pages = d.pop("pages", UNSET)

        radio_ports = []
        _radio_ports = d.pop("radio_ports", UNSET)
        for radio_ports_item_data in _radio_ports or []:
            radio_ports_item = RadioPort.from_dict(radio_ports_item_data)

            radio_ports.append(radio_ports_item)

        radio_port_list = cls(
            page=page,
            total=total,
            pages=pages,
            radio_ports=radio_ports,
        )

        radio_port_list.additional_properties = d
        return radio_port_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
