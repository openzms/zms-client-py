from enum import Enum


class ListSpectrumSort(str, Enum):
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    DENIED_AT = "denied_at"
    ELEMENT_ID = "element_id"
    ENABLED = "enabled"
    EXPIRES_AT = "expires_at"
    NAME = "name"
    STARTS_AT = "starts_at"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
