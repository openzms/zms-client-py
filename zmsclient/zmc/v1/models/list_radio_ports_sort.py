from enum import Enum


class ListRadioPortsSort(str, Enum):
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    ENABLED = "enabled"
    MAX_FREQ = "max_freq"
    MAX_POWER = "max_power"
    MIN_FREQ = "min_freq"
    NAME = "name"
    RX = "rx"
    TX = "tx"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
