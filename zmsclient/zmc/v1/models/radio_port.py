import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.antenna import Antenna
    from ..models.location import Location


T = TypeVar("T", bound="RadioPort")


@_attrs_define
class RadioPort:
    """Defines a transmitter, receiver, or transceiver by binding an antenna to an RF connector on a radio, and describing its location.

    Attributes:
        name (str): The port name.
        tx (bool): True if this port can transmit.
        rx (bool): True if this port can receive.
        min_freq (int): The start of the frequency range (inclusive, Hz) on which this port can transmit or receive.
        max_freq (int): The end of the frequency range (inclusive, Hz) on which this port can transmit or receive.
        max_power (float): The maximum transmit power (dBm).
        id (Union[Unset, str]): The id of the port.
        radio_id (Union[Unset, str]): The id of the associated radio.
        device_id (Union[None, Unset, str]): A secondary device id, if any.
        antenna_id (Union[None, Unset, str]): The id of the associated antenna.
        antenna_location_id (Union[None, Unset, str]): The deployed antenna's location; may differ from radio location.
        antenna_azimuth_angle (Union[None, Unset, float]): The deployed antenna's azimuth angle.
        antenna_elevation_angle (Union[None, Unset, float]): The deployed antenna's elevation angle.
        antenna_elevation_delta (Union[None, Unset, float]): The deployed antenna's elevation delta above the elevation
            of the surface it is installed upon.
        enabled (Union[Unset, bool]): True if this radio port may be used.  If in use or scheduled, may not be set False
            unless forced.
        creator_id (Union[Unset, str]): The id of the creator.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updater_id (Union[None, Unset, str]): The id of the updater.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        antenna (Union[None, Unset, Antenna]): A description of an antenna (type, kind, gain profile, etc).  May be
            linked to a Radio via a RadioPort to model a transmitter or receiver.  This object describes the properties of
            an Antenna, not its physical deployment.  Those deployment properties are associated with one or more RadioPort.
        antenna_location (Union[None, Unset, Location]): A point that encodes a location.  Locations are owned by
            elements and can only be modified by users with an Element Operator role or greater.
    """

    name: str
    tx: bool
    rx: bool
    min_freq: int
    max_freq: int
    max_power: float
    id: Union[Unset, str] = UNSET
    radio_id: Union[Unset, str] = UNSET
    device_id: Union[None, Unset, str] = UNSET
    antenna_id: Union[None, Unset, str] = UNSET
    antenna_location_id: Union[None, Unset, str] = UNSET
    antenna_azimuth_angle: Union[None, Unset, float] = UNSET
    antenna_elevation_angle: Union[None, Unset, float] = UNSET
    antenna_elevation_delta: Union[None, Unset, float] = UNSET
    enabled: Union[Unset, bool] = UNSET
    creator_id: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    antenna: Union[None, Unset, "Antenna"] = UNSET
    antenna_location: Union[None, Unset, "Location"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "radio_id": {"put": True, "post": None},
        "name": {"put": None, "post": None},
        "device_id": {"put": None, "post": None},
        "tx": {"put": None, "post": None},
        "rx": {"put": None, "post": None},
        "min_freq": {"put": None, "post": None},
        "max_freq": {"put": None, "post": None},
        "max_power": {"put": None, "post": None},
        "antenna_id": {"put": None, "post": None},
        "antenna_location_id": {"put": None, "post": None},
        "antenna_azimuth_angle": {"put": None, "post": None},
        "antenna_elevation_angle": {"put": None, "post": None},
        "antenna_elevation_delta": {"put": None, "post": None},
        "enabled": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "antenna": {"put": True, "post": None},
        "antenna_location": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        tx = self.tx

        rx = self.rx

        min_freq = self.min_freq

        max_freq = self.max_freq

        max_power = self.max_power

        id = self.id

        radio_id = self.radio_id

        device_id: Union[None, Unset, str]
        if isinstance(self.device_id, Unset):
            device_id = UNSET
        else:
            device_id = self.device_id

        antenna_id: Union[None, Unset, str]
        if isinstance(self.antenna_id, Unset):
            antenna_id = UNSET
        else:
            antenna_id = self.antenna_id

        antenna_location_id: Union[None, Unset, str]
        if isinstance(self.antenna_location_id, Unset):
            antenna_location_id = UNSET
        else:
            antenna_location_id = self.antenna_location_id

        antenna_azimuth_angle: Union[None, Unset, float]
        if isinstance(self.antenna_azimuth_angle, Unset):
            antenna_azimuth_angle = UNSET
        else:
            antenna_azimuth_angle = self.antenna_azimuth_angle

        antenna_elevation_angle: Union[None, Unset, float]
        if isinstance(self.antenna_elevation_angle, Unset):
            antenna_elevation_angle = UNSET
        else:
            antenna_elevation_angle = self.antenna_elevation_angle

        antenna_elevation_delta: Union[None, Unset, float]
        if isinstance(self.antenna_elevation_delta, Unset):
            antenna_elevation_delta = UNSET
        else:
            antenna_elevation_delta = self.antenna_elevation_delta

        enabled = self.enabled

        creator_id = self.creator_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        antenna: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.antenna is None:
            antenna = None
        elif not isinstance(self.antenna, Unset):
            antenna = self.antenna.to_dict()

        antenna_location: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.antenna_location is None:
            antenna_location = None
        elif not isinstance(self.antenna_location, Unset):
            antenna_location = self.antenna_location.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "tx": tx,
                "rx": rx,
                "min_freq": min_freq,
                "max_freq": max_freq,
                "max_power": max_power,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if radio_id is not UNSET:
            field_dict["radio_id"] = radio_id
        if device_id is not UNSET:
            field_dict["device_id"] = device_id
        if antenna_id is not UNSET:
            field_dict["antenna_id"] = antenna_id
        if antenna_location_id is not UNSET:
            field_dict["antenna_location_id"] = antenna_location_id
        if antenna_azimuth_angle is not UNSET:
            field_dict["antenna_azimuth_angle"] = antenna_azimuth_angle
        if antenna_elevation_angle is not UNSET:
            field_dict["antenna_elevation_angle"] = antenna_elevation_angle
        if antenna_elevation_delta is not UNSET:
            field_dict["antenna_elevation_delta"] = antenna_elevation_delta
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if antenna is not UNSET:
            field_dict["antenna"] = antenna
        if antenna_location is not UNSET:
            field_dict["antenna_location"] = antenna_location

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.antenna import Antenna
        from ..models.location import Location

        d = src_dict.copy()
        name = d.pop("name")

        tx = d.pop("tx")

        rx = d.pop("rx")

        min_freq = d.pop("min_freq")

        max_freq = d.pop("max_freq")

        max_power = d.pop("max_power")

        id = d.pop("id", UNSET)

        radio_id = d.pop("radio_id", UNSET)

        def _parse_device_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        device_id = _parse_device_id(d.pop("device_id", UNSET))

        def _parse_antenna_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        antenna_id = _parse_antenna_id(d.pop("antenna_id", UNSET))

        def _parse_antenna_location_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        antenna_location_id = _parse_antenna_location_id(
            d.pop("antenna_location_id", UNSET)
        )

        def _parse_antenna_azimuth_angle(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        antenna_azimuth_angle = _parse_antenna_azimuth_angle(
            d.pop("antenna_azimuth_angle", UNSET)
        )

        def _parse_antenna_elevation_angle(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        antenna_elevation_angle = _parse_antenna_elevation_angle(
            d.pop("antenna_elevation_angle", UNSET)
        )

        def _parse_antenna_elevation_delta(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        antenna_elevation_delta = _parse_antenna_elevation_delta(
            d.pop("antenna_elevation_delta", UNSET)
        )

        enabled = d.pop("enabled", UNSET)

        creator_id = d.pop("creator_id", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        _antenna = d.pop("antenna", UNSET)
        antenna: Union[None, Unset, Antenna]
        if isinstance(_antenna, Unset):
            antenna = UNSET
        elif _antenna is None:
            antenna = None
        else:
            antenna = Antenna.from_dict(_antenna)

        _antenna_location = d.pop("antenna_location", UNSET)
        antenna_location: Union[None, Unset, Location]
        if isinstance(_antenna_location, Unset):
            antenna_location = UNSET
        elif _antenna_location is None:
            antenna_location = None
        else:
            antenna_location = Location.from_dict(_antenna_location)

        radio_port = cls(
            name=name,
            tx=tx,
            rx=rx,
            min_freq=min_freq,
            max_freq=max_freq,
            max_power=max_power,
            id=id,
            radio_id=radio_id,
            device_id=device_id,
            antenna_id=antenna_id,
            antenna_location_id=antenna_location_id,
            antenna_azimuth_angle=antenna_azimuth_angle,
            antenna_elevation_angle=antenna_elevation_angle,
            antenna_elevation_delta=antenna_elevation_delta,
            enabled=enabled,
            creator_id=creator_id,
            created_at=created_at,
            updater_id=updater_id,
            updated_at=updated_at,
            deleted_at=deleted_at,
            antenna=antenna,
            antenna_location=antenna_location,
        )

        radio_port.additional_properties = d
        return radio_port

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
