import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Tardys3ScheduledEvent")


@_attrs_define
class Tardys3ScheduledEvent:
    """
    Attributes:
        event_id (str): Event ID
        dpa_id (str): DPA ID
        dpa_name (str): DPA Name
        date_time_start (datetime.datetime): Start time
        date_time_end (datetime.datetime): End time
        channels (List[str]): Channels
        recurrence (Union[None, Unset, str]): Recurrence
    """

    event_id: str
    dpa_id: str
    dpa_name: str
    date_time_start: datetime.datetime
    date_time_end: datetime.datetime
    channels: List[str]
    recurrence: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "eventId": {"put": None, "post": None},
        "dpaId": {"put": None, "post": None},
        "dpaName": {"put": None, "post": None},
        "dateTimeStart": {"put": None, "post": None},
        "dateTimeEnd": {"put": None, "post": None},
        "recurrence": {"put": None, "post": None},
        "channels": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        event_id = self.event_id

        dpa_id = self.dpa_id

        dpa_name = self.dpa_name

        date_time_start = self.date_time_start.isoformat()

        date_time_end = self.date_time_end.isoformat()

        channels = self.channels

        recurrence: Union[None, Unset, str]
        if isinstance(self.recurrence, Unset):
            recurrence = UNSET
        else:
            recurrence = self.recurrence

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "eventId": event_id,
                "dpaId": dpa_id,
                "dpaName": dpa_name,
                "dateTimeStart": date_time_start,
                "dateTimeEnd": date_time_end,
                "channels": channels,
            }
        )
        if recurrence is not UNSET:
            field_dict["recurrence"] = recurrence

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        event_id = d.pop("eventId")

        dpa_id = d.pop("dpaId")

        dpa_name = d.pop("dpaName")

        date_time_start = isoparse(d.pop("dateTimeStart"))

        date_time_end = isoparse(d.pop("dateTimeEnd"))

        channels = cast(List[str], d.pop("channels"))

        def _parse_recurrence(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        recurrence = _parse_recurrence(d.pop("recurrence", UNSET))

        tardys_3_scheduled_event = cls(
            event_id=event_id,
            dpa_id=dpa_id,
            dpa_name=dpa_name,
            date_time_start=date_time_start,
            date_time_end=date_time_end,
            channels=channels,
            recurrence=recurrence,
        )

        tardys_3_scheduled_event.additional_properties = d
        return tardys_3_scheduled_event

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
