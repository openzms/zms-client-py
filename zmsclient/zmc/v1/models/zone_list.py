from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.zone import Zone


T = TypeVar("T", bound="ZoneList")


@_attrs_define
class ZoneList:
    """
    Attributes:
        page (Union[Unset, int]): The current page of results.
        total (Union[Unset, int]): The total number of records available.
        pages (Union[Unset, int]): The total number of pages available.
        zones (Union[Unset, List['Zone']]):
    """

    page: Union[Unset, int] = UNSET
    total: Union[Unset, int] = UNSET
    pages: Union[Unset, int] = UNSET
    zones: Union[Unset, List["Zone"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {}

    def to_dict(self) -> Dict[str, Any]:
        page = self.page

        total = self.total

        pages = self.pages

        zones: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.zones, Unset):
            zones = []
            for zones_item_data in self.zones:
                zones_item = zones_item_data.to_dict()
                zones.append(zones_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if page is not UNSET:
            field_dict["page"] = page
        if total is not UNSET:
            field_dict["total"] = total
        if pages is not UNSET:
            field_dict["pages"] = pages
        if zones is not UNSET:
            field_dict["zones"] = zones

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.zone import Zone

        d = src_dict.copy()
        page = d.pop("page", UNSET)

        total = d.pop("total", UNSET)

        pages = d.pop("pages", UNSET)

        zones = []
        _zones = d.pop("zones", UNSET)
        for zones_item_data in _zones or []:
            zones_item = Zone.from_dict(zones_item_data)

            zones.append(zones_item)

        zone_list = cls(
            page=page,
            total=total,
            pages=pages,
            zones=zones,
        )

        zone_list.additional_properties = d
        return zone_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
