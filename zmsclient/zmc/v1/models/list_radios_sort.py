from enum import Enum


class ListRadiosSort(str, Enum):
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    DEVICE_ID = "device_id"
    ELEMENT_ID = "element_id"
    ENABLED = "enabled"
    FCC_ID = "fcc_id"
    NAME = "name"
    SERIAL_NUMBER = "serial_number"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
