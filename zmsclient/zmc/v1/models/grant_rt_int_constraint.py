from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.rt_int_constraint import RtIntConstraint


T = TypeVar("T", bound="GrantRtIntConstraint")


@_attrs_define
class GrantRtIntConstraint:
    """Runtime interference constraint for a grant.

    Attributes:
        grant_id (Union[Unset, str]): The id of the spectrum grant.
        rt_int_constraint_id (Union[Unset, str]): The runtime interference constraint id associated with the grant.
        rt_int_constraint (Union[None, Unset, RtIntConstraint]): A runtime interference constraint.
    """

    grant_id: Union[Unset, str] = UNSET
    rt_int_constraint_id: Union[Unset, str] = UNSET
    rt_int_constraint: Union[None, Unset, "RtIntConstraint"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "grant_id": {"put": True, "post": None},
        "rt_int_constraint_id": {"put": True, "post": None},
        "rt_int_constraint": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        grant_id = self.grant_id

        rt_int_constraint_id = self.rt_int_constraint_id

        rt_int_constraint: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.rt_int_constraint is None:
            rt_int_constraint = None
        elif not isinstance(self.rt_int_constraint, Unset):
            rt_int_constraint = self.rt_int_constraint.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if grant_id is not UNSET:
            field_dict["grant_id"] = grant_id
        if rt_int_constraint_id is not UNSET:
            field_dict["rt_int_constraint_id"] = rt_int_constraint_id
        if rt_int_constraint is not UNSET:
            field_dict["rt_int_constraint"] = rt_int_constraint

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.rt_int_constraint import RtIntConstraint

        d = src_dict.copy()
        grant_id = d.pop("grant_id", UNSET)

        rt_int_constraint_id = d.pop("rt_int_constraint_id", UNSET)

        _rt_int_constraint = d.pop("rt_int_constraint", UNSET)
        rt_int_constraint: Union[None, Unset, RtIntConstraint]
        if isinstance(_rt_int_constraint, Unset):
            rt_int_constraint = UNSET
        elif _rt_int_constraint is None:
            rt_int_constraint = None
        else:
            rt_int_constraint = RtIntConstraint.from_dict(_rt_int_constraint)

        grant_rt_int_constraint = cls(
            grant_id=grant_id,
            rt_int_constraint_id=rt_int_constraint_id,
            rt_int_constraint=rt_int_constraint,
        )

        grant_rt_int_constraint.additional_properties = d
        return grant_rt_int_constraint

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
