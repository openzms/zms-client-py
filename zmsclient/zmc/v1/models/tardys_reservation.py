import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.tardys_3_scheduled_event import Tardys3ScheduledEvent
    from ..models.tardys_4_scheduled_event import Tardys4ScheduledEvent


T = TypeVar("T", bound="TardysReservation")


@_attrs_define
class TardysReservation:
    """
    Attributes:
        transaction_id (Union[Unset, str]): The Tardys ID; should be a random UUID.
        datetime_published (Union[Unset, datetime.datetime]): Publish time.
        datetime_created (Union[Unset, datetime.datetime]): Created time.
        checksum (Union[Unset, str]): CRC32 checksum of scheduledEvents array.
        scheduled_events (Union[Unset, List[Union['Tardys3ScheduledEvent', 'Tardys4ScheduledEvent']]]): A list of
            TardyS3 or TardyS4 scheduled events.
    """

    transaction_id: Union[Unset, str] = UNSET
    datetime_published: Union[Unset, datetime.datetime] = UNSET
    datetime_created: Union[Unset, datetime.datetime] = UNSET
    checksum: Union[Unset, str] = UNSET
    scheduled_events: Union[
        Unset, List[Union["Tardys3ScheduledEvent", "Tardys4ScheduledEvent"]]
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "transactionId": {"put": None, "post": None},
        "datetimePublished": {"put": None, "post": None},
        "datetimeCreated": {"put": None, "post": None},
        "checksum": {"put": None, "post": None},
        "scheduledEvents": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        from ..models.tardys_3_scheduled_event import Tardys3ScheduledEvent

        transaction_id = self.transaction_id

        datetime_published: Union[Unset, str] = UNSET
        if not isinstance(self.datetime_published, Unset):
            datetime_published = self.datetime_published.isoformat()

        datetime_created: Union[Unset, str] = UNSET
        if not isinstance(self.datetime_created, Unset):
            datetime_created = self.datetime_created.isoformat()

        checksum = self.checksum

        scheduled_events: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.scheduled_events, Unset):
            scheduled_events = []
            for scheduled_events_item_data in self.scheduled_events:
                scheduled_events_item: Dict[str, Any]
                if isinstance(scheduled_events_item_data, Tardys3ScheduledEvent):
                    scheduled_events_item = scheduled_events_item_data.to_dict()
                else:
                    scheduled_events_item = scheduled_events_item_data.to_dict()

                scheduled_events.append(scheduled_events_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if transaction_id is not UNSET:
            field_dict["transactionId"] = transaction_id
        if datetime_published is not UNSET:
            field_dict["datetimePublished"] = datetime_published
        if datetime_created is not UNSET:
            field_dict["datetimeCreated"] = datetime_created
        if checksum is not UNSET:
            field_dict["checksum"] = checksum
        if scheduled_events is not UNSET:
            field_dict["scheduledEvents"] = scheduled_events

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.tardys_3_scheduled_event import Tardys3ScheduledEvent
        from ..models.tardys_4_scheduled_event import Tardys4ScheduledEvent

        d = src_dict.copy()
        transaction_id = d.pop("transactionId", UNSET)

        _datetime_published = d.pop("datetimePublished", UNSET)
        datetime_published: Union[Unset, datetime.datetime]
        if isinstance(_datetime_published, Unset):
            datetime_published = UNSET
        else:
            datetime_published = isoparse(_datetime_published)

        _datetime_created = d.pop("datetimeCreated", UNSET)
        datetime_created: Union[Unset, datetime.datetime]
        if isinstance(_datetime_created, Unset):
            datetime_created = UNSET
        else:
            datetime_created = isoparse(_datetime_created)

        checksum = d.pop("checksum", UNSET)

        scheduled_events = []
        _scheduled_events = d.pop("scheduledEvents", UNSET)
        for scheduled_events_item_data in _scheduled_events or []:

            def _parse_scheduled_events_item(
                data: object,
            ) -> Union["Tardys3ScheduledEvent", "Tardys4ScheduledEvent"]:
                try:
                    if not isinstance(data, dict):
                        raise TypeError()
                    scheduled_events_item_type_0 = Tardys3ScheduledEvent.from_dict(data)

                    return scheduled_events_item_type_0
                except:  # noqa: E722
                    pass
                if not isinstance(data, dict):
                    raise TypeError()
                scheduled_events_item_type_1 = Tardys4ScheduledEvent.from_dict(data)

                return scheduled_events_item_type_1

            scheduled_events_item = _parse_scheduled_events_item(
                scheduled_events_item_data
            )

            scheduled_events.append(scheduled_events_item)

        tardys_reservation = cls(
            transaction_id=transaction_id,
            datetime_published=datetime_published,
            datetime_created=datetime_created,
            checksum=checksum,
            scheduled_events=scheduled_events,
        )

        tardys_reservation.additional_properties = d
        return tardys_reservation

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
