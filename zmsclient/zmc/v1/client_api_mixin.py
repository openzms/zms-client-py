import functools

from .api.antenna.create_antenna import sync_detailed as create_antenna_sync_detailed
from .api.antenna.delete_antenna import sync_detailed as delete_antenna_sync_detailed
from .api.antenna.get_antenna import sync_detailed as get_antenna_sync_detailed
from .api.antenna.list_antennas import sync_detailed as list_antennas_sync_detailed
from .api.antenna.update_antenna import sync_detailed as update_antenna_sync_detailed
from .api.claim.create_claim import sync_detailed as create_claim_sync_detailed
from .api.claim.delete_claim import sync_detailed as delete_claim_sync_detailed
from .api.claim.get_claim import sync_detailed as get_claim_sync_detailed
from .api.claim.list_claims import sync_detailed as list_claims_sync_detailed
from .api.claim.update_claim import sync_detailed as update_claim_sync_detailed
from .api.grant.create_grant import sync_detailed as create_grant_sync_detailed
from .api.grant.create_grant_constraint import (
    sync_detailed as create_grant_constraint_sync_detailed,
)
from .api.grant.create_grant_int_constraint import (
    sync_detailed as create_grant_int_constraint_sync_detailed,
)
from .api.grant.create_grant_replacement import (
    sync_detailed as create_grant_replacement_sync_detailed,
)
from .api.grant.create_grant_rt_int_constraint import (
    sync_detailed as create_grant_rt_int_constraint_sync_detailed,
)
from .api.grant.create_tardys_reservation import (
    sync_detailed as create_tardys_reservation_sync_detailed,
)
from .api.grant.delete_grant import sync_detailed as delete_grant_sync_detailed
from .api.grant.get_grant import sync_detailed as get_grant_sync_detailed
from .api.grant.list_grants import sync_detailed as list_grants_sync_detailed
from .api.grant.replace_grant import sync_detailed as replace_grant_sync_detailed
from .api.grant.update_grant import sync_detailed as update_grant_sync_detailed
from .api.grant.update_grant_op_status import (
    sync_detailed as update_grant_op_status_sync_detailed,
)
from .api.health.get_alive import sync_detailed as get_alive_sync_detailed
from .api.health.get_ready import sync_detailed as get_ready_sync_detailed
from .api.location.create_location import sync_detailed as create_location_sync_detailed
from .api.location.delete_location import sync_detailed as delete_location_sync_detailed
from .api.location.get_location import sync_detailed as get_location_sync_detailed
from .api.location.list_locations import sync_detailed as list_locations_sync_detailed
from .api.location.update_location import sync_detailed as update_location_sync_detailed
from .api.monitor.create_monitor import sync_detailed as create_monitor_sync_detailed
from .api.monitor.delete_monitor import sync_detailed as delete_monitor_sync_detailed
from .api.monitor.get_monitor import sync_detailed as get_monitor_sync_detailed
from .api.monitor.list_monitors import sync_detailed as list_monitors_sync_detailed
from .api.monitor.update_monitor import sync_detailed as update_monitor_sync_detailed
from .api.radio.create_radio import sync_detailed as create_radio_sync_detailed
from .api.radio.delete_radio import sync_detailed as delete_radio_sync_detailed
from .api.radio.get_radio import sync_detailed as get_radio_sync_detailed
from .api.radio.list_radios import sync_detailed as list_radios_sync_detailed
from .api.radio.update_radio import sync_detailed as update_radio_sync_detailed
from .api.radio_port.create_radio_port import (
    sync_detailed as create_radio_port_sync_detailed,
)
from .api.radio_port.delete_radio_port import (
    sync_detailed as delete_radio_port_sync_detailed,
)
from .api.radio_port.get_radio_port import sync_detailed as get_radio_port_sync_detailed
from .api.radio_port.list_radio_ports import (
    sync_detailed as list_radio_ports_sync_detailed,
)
from .api.radio_port.update_radio_port import (
    sync_detailed as update_radio_port_sync_detailed,
)
from .api.spectrum.create_spectrum import sync_detailed as create_spectrum_sync_detailed
from .api.spectrum.create_spectrum_constraint import (
    sync_detailed as create_spectrum_constraint_sync_detailed,
)
from .api.spectrum.create_spectrum_policy import (
    sync_detailed as create_spectrum_policy_sync_detailed,
)
from .api.spectrum.delete_spectrum import sync_detailed as delete_spectrum_sync_detailed
from .api.spectrum.delete_spectrum_constraint import (
    sync_detailed as delete_spectrum_constraint_sync_detailed,
)
from .api.spectrum.delete_spectrum_policy import (
    sync_detailed as delete_spectrum_policy_sync_detailed,
)
from .api.spectrum.get_spectrum import sync_detailed as get_spectrum_sync_detailed
from .api.spectrum.get_spectrum_constraint import (
    sync_detailed as get_spectrum_constraint_sync_detailed,
)
from .api.spectrum.get_spectrum_policy import (
    sync_detailed as get_spectrum_policy_sync_detailed,
)
from .api.spectrum.list_spectrum import sync_detailed as list_spectrum_sync_detailed
from .api.spectrum.update_spectrum import sync_detailed as update_spectrum_sync_detailed
from .api.spectrum.update_spectrum_constraint import (
    sync_detailed as update_spectrum_constraint_sync_detailed,
)
from .api.spectrum.update_spectrum_policy import (
    sync_detailed as update_spectrum_policy_sync_detailed,
)
from .api.subscription.create_subscription import (
    sync_detailed as create_subscription_sync_detailed,
)
from .api.subscription.delete_subscription import (
    sync_detailed as delete_subscription_sync_detailed,
)
from .api.subscription.get_subscription_events import (
    sync_detailed as get_subscription_events_sync_detailed,
)
from .api.subscription.list_subscriptions import (
    sync_detailed as list_subscriptions_sync_detailed,
)
from .api.version.get_version import sync_detailed as get_version_sync_detailed


class ClientApiMixin:
    def _call_api_func(self, func, *args, **kwargs):
        return func(*args, **kwargs, client=self)

    def get_version(self, *args, **kwargs):
        return self._call_api_func(get_version_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_version, wrapped=get_version_sync_detailed)

    def get_alive(self, *args, **kwargs):
        return self._call_api_func(get_alive_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_alive, wrapped=get_alive_sync_detailed)

    def get_ready(self, *args, **kwargs):
        return self._call_api_func(get_ready_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_ready, wrapped=get_ready_sync_detailed)

    def list_spectrum(self, *args, **kwargs):
        return self._call_api_func(list_spectrum_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_spectrum, wrapped=list_spectrum_sync_detailed)

    def create_spectrum(self, *args, **kwargs):
        return self._call_api_func(create_spectrum_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_spectrum, wrapped=create_spectrum_sync_detailed)

    def get_spectrum(self, *args, **kwargs):
        return self._call_api_func(get_spectrum_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_spectrum, wrapped=get_spectrum_sync_detailed)

    def update_spectrum(self, *args, **kwargs):
        return self._call_api_func(update_spectrum_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_spectrum, wrapped=update_spectrum_sync_detailed)

    def delete_spectrum(self, *args, **kwargs):
        return self._call_api_func(delete_spectrum_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_spectrum, wrapped=delete_spectrum_sync_detailed)

    def create_spectrum_constraint(self, *args, **kwargs):
        return self._call_api_func(
            create_spectrum_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_spectrum_constraint, wrapped=create_spectrum_constraint_sync_detailed
    )

    def get_spectrum_constraint(self, *args, **kwargs):
        return self._call_api_func(
            get_spectrum_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        get_spectrum_constraint, wrapped=get_spectrum_constraint_sync_detailed
    )

    def update_spectrum_constraint(self, *args, **kwargs):
        return self._call_api_func(
            update_spectrum_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        update_spectrum_constraint, wrapped=update_spectrum_constraint_sync_detailed
    )

    def delete_spectrum_constraint(self, *args, **kwargs):
        return self._call_api_func(
            delete_spectrum_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        delete_spectrum_constraint, wrapped=delete_spectrum_constraint_sync_detailed
    )

    def create_spectrum_policy(self, *args, **kwargs):
        return self._call_api_func(
            create_spectrum_policy_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_spectrum_policy, wrapped=create_spectrum_policy_sync_detailed
    )

    def get_spectrum_policy(self, *args, **kwargs):
        return self._call_api_func(get_spectrum_policy_sync_detailed, *args, **kwargs)

    functools.update_wrapper(
        get_spectrum_policy, wrapped=get_spectrum_policy_sync_detailed
    )

    def update_spectrum_policy(self, *args, **kwargs):
        return self._call_api_func(
            update_spectrum_policy_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        update_spectrum_policy, wrapped=update_spectrum_policy_sync_detailed
    )

    def delete_spectrum_policy(self, *args, **kwargs):
        return self._call_api_func(
            delete_spectrum_policy_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        delete_spectrum_policy, wrapped=delete_spectrum_policy_sync_detailed
    )

    def list_locations(self, *args, **kwargs):
        return self._call_api_func(list_locations_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_locations, wrapped=list_locations_sync_detailed)

    def create_location(self, *args, **kwargs):
        return self._call_api_func(create_location_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_location, wrapped=create_location_sync_detailed)

    def get_location(self, *args, **kwargs):
        return self._call_api_func(get_location_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_location, wrapped=get_location_sync_detailed)

    def update_location(self, *args, **kwargs):
        return self._call_api_func(update_location_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_location, wrapped=update_location_sync_detailed)

    def delete_location(self, *args, **kwargs):
        return self._call_api_func(delete_location_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_location, wrapped=delete_location_sync_detailed)

    def list_radios(self, *args, **kwargs):
        return self._call_api_func(list_radios_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_radios, wrapped=list_radios_sync_detailed)

    def create_radio(self, *args, **kwargs):
        return self._call_api_func(create_radio_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_radio, wrapped=create_radio_sync_detailed)

    def get_radio(self, *args, **kwargs):
        return self._call_api_func(get_radio_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_radio, wrapped=get_radio_sync_detailed)

    def update_radio(self, *args, **kwargs):
        return self._call_api_func(update_radio_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_radio, wrapped=update_radio_sync_detailed)

    def delete_radio(self, *args, **kwargs):
        return self._call_api_func(delete_radio_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_radio, wrapped=delete_radio_sync_detailed)

    def list_radio_ports(self, *args, **kwargs):
        return self._call_api_func(list_radio_ports_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_radio_ports, wrapped=list_radio_ports_sync_detailed)

    def create_radio_port(self, *args, **kwargs):
        return self._call_api_func(create_radio_port_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_radio_port, wrapped=create_radio_port_sync_detailed)

    def get_radio_port(self, *args, **kwargs):
        return self._call_api_func(get_radio_port_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_radio_port, wrapped=get_radio_port_sync_detailed)

    def update_radio_port(self, *args, **kwargs):
        return self._call_api_func(update_radio_port_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_radio_port, wrapped=update_radio_port_sync_detailed)

    def delete_radio_port(self, *args, **kwargs):
        return self._call_api_func(delete_radio_port_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_radio_port, wrapped=delete_radio_port_sync_detailed)

    def list_antennas(self, *args, **kwargs):
        return self._call_api_func(list_antennas_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_antennas, wrapped=list_antennas_sync_detailed)

    def create_antenna(self, *args, **kwargs):
        return self._call_api_func(create_antenna_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_antenna, wrapped=create_antenna_sync_detailed)

    def get_antenna(self, *args, **kwargs):
        return self._call_api_func(get_antenna_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_antenna, wrapped=get_antenna_sync_detailed)

    def update_antenna(self, *args, **kwargs):
        return self._call_api_func(update_antenna_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_antenna, wrapped=update_antenna_sync_detailed)

    def delete_antenna(self, *args, **kwargs):
        return self._call_api_func(delete_antenna_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_antenna, wrapped=delete_antenna_sync_detailed)

    def list_grants(self, *args, **kwargs):
        return self._call_api_func(list_grants_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_grants, wrapped=list_grants_sync_detailed)

    def create_grant(self, *args, **kwargs):
        return self._call_api_func(create_grant_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_grant, wrapped=create_grant_sync_detailed)

    def get_grant(self, *args, **kwargs):
        return self._call_api_func(get_grant_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_grant, wrapped=get_grant_sync_detailed)

    def update_grant(self, *args, **kwargs):
        return self._call_api_func(update_grant_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_grant, wrapped=update_grant_sync_detailed)

    def replace_grant(self, *args, **kwargs):
        return self._call_api_func(replace_grant_sync_detailed, *args, **kwargs)

    functools.update_wrapper(replace_grant, wrapped=replace_grant_sync_detailed)

    def delete_grant(self, *args, **kwargs):
        return self._call_api_func(delete_grant_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_grant, wrapped=delete_grant_sync_detailed)

    def create_grant_replacement(self, *args, **kwargs):
        return self._call_api_func(
            create_grant_replacement_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_grant_replacement, wrapped=create_grant_replacement_sync_detailed
    )

    def create_grant_constraint(self, *args, **kwargs):
        return self._call_api_func(
            create_grant_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_grant_constraint, wrapped=create_grant_constraint_sync_detailed
    )

    def create_grant_int_constraint(self, *args, **kwargs):
        return self._call_api_func(
            create_grant_int_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_grant_int_constraint, wrapped=create_grant_int_constraint_sync_detailed
    )

    def create_grant_rt_int_constraint(self, *args, **kwargs):
        return self._call_api_func(
            create_grant_rt_int_constraint_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_grant_rt_int_constraint,
        wrapped=create_grant_rt_int_constraint_sync_detailed,
    )

    def update_grant_op_status(self, *args, **kwargs):
        return self._call_api_func(
            update_grant_op_status_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        update_grant_op_status, wrapped=update_grant_op_status_sync_detailed
    )

    def create_tardys_reservation(self, *args, **kwargs):
        return self._call_api_func(
            create_tardys_reservation_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_tardys_reservation, wrapped=create_tardys_reservation_sync_detailed
    )

    def list_claims(self, *args, **kwargs):
        return self._call_api_func(list_claims_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_claims, wrapped=list_claims_sync_detailed)

    def create_claim(self, *args, **kwargs):
        return self._call_api_func(create_claim_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_claim, wrapped=create_claim_sync_detailed)

    def get_claim(self, *args, **kwargs):
        return self._call_api_func(get_claim_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_claim, wrapped=get_claim_sync_detailed)

    def update_claim(self, *args, **kwargs):
        return self._call_api_func(update_claim_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_claim, wrapped=update_claim_sync_detailed)

    def delete_claim(self, *args, **kwargs):
        return self._call_api_func(delete_claim_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_claim, wrapped=delete_claim_sync_detailed)

    def list_monitors(self, *args, **kwargs):
        return self._call_api_func(list_monitors_sync_detailed, *args, **kwargs)

    functools.update_wrapper(list_monitors, wrapped=list_monitors_sync_detailed)

    def create_monitor(self, *args, **kwargs):
        return self._call_api_func(create_monitor_sync_detailed, *args, **kwargs)

    functools.update_wrapper(create_monitor, wrapped=create_monitor_sync_detailed)

    def get_monitor(self, *args, **kwargs):
        return self._call_api_func(get_monitor_sync_detailed, *args, **kwargs)

    functools.update_wrapper(get_monitor, wrapped=get_monitor_sync_detailed)

    def update_monitor(self, *args, **kwargs):
        return self._call_api_func(update_monitor_sync_detailed, *args, **kwargs)

    functools.update_wrapper(update_monitor, wrapped=update_monitor_sync_detailed)

    def delete_monitor(self, *args, **kwargs):
        return self._call_api_func(delete_monitor_sync_detailed, *args, **kwargs)

    functools.update_wrapper(delete_monitor, wrapped=delete_monitor_sync_detailed)

    def list_subscriptions(self, *args, **kwargs):
        return self._call_api_func(list_subscriptions_sync_detailed, *args, **kwargs)

    functools.update_wrapper(
        list_subscriptions, wrapped=list_subscriptions_sync_detailed
    )

    def create_subscription(self, *args, **kwargs):
        return self._call_api_func(create_subscription_sync_detailed, *args, **kwargs)

    functools.update_wrapper(
        create_subscription, wrapped=create_subscription_sync_detailed
    )

    def delete_subscription(self, *args, **kwargs):
        return self._call_api_func(delete_subscription_sync_detailed, *args, **kwargs)

    functools.update_wrapper(
        delete_subscription, wrapped=delete_subscription_sync_detailed
    )

    def get_subscription_events(self, *args, **kwargs):
        return self._call_api_func(
            get_subscription_events_sync_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        get_subscription_events, wrapped=get_subscription_events_sync_detailed
    )
