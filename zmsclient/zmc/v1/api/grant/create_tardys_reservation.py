from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.grant_list import GrantList
from ...models.tardys_reservation import TardysReservation
from ...types import Response


def _get_kwargs(
    *,
    body: TardysReservation,
    x_api_token: str,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": "/tardys",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, GrantList]]:
    if response.status_code == HTTPStatus.CREATED:
        response_201 = GrantList.from_dict(response.json())

        return response_201
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, GrantList]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: TardysReservation,
    x_api_token: str,
) -> Response[Union[Error, GrantList]]:
    """Create grant via a TardyS3 or TardyS4 reservation.

    Args:
        x_api_token (str):
        body (TardysReservation):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, GrantList]]
    """

    kwargs = _get_kwargs(
        body=body,
        x_api_token=x_api_token,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    body: TardysReservation,
    x_api_token: str,
) -> Optional[Union[Error, GrantList]]:
    """Create grant via a TardyS3 or TardyS4 reservation.

    Args:
        x_api_token (str):
        body (TardysReservation):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, GrantList]
    """

    return sync_detailed(
        client=client,
        body=body,
        x_api_token=x_api_token,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: TardysReservation,
    x_api_token: str,
) -> Response[Union[Error, GrantList]]:
    """Create grant via a TardyS3 or TardyS4 reservation.

    Args:
        x_api_token (str):
        body (TardysReservation):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, GrantList]]
    """

    kwargs = _get_kwargs(
        body=body,
        x_api_token=x_api_token,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    body: TardysReservation,
    x_api_token: str,
) -> Optional[Union[Error, GrantList]]:
    """Create grant via a TardyS3 or TardyS4 reservation.

    Args:
        x_api_token (str):
        body (TardysReservation):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, GrantList]
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
            x_api_token=x_api_token,
        )
    ).parsed
