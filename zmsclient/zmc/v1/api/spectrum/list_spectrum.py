from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.list_spectrum_sort import ListSpectrumSort
from ...models.spectrum_list import SpectrumList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    element_id: Union[Unset, str] = UNSET,
    spectrum: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    approved: Union[Unset, bool] = UNSET,
    denied: Union[Unset, bool] = False,
    deleted: Union[Unset, bool] = False,
    current: Union[Unset, bool] = UNSET,
    sort: Union[Unset, ListSpectrumSort] = UNSET,
    sort_asc: Union[Unset, bool] = False,
    page: Union[Unset, int] = UNSET,
    items_per_page: Union[Unset, int] = UNSET,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    if not isinstance(x_api_token, Unset):
        headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    params: Dict[str, Any] = {}

    params["element_id"] = element_id

    params["spectrum"] = spectrum

    params["enabled"] = enabled

    params["approved"] = approved

    params["denied"] = denied

    params["deleted"] = deleted

    params["current"] = current

    json_sort: Union[Unset, str] = UNSET
    if not isinstance(sort, Unset):
        json_sort = sort.value

    params["sort"] = json_sort

    params["sort_asc"] = sort_asc

    params["page"] = page

    params["items_per_page"] = items_per_page

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/spectrum",
        "params": params,
    }

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, SpectrumList]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = SpectrumList.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, SpectrumList]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    element_id: Union[Unset, str] = UNSET,
    spectrum: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    approved: Union[Unset, bool] = UNSET,
    denied: Union[Unset, bool] = False,
    deleted: Union[Unset, bool] = False,
    current: Union[Unset, bool] = UNSET,
    sort: Union[Unset, ListSpectrumSort] = UNSET,
    sort_asc: Union[Unset, bool] = False,
    page: Union[Unset, int] = UNSET,
    items_per_page: Union[Unset, int] = UNSET,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, SpectrumList]]:
    """Retrieve a list of spectrum ranges managed by the ZMS.

    Args:
        element_id (Union[Unset, str]):
        spectrum (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        approved (Union[Unset, bool]):
        denied (Union[Unset, bool]):  Default: False.
        deleted (Union[Unset, bool]):  Default: False.
        current (Union[Unset, bool]):
        sort (Union[Unset, ListSpectrumSort]):
        sort_asc (Union[Unset, bool]):  Default: False.
        page (Union[Unset, int]):
        items_per_page (Union[Unset, int]):
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, SpectrumList]]
    """

    kwargs = _get_kwargs(
        element_id=element_id,
        spectrum=spectrum,
        enabled=enabled,
        approved=approved,
        denied=denied,
        deleted=deleted,
        current=current,
        sort=sort,
        sort_asc=sort_asc,
        page=page,
        items_per_page=items_per_page,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    element_id: Union[Unset, str] = UNSET,
    spectrum: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    approved: Union[Unset, bool] = UNSET,
    denied: Union[Unset, bool] = False,
    deleted: Union[Unset, bool] = False,
    current: Union[Unset, bool] = UNSET,
    sort: Union[Unset, ListSpectrumSort] = UNSET,
    sort_asc: Union[Unset, bool] = False,
    page: Union[Unset, int] = UNSET,
    items_per_page: Union[Unset, int] = UNSET,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, SpectrumList]]:
    """Retrieve a list of spectrum ranges managed by the ZMS.

    Args:
        element_id (Union[Unset, str]):
        spectrum (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        approved (Union[Unset, bool]):
        denied (Union[Unset, bool]):  Default: False.
        deleted (Union[Unset, bool]):  Default: False.
        current (Union[Unset, bool]):
        sort (Union[Unset, ListSpectrumSort]):
        sort_asc (Union[Unset, bool]):  Default: False.
        page (Union[Unset, int]):
        items_per_page (Union[Unset, int]):
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, SpectrumList]
    """

    return sync_detailed(
        client=client,
        element_id=element_id,
        spectrum=spectrum,
        enabled=enabled,
        approved=approved,
        denied=denied,
        deleted=deleted,
        current=current,
        sort=sort,
        sort_asc=sort_asc,
        page=page,
        items_per_page=items_per_page,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    element_id: Union[Unset, str] = UNSET,
    spectrum: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    approved: Union[Unset, bool] = UNSET,
    denied: Union[Unset, bool] = False,
    deleted: Union[Unset, bool] = False,
    current: Union[Unset, bool] = UNSET,
    sort: Union[Unset, ListSpectrumSort] = UNSET,
    sort_asc: Union[Unset, bool] = False,
    page: Union[Unset, int] = UNSET,
    items_per_page: Union[Unset, int] = UNSET,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, SpectrumList]]:
    """Retrieve a list of spectrum ranges managed by the ZMS.

    Args:
        element_id (Union[Unset, str]):
        spectrum (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        approved (Union[Unset, bool]):
        denied (Union[Unset, bool]):  Default: False.
        deleted (Union[Unset, bool]):  Default: False.
        current (Union[Unset, bool]):
        sort (Union[Unset, ListSpectrumSort]):
        sort_asc (Union[Unset, bool]):  Default: False.
        page (Union[Unset, int]):
        items_per_page (Union[Unset, int]):
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, SpectrumList]]
    """

    kwargs = _get_kwargs(
        element_id=element_id,
        spectrum=spectrum,
        enabled=enabled,
        approved=approved,
        denied=denied,
        deleted=deleted,
        current=current,
        sort=sort,
        sort_asc=sort_asc,
        page=page,
        items_per_page=items_per_page,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    element_id: Union[Unset, str] = UNSET,
    spectrum: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    approved: Union[Unset, bool] = UNSET,
    denied: Union[Unset, bool] = False,
    deleted: Union[Unset, bool] = False,
    current: Union[Unset, bool] = UNSET,
    sort: Union[Unset, ListSpectrumSort] = UNSET,
    sort_asc: Union[Unset, bool] = False,
    page: Union[Unset, int] = UNSET,
    items_per_page: Union[Unset, int] = UNSET,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, SpectrumList]]:
    """Retrieve a list of spectrum ranges managed by the ZMS.

    Args:
        element_id (Union[Unset, str]):
        spectrum (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        approved (Union[Unset, bool]):
        denied (Union[Unset, bool]):  Default: False.
        deleted (Union[Unset, bool]):  Default: False.
        current (Union[Unset, bool]):
        sort (Union[Unset, ListSpectrumSort]):
        sort_asc (Union[Unset, bool]):  Default: False.
        page (Union[Unset, int]):
        items_per_page (Union[Unset, int]):
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, SpectrumList]
    """

    return (
        await asyncio_detailed(
            client=client,
            element_id=element_id,
            spectrum=spectrum,
            enabled=enabled,
            approved=approved,
            denied=denied,
            deleted=deleted,
            current=current,
            sort=sort,
            sort_asc=sort_asc,
            page=page,
            items_per_page=items_per_page,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
        )
    ).parsed
