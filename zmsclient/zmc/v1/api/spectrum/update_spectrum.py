from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.spectrum import Spectrum
from ...types import UNSET, Response, Unset


def _get_kwargs(
    spectrum_id: str,
    *,
    body: Spectrum,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    if not isinstance(x_api_force_update, Unset):
        headers["X-Api-Force-Update"] = x_api_force_update

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/spectrum/{spectrum_id}".format(
            spectrum_id=spectrum_id,
        ),
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, Spectrum]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Spectrum.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.CONFLICT:
        response_409 = Error.from_dict(response.json())

        return response_409
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, Spectrum]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    spectrum_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Spectrum,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Spectrum]]:
    """Update spectrum details.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        x_api_force_update (Union[Unset, str]):  Example: true.
        body (Spectrum): A constrained frequency range provided to the ZMS as shareable, managed
            spectrum by one Element.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Spectrum]]
    """

    kwargs = _get_kwargs(
        spectrum_id=spectrum_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        x_api_force_update=x_api_force_update,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    spectrum_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Spectrum,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Spectrum]]:
    """Update spectrum details.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        x_api_force_update (Union[Unset, str]):  Example: true.
        body (Spectrum): A constrained frequency range provided to the ZMS as shareable, managed
            spectrum by one Element.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Spectrum]
    """

    return sync_detailed(
        spectrum_id=spectrum_id,
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        x_api_force_update=x_api_force_update,
    ).parsed


async def asyncio_detailed(
    spectrum_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Spectrum,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Spectrum]]:
    """Update spectrum details.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        x_api_force_update (Union[Unset, str]):  Example: true.
        body (Spectrum): A constrained frequency range provided to the ZMS as shareable, managed
            spectrum by one Element.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Spectrum]]
    """

    kwargs = _get_kwargs(
        spectrum_id=spectrum_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        x_api_force_update=x_api_force_update,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    spectrum_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Spectrum,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Spectrum]]:
    """Update spectrum details.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        x_api_force_update (Union[Unset, str]):  Example: true.
        body (Spectrum): A constrained frequency range provided to the ZMS as shareable, managed
            spectrum by one Element.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Spectrum]
    """

    return (
        await asyncio_detailed(
            spectrum_id=spectrum_id,
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
            x_api_force_update=x_api_force_update,
        )
    ).parsed
