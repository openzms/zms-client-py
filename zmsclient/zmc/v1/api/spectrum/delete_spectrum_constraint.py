from http import HTTPStatus
from typing import Any, Dict, Optional, Union, cast

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...types import UNSET, Response, Unset


def _get_kwargs(
    spectrum_id: str,
    constraint_id: str,
    *,
    x_api_token: str,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_force_update, Unset):
        headers["X-Api-Force-Update"] = x_api_force_update

    _kwargs: Dict[str, Any] = {
        "method": "delete",
        "url": "/spectrum/{spectrum_id}/constraints/{constraint_id}".format(
            spectrum_id=spectrum_id,
            constraint_id=constraint_id,
        ),
    }

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Any, Error]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = cast(Any, None)
        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.CONFLICT:
        response_409 = Error.from_dict(response.json())

        return response_409
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Any, Error]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    spectrum_id: str,
    constraint_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Response[Union[Any, Error]]:
    """Delete spectrum constraint.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        constraint_id (str):
        x_api_token (str):
        x_api_force_update (Union[Unset, str]):  Example: true.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, Error]]
    """

    kwargs = _get_kwargs(
        spectrum_id=spectrum_id,
        constraint_id=constraint_id,
        x_api_token=x_api_token,
        x_api_force_update=x_api_force_update,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    spectrum_id: str,
    constraint_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Optional[Union[Any, Error]]:
    """Delete spectrum constraint.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        constraint_id (str):
        x_api_token (str):
        x_api_force_update (Union[Unset, str]):  Example: true.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, Error]
    """

    return sync_detailed(
        spectrum_id=spectrum_id,
        constraint_id=constraint_id,
        client=client,
        x_api_token=x_api_token,
        x_api_force_update=x_api_force_update,
    ).parsed


async def asyncio_detailed(
    spectrum_id: str,
    constraint_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Response[Union[Any, Error]]:
    """Delete spectrum constraint.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        constraint_id (str):
        x_api_token (str):
        x_api_force_update (Union[Unset, str]):  Example: true.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Any, Error]]
    """

    kwargs = _get_kwargs(
        spectrum_id=spectrum_id,
        constraint_id=constraint_id,
        x_api_token=x_api_token,
        x_api_force_update=x_api_force_update,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    spectrum_id: str,
    constraint_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_force_update: Union[Unset, str] = UNSET,
) -> Optional[Union[Any, Error]]:
    """Delete spectrum constraint.  May fail if spectrum is in use or scheduled; override by setting the
    X-Api-Force-Update header to true.

    Args:
        spectrum_id (str):
        constraint_id (str):
        x_api_token (str):
        x_api_force_update (Union[Unset, str]):  Example: true.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Any, Error]
    """

    return (
        await asyncio_detailed(
            spectrum_id=spectrum_id,
            constraint_id=constraint_id,
            client=client,
            x_api_token=x_api_token,
            x_api_force_update=x_api_force_update,
        )
    ).parsed
