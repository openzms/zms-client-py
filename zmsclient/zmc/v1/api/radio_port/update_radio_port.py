from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.radio_port import RadioPort
from ...types import UNSET, Response, Unset


def _get_kwargs(
    radio_id: str,
    radio_port_id: str,
    *,
    body: RadioPort,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/radios/{radio_id}/ports/{radio_port_id}".format(
            radio_id=radio_id,
            radio_port_id=radio_port_id,
        ),
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, RadioPort]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = RadioPort.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.CONFLICT:
        response_409 = Error.from_dict(response.json())

        return response_409
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, RadioPort]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    radio_id: str,
    radio_port_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RadioPort,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, RadioPort]]:
    """Update radio port details.

    Args:
        radio_id (str):
        radio_port_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RadioPort): Defines a transmitter, receiver, or transceiver by binding an antenna to
            an RF connector on a radio, and describing its location.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RadioPort]]
    """

    kwargs = _get_kwargs(
        radio_id=radio_id,
        radio_port_id=radio_port_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    radio_id: str,
    radio_port_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RadioPort,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, RadioPort]]:
    """Update radio port details.

    Args:
        radio_id (str):
        radio_port_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RadioPort): Defines a transmitter, receiver, or transceiver by binding an antenna to
            an RF connector on a radio, and describing its location.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RadioPort]
    """

    return sync_detailed(
        radio_id=radio_id,
        radio_port_id=radio_port_id,
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    ).parsed


async def asyncio_detailed(
    radio_id: str,
    radio_port_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RadioPort,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, RadioPort]]:
    """Update radio port details.

    Args:
        radio_id (str):
        radio_port_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RadioPort): Defines a transmitter, receiver, or transceiver by binding an antenna to
            an RF connector on a radio, and describing its location.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RadioPort]]
    """

    kwargs = _get_kwargs(
        radio_id=radio_id,
        radio_port_id=radio_port_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    radio_id: str,
    radio_port_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RadioPort,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, RadioPort]]:
    """Update radio port details.

    Args:
        radio_id (str):
        radio_port_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RadioPort): Defines a transmitter, receiver, or transceiver by binding an antenna to
            an RF connector on a radio, and describing its location.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RadioPort]
    """

    return (
        await asyncio_detailed(
            radio_id=radio_id,
            radio_port_id=radio_port_id,
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
        )
    ).parsed
