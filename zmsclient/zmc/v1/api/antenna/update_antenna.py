from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.antenna import Antenna
from ...models.error import Error
from ...types import UNSET, Response, Unset


def _get_kwargs(
    antenna_id: str,
    *,
    body: Antenna,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/antennas/{antenna_id}".format(
            antenna_id=antenna_id,
        ),
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Antenna, Error]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Antenna.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Antenna, Error]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    antenna_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Antenna,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Antenna, Error]]:
    """Update antenna details.

    Args:
        antenna_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (Antenna): A description of an antenna (type, kind, gain profile, etc).  May be
            linked to a Radio via a RadioPort to model a transmitter or receiver.  This object
            describes the properties of an Antenna, not its physical deployment.  Those deployment
            properties are associated with one or more RadioPort.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Antenna, Error]]
    """

    kwargs = _get_kwargs(
        antenna_id=antenna_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    antenna_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Antenna,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Antenna, Error]]:
    """Update antenna details.

    Args:
        antenna_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (Antenna): A description of an antenna (type, kind, gain profile, etc).  May be
            linked to a Radio via a RadioPort to model a transmitter or receiver.  This object
            describes the properties of an Antenna, not its physical deployment.  Those deployment
            properties are associated with one or more RadioPort.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Antenna, Error]
    """

    return sync_detailed(
        antenna_id=antenna_id,
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    ).parsed


async def asyncio_detailed(
    antenna_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Antenna,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Antenna, Error]]:
    """Update antenna details.

    Args:
        antenna_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (Antenna): A description of an antenna (type, kind, gain profile, etc).  May be
            linked to a Radio via a RadioPort to model a transmitter or receiver.  This object
            describes the properties of an Antenna, not its physical deployment.  Those deployment
            properties are associated with one or more RadioPort.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Antenna, Error]]
    """

    kwargs = _get_kwargs(
        antenna_id=antenna_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    antenna_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: Antenna,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Antenna, Error]]:
    """Update antenna details.

    Args:
        antenna_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (Antenna): A description of an antenna (type, kind, gain profile, etc).  May be
            linked to a Radio via a RadioPort to model a transmitter or receiver.  This object
            describes the properties of an Antenna, not its physical deployment.  Those deployment
            properties are associated with one or more RadioPort.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Antenna, Error]
    """

    return (
        await asyncio_detailed(
            antenna_id=antenna_id,
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
        )
    ).parsed
