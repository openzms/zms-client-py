
from .v1.client import AuthenticatedClient
from .v1.client_api_mixin import ClientApiMixin

class ZmsZmcClient(AuthenticatedClient, ClientApiMixin):

    def _call_api_func(self, func, *args, **kwargs):
        if 'x_api_elaborate' not in kwargs and 'elaborate' in kwargs:
            kwargs['x_api_elaborate'] = str(kwargs['elaborate'])
            del kwargs['elaborate']
        kwargs['x_api_token'] = self.token
        result = ClientApiMixin._call_api_func(self, func, *args, **kwargs)
        if result == None or self._detailed == True:
            return result
        return result.parsed

    def __init__(self, base_url: str, token: str,
                 raise_on_unexpected_status: bool = False,
                 detailed: bool = True, **kwargs):
        self._detailed = detailed
        super(ZmsZmcClient, self).__init__(base_url=base_url, token=token, prefix="", auth_header_name="X-Api-Token", raise_on_unexpected_status=raise_on_unexpected_status, **kwargs)
