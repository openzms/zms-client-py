import uuid
import json
import traceback
import logging
import inspect
import importlib
import time
import asyncio
import websockets
from websockets.exceptions import (
    ConnectionClosed, ConnectionClosedError, ConnectionClosedOK )
try:
    # Support websockets>=11
    from websockets.asyncio.client import connect as websockets_asyncio_connect
    additional_headers_key = "additional_headers"
except:
    # Support websockets<=10
    from websockets import connect as websockets_asyncio_connect
    additional_headers_key = "extra_headers"

LOG = logging.getLogger(__name__)

def find_client_generated_pkg(client):
    for c in inspect.getmro(client.__class__):
        if c.__name__ in [ "AuthenticatedClient", "Client" ]:
            return inspect.getmodule(c).__package__
    raise Exception("cannot find generated pkg")

class ZmsSubscription:
    def __init__(self, zmsclient, subscription=None, raw=False, reconnect_on_error=False):
        self.zmsclient    = zmsclient
        self.subscription = subscription
        self.raw = raw
        self.reconnect_on_error = reconnect_on_error
        self._ws = None
        self._id = None
        self._zmsclient_pkg = find_client_generated_pkg(zmsclient)
        self._zmsclient_event_cls = importlib.import_module(self._zmsclient_pkg + '.models.event').Event

    @property
    def id(self) -> str:
        return self._id

    @property
    def subscribed(self) -> bool:
        return self._id != None

    @property
    def connected(self) -> bool:
        return self._ws

    def subscribe(self):
        if self._id is not None:
            raise FileExistsError("already subscribed")
        LOG.debug("subscribe: %r", self.subscription)
        self._id = self.subscription.id
        subscription = self.zmsclient.create_subscription(
            body=self.subscription)
        if not subscription:
            raise Exception("Unknown error subscribing to events")
        self.url = self.zmsclient._base_url + "/subscriptions/" + self._id + "/events"
        self.url = self.url.replace("http", "ws")
        self._headers = {
            "X-Api-Token"     : self.zmsclient.token,
            "X-Api-Elaborate" : "true"
        }
        
    def unsubscribe(self):
        if not self._id:
            raise FileNotFoundError("not subscribed")
        LOG.debug("unsubscribe: %r", self._id)
        self.zmsclient.delete_subscription(
            subscription_id=self._id)
        self._id = None

    def _parse_event(self, msg):
        LOG.debug("_parse_event: %r", msg)
        return self._zmsclient_event_cls.from_dict(src_dict=json.loads(msg))

    async def connect(self):
        if self._ws:
            raise FileExistsError("already connected")
        LOG.debug("connect: %r", self._id)
        self.url = self.zmsclient._base_url + "/subscriptions/" + self._id + "/events"
        self.url = self.url.replace("http", "ws")

        headers = { "X-Api-Token"     : self.zmsclient.token,
                    "X-Api-Elaborate" : "true"
        }
        # Make sure to be nice to the server; delete the subscription on
        # connection close/failure.
        if not self.reconnect_on_error:
            headers["X-Api-Delete-On-Close"] = "true"
        header_kwargs = { additional_headers_key: headers }
        tmp = websockets_asyncio_connect(
            self.url, **header_kwargs)
        self._ws = await tmp.create_connection()

    async def disconnect(self):
        if not self._ws:
            raise FileNotFoundError("not connected")
        LOG.debug("disconnect: %r", self._id)
        await self._ws.close()
        self._ws = None


class ZmsSubscriptionCallback(ZmsSubscription):

    async def run_callbacks(self):
        self.subscribe()
        header_kwargs = { additional_headers_key: self._headers }
        while True:
            async with websockets_asyncio_connect(
                self.url, **header_kwargs) as ws:
                self._ws = ws
            
                try:
                    self.on_open(self._ws)
                except Exception as ex:
                    LOG.exception(ex)
                    continue

                try:
                    while True:
                        msg = await self._ws.recv()
                        if not msg:
                            break
                        if self.raw:
                            self.on_message(self._ws, msg)
                        else:
                            self.on_event(self._ws, self._parse_event(msg), msg)
                except asyncio.CancelledError:
                    raise
                except TimeoutError:
                    if self._ws is None:
                        return
                except ConnectionClosed as ex:
                    if isinstance(ex, ConnectionClosedOK):
                        self.on_close(self._ws, ex.code, ex.reason, error=ex)
                        return
                    elif self.reconnect_on_error:
                        self.on_error(self._ws, ex.code, ex.reason, error=ex)
                        self._ws = None
                    pass
                pass
            # If we lose connection, keep trying.
            await asyncio.sleep(10)
            self.unsubscribe()
            self.subscribe()
            pass
        pass

    def on_message(self, ws, message):
        LOG.debug("websocket message: %r", message)

    def on_error(self, ws, code, reason, error=None):
        LOG.debug("websocket error: %r, %r (%r)", code, reason, error)

    def on_close(self, ws, code, reason, error=None):
        LOG.debug("websocket closed: %r, %r (%r)", code, reason, error)

    def on_open(self, ws):
        LOG.debug("websocket opened")
