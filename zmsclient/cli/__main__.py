import os
import sys
import http
import json
import logging
import importlib
import pkgutil
import inspect
import typing
import typer
import click
from typing_extensions import Annotated

from zmsclient.cli.output import get_default_maxwidth, pretty_print_result
from zmsclient.cli.typer import (
    wrap_api_func, is_zmsclient_single_body_model_func,
    find_zmsclient_unset_type)
from zmsclient.cli.context import ContextObj, OutputKind

LOG = logging.getLogger('zmsclient.cli')

# This typer callback registers global options and initializes the
# typer.Context `obj` attribute for later use.
#
# NB: we really want to mark `identity_url` and `token` as required, but if we
# do, the user cannot see subcommand help until they set those values.  So we
# have to throw errors later.  And technically, not all endpoints require them.
def app_init(
    ctx: typer.Context,
    debug: Annotated[bool, typer.Option(envvar="ZMSCLIENT_DEBUG", help="Print debugging info from `zmsclient` modules.")] = False,
    debug_all: Annotated[bool, typer.Option(envvar="ZMSCLIENT_DEBUG_ALL", help="Print debugging from Python root logger (everything).")] = False,
    include_raw: Annotated[bool, typer.Option(envvar="ZMSCLIENT_INCLUDE_RAW", help="Include *_raw subcommands for create/update calls that require a JSON object, in addition to their 'exploded' forms.")] = False,
    token: Annotated[str, typer.Option(envvar="ZMS_TOKEN", help="OpenZMS API token for authentication.")] = None,
    identity_url: Annotated[str, typer.Option(envvar="IDENTITY_HTTP", help="OpenZMS identity service URL (excluding version).")] = None,
    zmc_url: Annotated[str, typer.Option(envvar="ZMC_HTTP", help="OpenZMS zmc service URL (excluding version).")] = None,
    dst_url: Annotated[str, typer.Option(envvar="DST_HTTP", help="OpenZMS dst service URL (excluding version).")] = None,
    elaborate: Annotated[bool, typer.Option(help="Return elaborated objects with foreign key child relationships.")] = False,
    force: Annotated[bool, typer.Option(help="Force update or deletion even if dependent resources exist.")] = False,
    timeout: Annotated[float, typer.Option(help="Timeout for HTTP requests.")] = 5.0,
    follow_redirects: Annotated[bool, typer.Option(help="Follow HTTP redirects.")] = True,
    raise_on_unexpected_status: Annotated[bool, typer.Option(help="Raise an error on unexpected HTTP return status.")] = True,
    verify_ssl: Annotated[bool, typer.Option(help="Enable SSL verification")] = True,
    output: Annotated[OutputKind, typer.Option(help="Output format.")] = "json",
    output_pretty_recurse: Annotated[bool, typer.Option(help="Recursively print related objects.")] = True,
    output_pretty_maxwidth: Annotated[int, typer.Option(help="Max char width to print (with exceptions: we will never truncate field names or record type titles; and we will never truncate values below 5 characters).  Defaults to the width of your terminal if we can extract it (>=Python 3.3), else 80 characters.")] = get_default_maxwidth(),
    output_pretty_indent: Annotated[int, typer.Option(help="Indentation level for related objects.")] = 2,
    output_pretty_localtime: Annotated[bool, typer.Option(help="Pretty-print datetimes in local timezone.")] = True):
    """
    An OpenZMS client.
    """
    ctx.obj = ContextObj(
        token=token,
        identity_url=identity_url,
        zmc_url=zmc_url,
        dst_url=dst_url,
        elaborate=elaborate,
        force=force,
        timeout=timeout,
        follow_redirects=follow_redirects,
        raise_on_unexpected_status=raise_on_unexpected_status,
        verify_ssl=verify_ssl,
        output=output,
        output_pretty_recurse=output_pretty_recurse,
        output_pretty_maxwidth=output_pretty_maxwidth,
        output_pretty_indent=output_pretty_indent,
        output_pretty_localtime=output_pretty_localtime)

    if debug or debug_all:
        logging.basicConfig()
    if debug:
        LOG.setLevel(logging.DEBUG)
    if debug_all:
        logging.getLogger().setLevel(logging.DEBUG)

def _is_success(status_code: http.HTTPStatus) -> bool:
    if isinstance(status_code.value, int) and status_code.value >= 200 and status_code.value < 300:
        return True
    return False

# Subcommands in Typer have a nasty bug or behavior, where if you call a
# deepest-leaf subcommand, and it has a result_callback, that callback is
# called first, then the next level up the hierarchy is called.  So, stop that
# by always exiting in our result_callback.  Not sure if this is intended or
# not.
@click.pass_context
def app_result_callback(ctx: typer.Context, result: typing.Any):
    if not _is_success(result.status_code):
        if ctx.obj.output == OutputKind.json and result.parsed:
            print(json.dumps(result.parsed.to_dict(), sort_keys=True, indent=4), file=sys.stderr)
            print("", file=sys.stderr)
        elif ctx.obj.output == OutputKind.pretty and result.parsed:
            pretty_print_result(result.parsed, ctx)
        elif result.parsed:
            print("Error {result.status_code.value}: {result.parsed.error}\n", file=sys.stderr)
        else:
            c = result.content.decode("utf-8")
            print("Error {result.status_code.value}: {c}\n", file=sys.stderr)
        raise typer.Exit(result.status_code.value)
    else:
        if ctx.obj.output == OutputKind.json and result.parsed:
            print(json.dumps(result.parsed.to_dict(), sort_keys=True, indent=4))
            print()
        elif ctx.obj.output == OutputKind.pretty and result.parsed:
            pretty_print_result(result.parsed, ctx)
        #elif result.parsed:
        else:
            print(result.content.decode("utf-8"))
            print()
        exit(0)

app = typer.Typer()

def main():
    # Initialize some debug logging; the pre-typer metaprogramming is
    # vulnerable to unforeseen cases.
    zmsclient_debug = os.getenv("ZMSCLIENT_DEBUG", None)
    zmsclient_debug_all = os.getenv("ZMSCLIENT_DEBUG_ALL", None)
    zmsclient_include_raw = os.getenv("ZMSCLIENT_INCLUDE_RAW", None)
    if zmsclient_debug or zmsclient_debug_all:
        logging.basicConfig()
    if zmsclient_debug:
        LOG.setLevel(logging.DEBUG)
    if zmsclient_debug_all:
        logging.getLogger().setLevel(logging.DEBUG)

    # List API submodules we're willing to wrap to the CLI.  We could also look
    # for anything with {api,models,client}, but why bother?
    submodules = [
        "zmsclient.identity.v1.api",
        "zmsclient.zmc.v1.api",
        "zmsclient.dst.v1.api",
    ]
    # Exclude a variety of api groups.
    exclude_api_groups = [
        "health",
        "version",
        "subscription",
    ]
    # Exclude a variety of endpoints.
    exclude_api_verbs = []
    # Transform some endpoint names.  We do this 
    api_replacements = {
        "role_binding": "rolebinding",
        "op_status": "opstatus",
        "int_constraint": "intconstraint",
        "rt_int_constraint": "rtintconstraint",
    }

    # Build subcommands.
    subapps = dict()
    for submodname in submodules:
        sm = importlib.import_module(submodname)
        for g in pkgutil.walk_packages(sm.__path__):
            gname = g.name
            if gname in exclude_api_groups:
                LOG.debug(f"skipping pkg {sm.__path__}.{gname}")
                continue
            LOG.debug(f"scanning pkg {sm.__path__}.{gname}")
            gmodname = submodname + "." + gname
            gmod = importlib.import_module(gmodname)
            for ep in pkgutil.walk_packages(gmod.__path__):
                LOG.debug(f"scanning pkg {sm.__path__}.{gname} endpoint {ep.name}")
                epname = ep.name
                epmodname = gmodname + "." + epname
                # Transform API endpoint name if requested.
                transname = epname
                for (rep, trans) in api_replacements.items():
                    ret = epname.find(rep)
                    if ret > -1:
                        transname = epname[0:ret] + trans + epname[ret+len(rep):]
                        break
                transsplit = transname.split("_")
                # Only accept known API endpoint "kinds."
                verb = transsplit[0]
                if verb not in ("list", "get", "update", "delete", "create"):
                    continue
                if verb in exclude_api_verbs:
                    continue
                # Do not accept unknown API endpoints.
                if len(transsplit) < 2:
                    LOG.warning(f"invalid api endpoint {epmodname}; skipping")
                    continue
                # Remove plural from list endpoints.
                if verb == "list":
                    if False and transsplit[-1].endswith("es"):
                        transsplit[-1] = transsplit[-1][0:-2]
                    elif transsplit[-1].endswith("s"):
                        transsplit[-1] = transsplit[-1][0:-1]
                objpath = transsplit[1:]
                objdict = subapps
                t = app
                for obj in objpath:
                    if obj not in objdict:
                        # Add a new Typer at this leaf.
                        nt = typer.Typer(result_callback=app_result_callback)
                        t.add_typer(nt, name=obj)
                        objdict[obj] = dict(__typer=nt)
                        t = nt
                    else:
                        # Grab the existing Typer and keep descending.
                        objdict = objdict[obj]
                        t = objdict["__typer"]
                # Import the endpoint pkg.
                epmod = importlib.import_module(epmodname)
                epf = getattr(epmod, "sync_detailed")
                if not epf:
                    LOG.warning(f"no 'sync_detailed' function in endpoint module {epmodname}; skipping")
                    continue
                else:
                    LOG.debug(f"wrapping '{epmodname}.sync_detailed'")
                # If this is a function with a single `body` argument whose
                # type is one of our schema models, then provide the exploded
                # version by default, as well as a raw version that simply
                # accepts `body`.
                #
                # Further, if this is an update function with a corresponding
                # get function, pass that in.  If the get function's return
                # type is the same as the single body param update function,
                # and if explode_body is True, then the lambda will call get
                # first, make all the params optional (except non-body *id
                # params), and for any params that are not Unset, overlay those
                # onto the fetched object from the get function, and finally,
                # sent that object as the body in the lambda to perform the
                # update.
                if is_zmsclient_single_body_model_func(epf):
                    ugf = None
                    ut = None
                    if verb == "update":
                        ugmodname = epmodname.replace(".update_", ".get_")
                        ugmod = None
                        ugf = None
                        try:
                            ugmod = importlib.import_module(ugmodname)
                            ugf = getattr(ugmod, "sync_detailed")
                            if not ugf:
                                LOG.debug(f"no 'sync_detailed' function in update getter endpoint module {ugmodname}; skipping")
                        except:
                            LOG.debug(f"no matching getter module '{ugmodname}' for update module '{epmodname}'")
                        if ugf:
                            ut = find_zmsclient_unset_type(epf)
                    wrapped_func = wrap_api_func(verb, epf, explode_body=True, skip_complex=True, explode_prefetcher=ugf, unset_type=ut)
                    t.command(verb)(wrapped_func)
                    LOG.debug(f"command: {verb} {epmodname} (exploded): %r", wrapped_func)

                    if zmsclient_include_raw:
                        wrapped_func_raw = wrap_api_func(epf, verb=verb)
                        verb_raw = f"{verb}_raw"
                        t.command(verb_raw)(wrapped_func_raw)
                        LOG.debug(f"command: {verb_raw} {epmodname} (raw): %r", wrapped_func_raw)
                else:
                    wrapped_func = wrap_api_func(verb, epf)
                    t.command(verb)(wrapped_func)
                    LOG.debug(f"command: {epmodname}: %r", wrapped_func)

    # Initialize the global options and typer Context.
    app.callback()(app_init)

    # Run typer.
    app()

if __name__ == "__main__":
    main()
