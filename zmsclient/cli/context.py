import attrs
import enum
import typing

from zmsclient.cli.output import get_default_maxwidth

class OutputKind(str, enum.Enum):
    json = "json"
    raw = "raw"
    pretty = "pretty"

@attrs.define
class ContextObj:
    token: str = None
    identity_url: str = None
    zmc_url: str = None
    dst_url: str = None
    elaborate: bool = False
    force: bool = False
    timeout: float = 5.0
    follow_redirects: bool = True
    verify_ssl: bool = True
    raise_on_unexpected_status: bool = True
    output: OutputKind = "json"
    output_pretty_recurse: bool = True
    output_pretty_maxwidth: int = get_default_maxwidth()
    output_pretty_indent: int = 2
    output_pretty_localtime: bool = True

    def get_service_url(self, svc: str) -> str:
        if svc == "identity":
            return self.identity_url
        elif svc == "zmc":
            return self.zmc_url
        elif svc == "dst":
            return self.dst_url
        else:
            raise Exception(f"No endpoint for service '{svc}'")

    def get_headers(self) -> typing.Dict[str, str]:
        headers = dict()
        if self.token:
            headers["X-Api-Token"] = self.token
        if self.elaborate:
            headers["X-Api-Elaborate"] = "true"
        if self.force:
            headers["X-Api-Force-Update"] = "true"
        return headers
