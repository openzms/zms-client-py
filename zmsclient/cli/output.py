import datetime
import enum
import typing
import click

from zmsclient.cli.typer import is_zmsclient_model_type

def get_default_maxwidth():
    try:
        return os.get_terminal_size()[0]
    except:
        return 80

def pretty_print_result(o: typing.Any, ctx: click.Context, cur_indent: int = 0, parentstr: str = "", seen: typing.List[typing.Any] = [], root_class: typing.Any = None, with_headers: bool = False):
    """
    Pretty-print a `Response.parsed` object (or `Error`) returned from an invocation of an API endpoint.
    """
    maxwidth = ctx.obj.output_pretty_maxwidth
    indent = ctx.obj.output_pretty_indent
    recurse = ctx.obj.output_pretty_recurse
    localtime = ctx.obj.output_pretty_localtime

    is_root_class_instance = False
    if not root_class:
        root_class = o.__class__
    elif isinstance(o, root_class):
        is_root_class_instance = True

    if o in seen:
        return

    if maxwidth is None:
        maxwidth = 0
    title = o.__class__.__name__

    if parentstr:
        title = parentstr + "." + title
    title_width = len(title)
    field_width = len("Field")
    columns = []
    relationships = []
    #
    # NB: o *must* be a class instance with slots, or a dict.
    #
    fields = None
    if hasattr(o, "__slots__"):
        fields = o.__slots__
    elif isinstance(o, dict):
        fields = o.keys()
    else:
        raise TypeError(f"invalid object {o}")
    for k in fields:
        v = getattr(o, k, None)
        if k.startswith("_"):
            continue
        elif k == "additional_properties":
            if v:
                relationships.append(k)
            else:
                continue
        elif isinstance(v, list):
            relationships.append(k)
        elif isinstance(v, dict):
            relationships.append(k)
        elif is_zmsclient_model_type(getattr(v, "__class__", None)):
            relationships.append(k)
        else:
            columns.append(k)
    for k in columns:
        if len(k) > field_width:
            field_width = len(k)
    for k in relationships:
        if len(k) > field_width:
            field_width = len(k)
    val_width = len("Value")
    column_values = {}
    for k in columns:
        val = None
        if isinstance(o, dict):
            val = o[k]
        else:
            val = getattr(o, k, None)
        if isinstance(val, datetime.datetime):
            if localtime:
                val = val.astimezone().isoformat()
            else:
                val = val.isoformat()
        elif isinstance(val, enum.Enum):
            val = str(val)
        else:
            val = repr(val)
        column_values[k] = val
        l = len(val)
        if l > val_width:
            val_width = l
    relationship_values = {}
    todo = []
    for k in relationships:
        v = getattr(o, k, None)
        if v is None:
            val = "None"
        elif k == "additional_properties":
            if v:
                val = str(len(v))
                todo.append(v)
        elif isinstance(v, list):
            val = str(len(v))
            todo.extend(v)
        elif isinstance(v, dict):
            val = str(len(v))
            todo.append(v)
        elif is_zmsclient_model_type(getattr(v, "__class__", None)):
            val = repr(v)
            todo.append(v)
        else:
            val = str(v)
        l = len(val)
        if l > val_width:
            val_width = l
        relationship_values[k] = val
    # We do not truncate the object title nor its fields, and we assume a
    # minimum of 5 chars to display partial values.  maxwidth is about
    # truncating values if necessary.  Our goal with all this is to calculate
    # two values: total_width (the max width of chars per line that we will
    # print); and val_width (a subordinate value that governs how wide the
    # value chars will be).
    total_title_width = cur_indent + title_width + 4
    total_fv_width = cur_indent + field_width + 4 + 3 + val_width
    MIN_VAL_WIDTH = 5
    least_val_width = val_width
    if least_val_width > MIN_VAL_WIDTH:
        least_val_width = MIN_VAL_WIDTH
    total_least_fv_width = cur_indent + field_width + 4 + 3 + least_val_width
    if maxwidth > 0:
        # We may need to limit chars per line:
        minwidth = total_title_width
        wide = "t"
        if total_fv_width > minwidth:
            minwidth = total_fv_width
            wide = "kv"
        if minwidth > maxwidth:
            total_width = minwidth
            if wide == "t":
                val_width = total_width - (cur_indent + field_width + 4 + 3)
            else:
                # This is the complicated case.  We are willing to reduce
                # val_width to least_val_width, but we only want to reduce it
                # as necessary for the title_width -- because we are not
                # willing to reduce that.
                ourmaxwidth = maxwidth
                if total_title_width > maxwidth:
                    ourmaxwidth = total_title_width
                if total_least_fv_width > ourmaxwidth:
                    ourmaxwidth = total_least_fv_width
                    val_width = least_val_width
                else:
                    val_width = ourmaxwidth - (cur_indent + field_width + 4 + 3)
                total_width = ourmaxwidth
        else:
            if wide == "t":
                total_width = minwidth
                val_width = minwidth - (cur_indent + field_width + 4 + 3)
            else:
                total_width = cur_indent + field_width + 4 + 3 + val_width
    else:
        # We just want to print everything, no limits:
        total_width = field_width + val_width + 4 + 3 + cur_indent
        if total_title_width > total_width:
            total_width = total_title_width
            diff = total_width - (field_width + val_width + 4 + 3 + cur_indent)
            val_width += diff

    print(" " * cur_indent + "+" + "=" * (total_width - 2 - cur_indent) + "+")
    print(" " * cur_indent + "| {:<{}} |".format(
        title[:(total_width - 4 - cur_indent)],total_width - 4 - cur_indent))
    print(" " * cur_indent + "+" + "=" * (total_width - 2 - cur_indent) + "+")
    if with_headers:
        print("{:<{}}| {:<{}} | {:<{}} |".format(
            "",cur_indent,"Field",field_width,"Value",val_width))
        print(" " * cur_indent + "+" + "-" * (total_width - 2 - cur_indent) + "+")
    for k in columns:
        v = column_values[k]
        print("{li:<{cur_indent}}| {field:<{field_width}} | {val:<{val_width}} |".format(
            li="",cur_indent=cur_indent,
            field=k,field_width=field_width,
            val=str(v)[:val_width],val_width=val_width))
    for k in relationships:
        v = relationship_values[k]
        print("{li:<{cur_indent}}| {field:<{field_width}} | {val:<{val_width}} |".format(
            li="",cur_indent=cur_indent,
            field=k,field_width=field_width,
            val=str(v)[:val_width],val_width=val_width))
    print(" " * cur_indent + "+" + "-" * (total_width - 2 - cur_indent) + "+")
    if hasattr(o,"id"):
        title += "(%s)" % (str(o.id))
    if recurse and todo and not is_root_class_instance:
        seen.append(o)
        for t in todo:
            pretty_print_result(
                t, ctx, cur_indent=cur_indent+indent,
                parentstr=title, seen=seen, root_class=root_class)
