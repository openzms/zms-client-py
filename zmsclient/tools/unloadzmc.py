#!/bin/python

import dateutil
import os
import sys
import argparse
import logging

from zmsclient.zmc.client import ZmsZmcClient
from . import DefaultDestEnvAction

LOG = logging.getLogger(__name__)

def fatal(response):
    LOG.error("ERROR: %r", response)
    exit(-1)
    pass

#
# Delete all Monitors
#
def deleteMonitors(inner_zmc_client, impotent=False):
    monitors = inner_zmc_client.list_monitors(items_per_page=1000)
    for monitor in monitors.monitors:
        if impotent:
            LOG.info("Not deleting monitor: %r", monitor.id)
            continue
        LOG.debug("Deleting: %r", monitor)
        response = inner_zmc_client.delete_monitor(monitor.id)
        if response:
            fatal(response)
            pass
        LOG.info("Deleted monitor: %r", monitor.id)
        LOG.debug("Deleted monitor: %r", response)
        pass
    pass

#
# Delete all Radios
#
def deleteRadios(inner_zmc_client, impotent=False):
    radios = inner_zmc_client.list_radios(items_per_page=1000)
    for radio in radios.radios:
        # Reload with elaborate so we get the ports.
        radio = inner_zmc_client.get_radio(radio.id, elaborate=True)
        if not radio:
            fatal("Could not reload radio: " + radio.id)
            pass
        for port in radio.ports:
            if impotent:
                LOG.info("Not deleting radio port: %r", port.id)
                continue
            LOG.debug("Deleting: %r", port)
            response = inner_zmc_client.delete_radio_port(radio.id, port.id)
            if response:
                fatal(response)
                pass
            LOG.info("Deleted radio port: %r", port.id)
            LOG.debug("Deleted radio port: %r", response)
            pass
        if impotent:
            LOG.info("Not deleting radio: %r", radio.id)
            continue
        LOG.debug("Deleting radio: %r", radio)
        response = inner_zmc_client.delete_radio(radio.id)
        if response:
            fatal(response)
            pass
        LOG.info("Deleted radio: %r", radio.id)
        LOG.debug("Deleted radio: %r", response)
        pass
    pass

#
# Delete all Antennas
#
def deleteAntennas(inner_zmc_client, impotent=False):
    antennas = inner_zmc_client.list_antennas(items_per_page=1000)
    for antenna in antennas.antennas:
        if impotent:
            LOG.info("Not deleting antenna: %r", antenna.id)
            continue
        LOG.debug("Deleting antenna: %r", antenna)
        response = inner_zmc_client.delete_antenna(antenna.id)
        if response:
            fatal(response)
            pass
        LOG.info("Deleted antenna: %r", antenna.id)
        LOG.debug("Deleted antenna: %r", response)
        pass
    pass

#
# Delete all locations
#
def deleteLocations(inner_zmc_client, impotent=False):
    locations = inner_zmc_client.list_locations(items_per_page=1000)
    for location in locations.locations:
        if impotent:
            LOG.info("Not deleting location: %r", location.id)
            continue
        LOG.debug("Deleting location: %r", location)
        response = inner_zmc_client.delete_location(location.id)
        if response:
            fatal(response)
            pass
        LOG.info("Deleted location: %r", location.id)
        LOG.debug("Deleted location: %r", response)
        pass
    pass

#
# Delete all spectrum
#
def deleteSpectrum(inner_zmc_client, impotent=False):
    spectrumlist = inner_zmc_client.list_spectrum(items_per_page=1000)
    for spectrum in spectrumlist.spectrum:
        if impotent:
            LOG.info("Not deleting spectrum: %r", spectrum.id)
            continue
        LOG.debug("Deleting spectrum: %r", spectrum)
        response = inner_zmc_client.delete_spectrum(spectrum.id)
        if response:
            fatal(response)
            pass
        LOG.info("Deleted spectrum: %r", spectrum.id)
        LOG.debug("Deleted spectrum: %r", response)
        pass
    pass

def main():
    parser = argparse.ArgumentParser(
        prog="loadzmc",
        description="Delete all objects (radios, antennas, monitors, locations) from a ZMC.")
    parser.add_argument(
        "-d", "--debug", default=0, action="count",
        help="Increase debug level: defaults to INFO; add once for zmsclient DEBUG; add twice to set the root logger level to DEBUG")
    parser.add_argument(
        "-n", "--impotent", default=False, action="store_true",
        help="Impotent: do not inject events")
    parser.add_argument(
        "--element-token", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) element token")
    parser.add_argument(
        "--zmc-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) ZMC URL")

    args = parser.parse_args(sys.argv[1:])

    logging.basicConfig()
    if args.debug:
        LOG.setLevel(logging.DEBUG)
        logging.getLogger('zmsclient').setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
    if args.debug > 1:
        logging.getLogger().setLevel(logging.DEBUG)

    inner_zmc_client = ZmsZmcClient(
        args.zmc_http, args.element_token,
        detailed=False, raise_on_unexpected_status=True)

    deleteMonitors(inner_zmc_client, impotent=args.impotent)
    deleteRadios(inner_zmc_client, impotent=args.impotent)
    deleteAntennas(inner_zmc_client, impotent=args.impotent)
    deleteLocations(inner_zmc_client, impotent=args.impotent)
    deleteSpectrum(inner_zmc_client, impotent=args.impotent)

if __name__ == "__main__":
    main()
