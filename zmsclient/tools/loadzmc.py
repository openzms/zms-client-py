#!/bin/python

import attrs
import dateutil
import datetime
import os
import sys
import json
import argparse
import logging

from zmsclient.zmc.client import ZmsZmcClient
from zmsclient.zmc.v1.models import Constraint, SpectrumConstraint
from zmsclient.zmc.v1.models import Spectrum, Policy
from zmsclient.zmc.v1.models import Location
from zmsclient.zmc.v1.models import Antenna
from zmsclient.zmc.v1.models import Radio, RadioPort
from zmsclient.zmc.v1.models import Monitor
from . import DefaultDestEnvAction

LOG = logging.getLogger(__name__)

def fatal(response):
    LOG.error(response)
    exit(-1)
    pass

# Map outer IDs to inner IDs and back.
@attrs.define
class ObjectMap:
    """Map outer IDs to inner IDs and back."""
    id_map = dict()

    def add_object(self, inner, outer):
        self.id_map[inner.id] = outer.id
        self.id_map[outer.id] = inner.id

    def map_to_inner_id(self, outer_id):
        if outer_id == None:
            return None
        return self.id_map.get(outer_id, None)

#
# Grab our grant, this becomes the initial spectrum
#
def importSpectrum(inner_zmc_client, outer_zmc_client, element_id, parent_grant_list,
                   objmap, impotent=False):
    for outer_id in parent_grant_list.split(","):
        grant = outer_zmc_client.get_grant(outer_id, elaborate=True)
        if not grant:
            fatal("Could not get grant: %r " % (outer_id,))
            pass

        #
        # We need to follow the replacement chain to find the current
        # grant.
        #
        current_outer_id = outer_id
        while grant.replacement:
            current_outer_id = grant.replacement.new_grant_id
            grant = outer_zmc_client.get_grant(current_outer_id, elaborate=True)
            pass

        #
        # Use the original grant ID in the name to key on.
        #
        inner_spectrum = inner_zmc_client.list_spectrum(
            element_id=element_id, spectrum="Outer RDZ grant: " + outer_id,
            elaborate=True)

        #
        # Check if already added.
        #
        if (inner_spectrum and inner_spectrum.spectrum and
            len(inner_spectrum.spectrum)):
            spec = inner_spectrum.spectrum[0]
            LOG.info("Spectrum already exists: %r", spec.id)
            LOG.debug("Fetched inner spectrum: %r", spec)
            #
            # if the grant has been replaced, lets update the expiration. 
            #
            if spec.ext_id != current_outer_id:
                LOG.info("Updating spectrum expiration: %r", grant.expires_at)
                spec.ext_id = current_outer_id
                spec.expires_at = grant.expires_at
                if not impotent:
                    response = inner_zmc_client.update_spectrum(
                        spectrum_id=spec.id, body=spec)
                    LOG.debug("updated spectrum: %r", response)
                    pass
                pass
            continue

        grant = outer_zmc_client.get_grant(current_outer_id, elaborate=True)
        if not grant:
            fatal("Could not get grant: %r " % (current_outer_id,))
            pass

        # Restrict the inner spectrum based on the inner OpenZMS grant.
        constraints = []
        for constraint in grant.constraints:
            constraint = constraint.constraint
            new = Constraint(min_freq=constraint.min_freq,
                             max_freq=constraint.max_freq,
                             max_eirp=constraint.max_eirp,
                             exclusive=True)
        
            constraints.append(SpectrumConstraint(constraint=new))
            pass

        # Stub policy, do not think I need to worry too much about this
        policy = Policy(element_id=element_id,
                        allowed=True,
                        auto_approve=True,
                        disable_emit_check=True,
                        priority=500)

        LOG.info("Spectrum does not exist yet: %r", outer_id)

        # Lets set the URL to the outer grant page.
        outer_grant_url = outer_zmc_client._base_url + "/grants/" + outer_id

        spectrum = Spectrum(name="Outer RDZ grant: " + outer_id,
                        description=outer_zmc_client._base_url,
                        enabled=True,
                        url=outer_grant_url,
                        ext_id=current_outer_id,
                        element_id=element_id,
                        starts_at=datetime.datetime.now(datetime.timezone.utc),
                        expires_at=grant.expires_at,
                        constraints=constraints,
                        policies=[policy])
        
        LOG.debug("Creating inner spectrum: %r", spectrum)
        if impotent:
            return

        response = inner_zmc_client.create_spectrum(body=spectrum)
        if not response:
            fatal("Could not create new spectrum")
            pass
        pass
    pass
    
#
# Copy locations in.
#
def importLocations(inner_zmc_client, outer_zmc_client, element_id,
                    objmap, name=None, impotent=False):
    kwargs = dict()
    if name:
        kwargs["name"] = name
    locations = outer_zmc_client.list_locations(**kwargs, items_per_page=1000)
    for location in locations.locations:
        #
        # Check if already added.
        #
        inner = inner_zmc_client.list_locations(items_per_page=1000,
            element_id=element_id, name=location.name)
        if inner and inner.locations and len(inner.locations):
            loc = inner.locations[0]
            LOG.info("Location already exists: %r, %r", loc.name, loc.id)
            LOG.debug("Location: %r", loc)
            objmap.add_object(location, loc)            
            continue

        LOG.info("Location does not exist yet: %r", location.name)
        if impotent:
            continue

        newloc = Location(name=location.name,
                          element_id=element_id,
                          srid=location.srid,
                          x=location.x,
                          y=location.y,
                          z=location.z)
        LOG.debug("Creating location: %r", newloc)
    
        response = inner_zmc_client.create_location(body=newloc)
        if not response:
            fatal("Could not create location: " + location.id)
            pass

        objmap.add_object(location, response)
        LOG.info("Created location: %r", response.id)
        LOG.debug("Created location: %r", response)
        pass
    pass

#
# Copy locations in.
#
def importAntennas(inner_zmc_client, outer_zmc_client, element_id,
                   objmap, name=None, impotent=False):
    kwargs = dict()
    if name:
        kwargs["antenna"] = name
    antennas = outer_zmc_client.list_antennas(**kwargs, items_per_page=1000)
    for antenna in antennas.antennas:
        #
        # Check if already added.
        #
        inner = inner_zmc_client.list_antennas(items_per_page=1000,
            element_id=element_id, antenna=antenna.name)
        if inner and inner.antennas and len(inner.antennas):
            ant = inner.antennas[0]
            LOG.info("Antenna already exists: %r, %r", ant.name, ant.id)
            LOG.debug("Antenna: %r", ant)
            objmap.add_object(antenna, ant)            
            continue

        LOG.info("Antenna does not exist yet: %r", antenna.name)
        if impotent:
            continue
        
        newantenna = Antenna(name=antenna.name,
                             element_id=element_id,
                             description=antenna.description,
                             type=antenna.type,
                             vendor=antenna.vendor,
                             model=antenna.model,
                             gain=antenna.gain,
                             beam_width=antenna.beam_width)
        LOG.debug("Creating new antenna: %r", newantenna)

        response = inner_zmc_client.create_antenna(body=newantenna)
        if not response:
            fatal("Could not create antenna: " + antenna.id)
            pass

        objmap.add_object(antenna, response)
        LOG.info("Created antenna: %r", response.id)
        LOG.debug("Created antenna: %r", response)
        pass
    pass

#
# Copy radios (and their ports) in.
#
def importRadios(inner_zmc_client, outer_zmc_client, element_id,
                 objmap, name=None, impotent=False):
    kwargs = dict()
    if name:
        kwargs["radio"] = name
    radios = outer_zmc_client.list_radios(**kwargs, items_per_page=1000)
    for radio in radios.radios:
        if not radio.enabled:
            continue

        # Reload with elaborate so we get the ports.
        radio = outer_zmc_client.get_radio(radio.id, elaborate=True)
        if not radio:
            fatal("Could not reload radio: " + radio.id)
            pass

        #
        # Check if already added.
        #
        inner = inner_zmc_client.list_radios(items_per_page=1000,
            element_id=element_id, radio=radio.id)
        
        if inner and inner.radios and len(inner.radios):
            rad = inner.radios[0]
            LOG.info("Radio already exists: %r, %r", rad.name, rad.id)
            LOG.debug("Radio: %r", rad)

            # Reload with elaborate so we get the ports.
            rad = inner_zmc_client.get_radio(rad.id, elaborate=True)
            if not rad:
                fatal("Could not reload inner radio: " + rad.id)
                pass
            objmap.add_object(radio, rad)

            #
            # Need mappings for the ports, for monitors below.
            # Need to deal with port changes (additions,deletions). Later
            #
            for iport in rad.ports:
                for oport in radio.ports:
                    if iport.device_id == oport.id:
                        LOG.info("Radio Port already exists: %r, %r", iport.name, iport.id)
                        objmap.add_object(oport, iport)
                        pass
                    pass
                pass
            continue

        LOG.info("Radio does not exist yet: %r", radio.name)
        if impotent:
            continue
        
        #
        # Need to create the local radio before we can add ports
        #
        newradio = Radio(name=radio.name,
                         element_id=element_id,
                         description=radio.description,
                         device_id=radio.id,
                         enabled=radio.enabled,
                         location_id=objmap.map_to_inner_id(radio.location_id))
        LOG.debug("Creating new radio: %r", newradio)

        response = inner_zmc_client.create_radio(body=newradio)
        if not response:
            fatal("Could not create radio: " + radio.id)
            pass
        LOG.debug("Created new radio: %r", response)

        objmap.add_object(radio, response)
        LOG.info("Created radio: %r", response.id)

        radio_id = response.id
        for port in radio.ports:
            newport = RadioPort(
                name=radio.name,
                radio_id=radio_id,
                device_id=port.id,
                tx=port.tx,
                rx=port.rx,
                enabled=port.enabled,
                min_freq=port.min_freq,
                max_freq=port.max_freq,
                max_power=port.max_power,
                antenna_id=objmap.map_to_inner_id(port.antenna_id),
                antenna_location_id=objmap.map_to_inner_id(port.antenna_location_id))
            LOG.debug("Creating new radio port: %r", newport)

            response = inner_zmc_client.create_radio_port(radio_id, body=newport)
            if not response:
                fatal("Could not create radio port: " + port.id)
                pass
            LOG.debug("Created new radio port: %r", response)

            objmap.add_object(port, response)
            LOG.info("Created radio port: %r", response.id)
            pass
        pass
    pass

#
# Copy monitors in.
#
def importMonitors(inner_zmc_client, outer_zmc_client, element_id,
                   objmap, name=None, impotent=False):
    kwargs = dict()
    if name:
        kwargs["monitor"] = name
    monitors = outer_zmc_client.list_monitors(**kwargs, items_per_page=1000)
    for monitor in monitors.monitors:
        if not monitor.enabled:
            continue
        
        # Reload with elaborate so we get the ports.
        monitor = outer_zmc_client.get_monitor(monitor.id, elaborate=True)
        if not monitor:
            fatal("Could not reload monitor: " + monitor.id)
            pass

        #
        # Check if already added.
        #
        inner = inner_zmc_client.list_monitors(
            element_id=element_id, monitor=monitor.id, items_per_page=1000)
        
        if inner and inner.monitors and len(inner.monitors):
            mon = inner.monitors[0]
            LOG.info("Monitor already exists: %r, %r", mon.name, mon.id)
            LOG.debug("Monitor: %r", mon)

            # Reload with elaborate so we get the ports.
            mon = inner_zmc_client.get_monitor(mon.id, elaborate=True)
            if not mon:
                fatal("Could not reload inner radio: " + mon.id)
                pass
            objmap.add_object(monitor, mon)
            continue

        LOG.info("Monitor does not exist yet: %r", monitor.name)
        if impotent:
            continue
        
        #
        # Need to create the local radio before we can add ports
        #
        radio_port_id = None
        monitored_radio_port_id = None

        if monitor.radio_port_id:
            radio_port_id = objmap.map_to_inner_id(monitor.radio_port_id);
            if not radio_port_id:
                fatal("No radio_port_id mapping for monitor: " + monitor.name)
                pass
        
        if monitor.monitored_radio_port_id:
            monitored_radio_port_id = objmap.map_to_inner_id(monitor.monitored_radio_port_id);
            if not monitored_radio_port_id:
                fatal("No monitored_radio_port_id mapping for monitor: " + monitor.name)
                pass
        
        newmon = Monitor(name=monitor.name,
                         element_id=element_id,
                         description=monitor.description,
                         device_id=monitor.id,
                         enabled=True,
                         types=monitor.types,
                         formats=monitor.formats,
                         radio_port_id=radio_port_id,
                         monitored_radio_port_id=monitored_radio_port_id)
        LOG.debug("Creating new monitor: %r", newmon)

        response = inner_zmc_client.create_monitor(body=newmon)
        if not response:
            fatal("Could not create monitor: " + monitor.id)
            pass
        LOG.debug("Created new monitor: %r", response)

        objmap.add_object(monitor, response)
        LOG.info("Created monitor: %r", response.id)
        pass
    pass

def main():
    parser = argparse.ArgumentParser(
        prog="loadzmc",
        description="Load objects (radios, antennas, monitors, locations) from one ZMC into another.  Optionally accept a grant from the source ZMC and create it as a Spectrum object (outer to inner).")
    parser.add_argument(
        "-d", "--debug", default=0, action="count",
        help="Increase debug level: defaults to INFO; add once for zmsclient DEBUG; add twice to set the root logger level to DEBUG")
    parser.add_argument(
        "-n", "--impotent", default=False, action="store_true",
        help="Impotent: do not inject events")
    parser.add_argument(
        "--element-id", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) element ID")
    parser.add_argument(
        "--element-token", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) element token")
    parser.add_argument(
        "--zmc-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) ZMC URL")
    parser.add_argument(
        "--parent-zmc-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) ZMC URL")
    parser.add_argument(
        "--parent-element-token", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) element token")
    parser.add_argument(
        "--parent-grant-list", action=DefaultDestEnvAction, type=str, required=False,
        help="Upstream (outer) grant list to transform into Spectrum objects in inner ZMC")
    parser.add_argument(
        "--radio", default=None, type=str,
        help="Filter by radio name; pass '' to skip entirely")
    parser.add_argument(
        "--monitor", default=None, type=str,
        help="Filter by monitor name; pass '' to skip entirely")
    parser.add_argument(
        "--antenna", default=None, type=str,
        help="Filter by antenna name; pass '' to skip entirely")
    parser.add_argument(
        "--location", default=None, type=str,
        help="Filter by location name; pass '' to skip entirely")

    args = parser.parse_args(sys.argv[1:])

    logging.basicConfig()
    if args.debug:
        LOG.setLevel(logging.DEBUG)
        logging.getLogger('zmsclient').setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
    if args.debug > 1:
        logging.getLogger().setLevel(logging.DEBUG)

    innerZMC   = ZmsZmcClient(args.zmc_http, args.element_token,
                              detailed=False, raise_on_unexpected_status=True)
    outerZMC   = ZmsZmcClient(args.parent_zmc_http, args.parent_element_token,
                              detailed=False, raise_on_unexpected_status=True)
    obj_map = ObjectMap()

    if args.parent_grant_list:
        importSpectrum(
            innerZMC, outerZMC, args.element_id, args.parent_grant_list,
            obj_map, impotent=args.impotent)
    if args.location != "":
        importLocations(
            innerZMC, outerZMC, args.element_id,
            obj_map, name=args.location, impotent=args.impotent)
    if args.antenna != "":
        importAntennas(
            innerZMC, outerZMC, args.element_id,
            obj_map, name=args.antenna, impotent=args.impotent)
    if args.radio != "":
        importRadios(
            innerZMC, outerZMC, args.element_id,
            obj_map, name=args.radio, impotent=args.impotent)
    if args.monitor != "":
        importMonitors(
            innerZMC, outerZMC, args.element_id,
            obj_map, name=args.monitor, impotent=args.impotent)

if __name__ == "__main__":
    main()
