#!/bin/python

import time
import uuid
import os
import sys
import signal
import traceback
import argparse
import logging
import asyncio
import datetime

from zmsclient.zmc.client import ZmsZmcClient
from zmsclient.dst.client import ZmsDstClient
from zmsclient.dst.v1.models import Observation
from zmsclient.zmc.v1.models import Subscription, EventFilter
from zmsclient.zmc.v1.models import Claim, Grant, Constraint, GrantConstraint
from zmsclient.common.subscription import ZmsSubscriptionCallback
from . import DefaultDestEnvAction

LOG = logging.getLogger(__name__)

# Not sure where to pick these up
EVENT_TYPE_REPLACED    =     1
EVENT_TYPE_CREATED     =     2
EVENT_SOURCETYPE_ZMC   =     2
EVENT_SOURCETYPE_DST   =     3
EVENT_CODE_GRANT       =     2006
EVENT_CODE_OBSERVATION =     3001

# Only when a daemon
LOGFILE = "/local/logs/zms-observations-relay.log"

#
# Need a map of outer monitor IDs to inner monitor IDs.
#
def loadMonitorMap(inner_zmc_client, outer_zmc_client, monitor=None):
    monitorMap = {}
    kwargs = dict()
    if monitor:
        kwargs["monitor"] = monitor
    inner_monitors = inner_zmc_client.list_monitors(**kwargs, items_per_page=1000)
    outer_monitors = outer_zmc_client.list_monitors(**kwargs, items_per_page=1000)

    for inner_monitor in inner_monitors.monitors:
        if not inner_monitor.enabled:
            continue

        for outer_monitor in outer_monitors.monitors:
            if outer_monitor.id == inner_monitor.device_id:
                monitorMap[outer_monitor.id] = inner_monitor.id;
                break

    LOG.info("Monitor map: %r", monitorMap)
    return monitorMap

#
# Also need a map of inner spectrum IDs to outer grants IDs
#
def loadSpectrumMap(inner_zmc_client, outer_zmc_client):
    spectrumMap = {}
    inner_spectrum = inner_zmc_client.list_spectrum(items_per_page=1000)
    for spectrum in inner_spectrum.spectrum:
        if not spectrum.ext_id or spectrum.ext_id == "":
            continue

        #
        # Need to chase back the grant chain in case there was a replacement
        # during the off season. At the moment, just worrying about the
        # expiration (extension) of the most recent grant in the chain. 
        #
        outer_id = spectrum.ext_id
        
        grant = outer_zmc_client.get_grant(outer_id, elaborate=True)
        if not grant:
            LOG.error("Could not get grant: %r " % (outer_id,))
            sys.exit(-1)
            pass

        current_outer_id = outer_id
        while grant.replacement:
            current_outer_id = grant.replacement.new_grant_id
            grant = outer_zmc_client.get_grant(current_outer_id, elaborate=True)
            if not grant:
                LOG.error("Could not get grant: %r " % (current_outer_id,))
                sys.exit(-1)
                pass
            pass

        #
        # if the grant has been replaced, lets update the expiration. 
        #
        if outer_id != current_outer_id:
            LOG.info("Updating spectrum expiration: %r", grant.expires_at)
            spectrum.ext_id = current_outer_id
            spectrum.expires_at = grant.expires_at
            response = inner_zmc_client.update_spectrum(
                spectrum_id=spectrum.id, body=spectrum)
            LOG.info("updated spectrum: %r -> %r", outer_id, current_outer_id )
            pass
        
        spectrumMap[spectrum.ext_id] = spectrum.id

        #
        # Watch for starting up and being out of sync wrt paused/resumed.
        #
        claimlist = inner_zmc_client.list_claims(
            element_id=spectrum.element_id, ext_id=spectrum.id, elaborate=True)

        if grant.status != "active" and claimlist.total == 0:
            LOG.info("Creating claim on spectrum: %r", spectrum.id)
            createClaim(inner_zmc_client, spectrum.id)
        elif grant.status == "active" and claimlist.total != 0:
            claim = claimlist.claims[0]
            LOG.info("Deleting claim on spectrum: %r", spectrum.id)
            inner_zmc_client.delete_claim(claim_id=claim.id)
            pass
        pass
    
    LOG.info("Spectrum map: %r", spectrumMap)
    return spectrumMap

#
# Create a claim on the entire spectrum object.
#
def createClaim(zmc_client, spectrum_id):
    spectrum = zmc_client.get_spectrum(spectrum_id, elaborate=True)    
    min_freq = spectrum.constraints[0].constraint.min_freq
    max_freq = spectrum.constraints[0].constraint.max_freq
    LOG.info("Creating a claim for %r,%r", min_freq, max_freq)
    
    claim = Claim(
        element_id=spectrum.element_id,
        ext_id=spectrum.id,
        type="claim",
        source="rdzinrdz",
        name="Paused",
        description="Paused by outer RDZ",
        grant=Grant(
            element_id=spectrum.element_id,
            spectrum_id=spectrum.id,
            priority=1023,
            name="Paused",
            ext_id=spectrum_id,
            description="Paused by outer RDZ",
            starts_at=datetime.datetime.now(datetime.timezone.utc),
            constraints=[
                GrantConstraint(
                    constraint=Constraint(
                        min_freq=min_freq,
                        max_freq=max_freq,
                        max_eirp=30,
                        exclusive=True
                    )
                )
            ]
        )
    )
    claim = zmc_client.create_claim(body=claim)
    if not claim:
        fatal("Could not create new claim")
        pass
    LOG.info("Created claim: %r", claim.id)
    pass

#
# Inject an observation from the parent, into the child.
#
def injectObservation(outer_dst_client, inner_dst_client, id, element_id,
                      monitor_map=dict(), impotent=False):
    observation = outer_dst_client.get_observation(id, data_inline=True)
    inner_monitor_id = monitor_map.get(observation.monitor_id, None)
    LOG.info("Observation: %r", id)
    LOG.info("  Outer Monitor: %r", observation.monitor_id)
    LOG.info("  Inner Monitor: %r", inner_monitor_id)

    new = Observation(element_id=element_id,
                      monitor_id=inner_monitor_id,
                      types=observation.types,
                      format_=observation.format_,
                      description=observation.description,
                      min_freq=observation.min_freq,
                      max_freq=observation.max_freq,
                      starts_at=observation.starts_at,
                      violation=observation.violation)

    LOG.debug("Creating observation: %r", new)
    if impotent:
        return

    new.data = observation.data
    response = inner_dst_client.create_observation(body=new)
    LOG.debug("created observation: %r", response)

#
# Watching for several cases;
# 1) Outer grant is paused/resumed; This is converted to a high priority
#    Claim on the entire spectrum,
# 2) Outer grant is replaced, as for extension; update the expires_at of
#    the corresponding spectrum object.
# 3) Outer grant is replaced, because the spectrum range had to be
#    moved to a different place on the dial. Update the spectrum range,
#
def updateSpectrum(outer_zmc_client, inner_zmc_client, grant, element_id,
                   spectrum_map=dict(), impotent=False):
    old_grant_id = grant.id
    LOG.info("Grant event: %r - %r", old_grant_id, grant.status)

    #
    # Watch for the grant being revoked or paused and disable the local
    # corresponding spectrum object
    #
    if not grant.replacement or not grant.replacement.new_grant_id:
        LOG.info("  Not a replacement")

        if not old_grant_id in spectrum_map:
            LOG.error("  Cannot find the old_grant_id in spectrum_map!")
            LOG.error("  Ignoring grant change")
            return

        # Ignore the "replacing" state, has no meaning here.
        if grant.status == "replacing":
            LOG.info("  Ignoring replacing state")
            return

        # Check for an existing claim on this spectrum.
        spectrum_id = spectrum_map[old_grant_id];
        claimlist = inner_zmc_client.list_claims(
            element_id=element_id, ext_id=spectrum_id, elaborate=True)

        if grant.status != "active" and grant.status != "approved" and claimlist.total == 0:
            LOG.info("  Creating claim on spectrum: %r", spectrum_id)
            createClaim(inner_zmc_client, spectrum_id)
        elif grant.status == "active" and claimlist.total != 0:
            claim = claimlist.claims[0]
            LOG.info("  Deleting claim on spectrum: %r", spectrum_id)
            inner_zmc_client.delete_claim(claim_id=claim.id)
            pass
        return

    new_grant_id = grant.replacement.new_grant_id
    grant = None
    LOG.info("Grant Replacement: %r -> %r", old_grant_id, new_grant_id)

    #
    # There can be a delay, so lets poll for a bit until it goes
    # active or is denied/deleted.
    #
    for x in range(30):
        time.sleep(2)
        grant = outer_zmc_client.get_grant(new_grant_id, elaborate=True)
        if grant.status in ["active", "denied", "deleted", "pending"]:
            break
        pass

    #
    # We get a new grant (replacement) even if the grant extension is
    # denied. We have to ignore that.
    #
    if grant.status != "active":
        LOG.error("  The replaced grant was not approved!")
        LOG.error("  Ignoring grant change")
        return

    #
    # I have a feeling that we will eventually get out of sync, if we
    # miss an event. 
    #
    if not old_grant_id in spectrum_map:
        LOG.error("  Cannot find the old_grant_id in spectrum_map!")
        LOG.error("  Ignoring grant change")
        return
    
    spectrum_id = spectrum_map[old_grant_id];
    spectrum = inner_zmc_client.get_spectrum(spectrum_id, elaborate=True)
    LOG.info("  Spectrum %r", spectrum_id)
    LOG.info("  Old expiration: %s", spectrum.expires_at or "")
    LOG.info("  New expiration: %s", grant.expires_at)

    if impotent:
        return

    # Update the spectrum for the new expiration and the replacement grant
    spectrum.ext_id = new_grant_id
    spectrum.expires_at = grant.expires_at
    # Lets set the URL to the outer grant page.
    spectrum.url = outer_zmc_client._base_url + "/grants/" + new_grant_id

    response = inner_zmc_client.update_spectrum(
        spectrum_id=spectrum_id, body=spectrum)
    LOG.info("  Updated spectrum expiration: %r", spectrum_id)
    LOG.debug("updated spectrum: %r", response)

    #
    # Watch for modified spectrum constraint.
    #
    # XXX There is currently no way to match a grant having multiple
    # constraints, to the corresponding spectrum constraint. But at
    # the moment Powder created grants are single constraint.
    #
    spectrum_constraint = spectrum.constraints[0].constraint
    grant_constraint = grant.constraints[0].constraint
    if (grant_constraint.min_freq != spectrum_constraint.min_freq or
        grant_constraint.max_freq != spectrum_constraint.max_freq):
        spectrum_constraint.min_freq = grant_constraint.min_freq
        spectrum_constraint.max_freq = grant_constraint.max_freq

        response = inner_zmc_client.update_spectrum_constraint(
            spectrum_id=spectrum_id, constraint_id=spectrum_constraint.id,
            body=spectrum_constraint)
        LOG.info("  Updated spectrum constraint: %r - %r,%r", spectrum_id,
                 grant_constraint.min_freq, grant_constraint.max_freq)
        LOG.debug("updated spectrum constraint: %r", response)
        pass

    # This is in leu of restarting the subscription and web socket. 
    spectrum_map[new_grant_id] = spectrum_id
    del(spectrum_map[old_grant_id])
    pass

#
# Subclass ZmsSubscriptionCallback, to inject observations from the outer ZMS,
# into the inner ZMS
#
class DSTSubscriptionCallback(ZmsSubscriptionCallback):
    def __init__(self, outer_dstclient, inner_dstclient, inner_element_id,
                 monitor_map=dict(), impotent=False, **kwargs):
        super(DSTSubscriptionCallback, self).__init__(outer_dstclient, **kwargs)
        self.outerDST = outer_dstclient
        self.innerDST = inner_dstclient
        self.inner_element_id = inner_element_id
        self.monitor_map = monitor_map
        self.impotent = impotent

    def on_event(self, ws, evt, message):
        # For now, just observations
        if evt.header.source_type != EVENT_SOURCETYPE_DST:
            LOG.error("on_event: unexpected source type: %r (%r)",
                      evt.header.source_type, message)
            return
        if evt.header.type != EVENT_TYPE_CREATED:
            LOG.error("on_event: unexpected type: %r (%r)",
                      evt.header.type, message)
            return
        if evt.header.code != EVENT_CODE_OBSERVATION:
            LOG.error("on_event: unexpected code: %r (%r)",
                      evt.header.code, message)
            return

        try:
            injectObservation(
                self.outerDST, self.innerDST, evt.header.object_id,
                self.inner_element_id, monitor_map=self.monitor_map,
                impotent=self.impotent)
        except Exception as ex:
            LOG.exception(ex)

class ZMCSubscriptionCallback(ZmsSubscriptionCallback):
    def __init__(self, outer_zmcclient, inner_zmcclient, inner_element_id,
                 spectrum_map=dict(), impotent=False, **kwargs):
        super(ZMCSubscriptionCallback, self).__init__(outer_zmcclient, **kwargs)
        self.outerZMC = outer_zmcclient
        self.innerZMC = inner_zmcclient
        self.inner_element_id = inner_element_id
        self.spectrum_map = spectrum_map
        self.impotent = impotent

    def on_event(self, ws, evt, message):
        if evt.header.source_type != EVENT_SOURCETYPE_ZMC:
            LOG.error("on_event: unexpected source type: %r (%r)",
                      evt.header.source_type, message)
            return

        # Since we are subscribed to all ZMC events for this element,
        # there will be chatter we do not care about.
        if (evt.header.code != EVENT_CODE_GRANT):
            return

        try:
            updateSpectrum(self.outerZMC, self.innerZMC, evt.object_,
                           self.inner_element_id, spectrum_map=self.spectrum_map,
                           impotent=self.impotent)
        except Exception as ex:
            LOG.exception(ex)

# The hander has to be outside the async main.
def set_signal_handler(signum, task_to_cancel):
    def handler(_signum, _frame):
        asyncio.get_running_loop().call_soon_threadsafe(task_to_cancel.cancel)
    signal.signal(signum, handler)

def init_main():
    parser = argparse.ArgumentParser(
        prog="relayevents",
        description="Relay observation events from a 'parent' OpenZMS to another.")
    parser.add_argument(
        "-b", "--daemon", default=False, action="store_true",
        help="Daemonize")
    parser.add_argument(
        "-d", "--debug", default=0, action="count",
        help="Increase debug level: defaults to INFO; add once for zmsclient DEBUG; add twice to set the root logger level to DEBUG")
    parser.add_argument(
        "-n", "--impotent", default=False, action="store_true",
        help="Impotent: do not inject events")
    parser.add_argument(
        "--logfile", default=LOGFILE, type=str,
        help="Redirect logging to a file when daemonizing.")
    parser.add_argument(
        "--element-id", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) element ID")
    parser.add_argument(
        "--element-token", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) element token")
    parser.add_argument(
        "--zmc-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) ZMC URL")
    parser.add_argument(
        "--dst-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Local (inner) DST URL")
    parser.add_argument(
        "--parent-zmc-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) ZMC URL")
    parser.add_argument(
        "--parent-dst-http", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) DST URL")
    parser.add_argument(
        "--parent-element-token", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) element token")
    parser.add_argument(
        "--parent-element-userid", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) user id that is bound to the upstream element token")
    parser.add_argument(
        "--parent-element-id", action=DefaultDestEnvAction, type=str, required=True,
        help="Upstream (outer) token id")
    parser.add_argument(
        "--monitor", default=None, type=str,
        help="Filter by monitor name")
    parser.add_argument(
        "--no-zmc", default=False, action="store_true",
        help="Disable ZMC subscription")
    parser.add_argument(
        "--no-dst", default=False, action="store_true",
        help="Disable DST subscription")

    args = parser.parse_args(sys.argv[1:])

    if args.debug:
        LOG.setLevel(logging.DEBUG)
        logging.getLogger('zmsclient').setLevel(logging.DEBUG)
    else:
        LOG.setLevel(logging.INFO)
        logging.getLogger('zmsclient').setLevel(logging.INFO)
    if args.debug > 1:
        logging.getLogger().setLevel(logging.DEBUG)

    innerZMC   = ZmsZmcClient(args.zmc_http, args.element_token,
                              detailed=False, raise_on_unexpected_status=True)
    innerDST   = ZmsDstClient(args.dst_http, args.element_token,
                              detailed=False, raise_on_unexpected_status=True)
    outerZMC   = ZmsZmcClient(args.parent_zmc_http, args.parent_element_token,
                              detailed=False, raise_on_unexpected_status=True)
    outerDST   = ZmsDstClient(args.parent_dst_http, args.parent_element_token,
                              detailed=False, raise_on_unexpected_status=True)

    # Map outer monitors to inner monitors
    monitor_map = loadMonitorMap(innerZMC, outerZMC, monitor=args.monitor)
    # Ditto inner spectrum to outer grants
    spectrum_map = loadSpectrumMap(innerZMC, outerZMC)

    # And create the subscription (which includes the websocket)
    DSTsubscription = None
    if not args.no_dst:
        DSTsubscription = DSTSubscriptionCallback(
            outerDST, innerDST, args.element_id,
            subscription=Subscription(id=str(uuid.uuid4())),
            monitor_map=monitor_map,
            impotent=args.impotent, reconnect_on_error=True)

    ZMCsubscription = None
    if not args.no_zmc:
        ZMCsubscription = ZMCSubscriptionCallback(
            outerZMC, innerZMC, args.element_id,
            subscription=Subscription(
                id=str(uuid.uuid4()),
                filters=[EventFilter(element_ids=[args.parent_element_id],
                                     user_ids=[args.parent_element_userid])]),
            spectrum_map=spectrum_map,
            impotent=args.impotent, reconnect_on_error=True)

    return DSTsubscription, ZMCsubscription, args.daemon, args.logfile

async def async_main(*args):
    this_task = asyncio.current_task();
    set_signal_handler(signal.SIGINT, this_task)
    set_signal_handler(signal.SIGHUP, this_task)
    set_signal_handler(signal.SIGTERM, this_task)

    try:
        runnable = [sub.run_callbacks() for sub in args]
        await asyncio.gather(*runnable)
    except asyncio.CancelledError:
        for sub in args:
            if sub.id:
                sub.unsubscribe()

#
# The reason for this split of main, is cause we need to do the daemon
# fork() before heading into the asyncio main.
#
def main():
    DSTsubscription, ZMCsubscription, daemonize, logfile = init_main()
    format = "%(levelname)s:%(asctime)s:%(message)s"

    if daemonize:
        try:
            fp = open(logfile, "a");
            sys.stdout = fp
            sys.stderr = fp
            sys.stdin.close();
            logging.basicConfig(stream=fp, format=format)
            pass
        except:
            print("Could not open log file for append")
            sys.exit(1);
            pass
        pid = os.fork()
        if pid:
            sys.exit(0)
        os.setsid();
    else:
        logging.basicConfig(format=format)

    subs = []
    if DSTsubscription:
        subs.append(DSTsubscription)
    if ZMCsubscription:
        subs.append(ZMCsubscription)
    if not subs:
        sys.exit(0)
    asyncio.run(async_main(*subs))

if __name__ == "__main__":
    main()
