import argparse
import os

class DefaultDestEnvAction(argparse.Action):
    def __init__(self, option_strings, dest, required=True, default=None, **kwargs):
        """An argparse.Action that initializes the default value of the arg from an environment variable named `dest.upper()` (where dest is the storage location of the value post-parse, e.g. `args.dest`); and, if the arg was required, *unsets* it from being required, so that argparse does not fail the parse if the argument is not supplied.  This is certainly a bit unfortunate since it changes the helptext behavior, but nothing to do about that."""
        dest_upper = dest.upper()
        if dest_upper in os.environ:
            default = os.environ[dest_upper]
        if required and default:
            required = False
        super(DefaultDestEnvAction, self).__init__(
            option_strings, dest, default=default, required=required, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)
