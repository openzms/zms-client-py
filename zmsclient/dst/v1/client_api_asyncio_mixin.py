import functools

from .api.health.get_alive import asyncio_detailed as get_alive_asyncio_detailed
from .api.health.get_ready import asyncio_detailed as get_ready_asyncio_detailed
from .api.observation.create_observation import (
    asyncio_detailed as create_observation_asyncio_detailed,
)
from .api.observation.delete_observation import (
    asyncio_detailed as delete_observation_asyncio_detailed,
)
from .api.observation.get_observation import (
    asyncio_detailed as get_observation_asyncio_detailed,
)
from .api.observation.list_observations import (
    asyncio_detailed as list_observations_asyncio_detailed,
)
from .api.observation.update_observation import (
    asyncio_detailed as update_observation_asyncio_detailed,
)
from .api.subscription.create_subscription import (
    asyncio_detailed as create_subscription_asyncio_detailed,
)
from .api.subscription.delete_subscription import (
    asyncio_detailed as delete_subscription_asyncio_detailed,
)
from .api.subscription.get_subscription_events import (
    asyncio_detailed as get_subscription_events_asyncio_detailed,
)
from .api.subscription.get_subscriptions import (
    asyncio_detailed as get_subscriptions_asyncio_detailed,
)
from .api.version.get_version import asyncio_detailed as get_version_asyncio_detailed


class ClientApiAsyncioMixin:
    def _call_api_func(self, func, *args, **kwargs):
        return func(*args, **kwargs, client=self)

    def get_version(self, *args, **kwargs):
        return self._call_api_func(get_version_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_version, wrapped=get_version_asyncio_detailed)

    def get_alive(self, *args, **kwargs):
        return self._call_api_func(get_alive_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_alive, wrapped=get_alive_asyncio_detailed)

    def get_ready(self, *args, **kwargs):
        return self._call_api_func(get_ready_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_ready, wrapped=get_ready_asyncio_detailed)

    def list_observations(self, *args, **kwargs):
        return self._call_api_func(list_observations_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        list_observations, wrapped=list_observations_asyncio_detailed
    )

    def create_observation(self, *args, **kwargs):
        return self._call_api_func(create_observation_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        create_observation, wrapped=create_observation_asyncio_detailed
    )

    def get_observation(self, *args, **kwargs):
        return self._call_api_func(get_observation_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_observation, wrapped=get_observation_asyncio_detailed)

    def update_observation(self, *args, **kwargs):
        return self._call_api_func(update_observation_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        update_observation, wrapped=update_observation_asyncio_detailed
    )

    def delete_observation(self, *args, **kwargs):
        return self._call_api_func(delete_observation_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        delete_observation, wrapped=delete_observation_asyncio_detailed
    )

    def get_subscriptions(self, *args, **kwargs):
        return self._call_api_func(get_subscriptions_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        get_subscriptions, wrapped=get_subscriptions_asyncio_detailed
    )

    def create_subscription(self, *args, **kwargs):
        return self._call_api_func(
            create_subscription_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_subscription, wrapped=create_subscription_asyncio_detailed
    )

    def delete_subscription(self, *args, **kwargs):
        return self._call_api_func(
            delete_subscription_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        delete_subscription, wrapped=delete_subscription_asyncio_detailed
    )

    def get_subscription_events(self, *args, **kwargs):
        return self._call_api_func(
            get_subscription_events_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        get_subscription_events, wrapped=get_subscription_events_asyncio_detailed
    )
