from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="EventFilter")


@_attrs_define
class EventFilter:
    """
    Attributes:
        types (Union[Unset, List[int]]): A list of integer event types, one of which must match for the EventFilter to
            match.  If the list is empty, any type is matched.
        codes (Union[Unset, List[int]]): A list of integer event codes, one of which must match for the EventFilter to
            match.  If the list is empty, any code is matched.
        object_ids (Union[Unset, List[str]]): A list of event object UUIDs, one of which must match for the EventFilter
            to match.  If the list is empty, any object UUID is matched.
        user_ids (Union[Unset, List[str]]): A list of event object-associated user UUIDs, one of which must match for
            the EventFilter to match.  If the list is empty, any associated user UUID is matched.  Typically this will be
            the owner UUID of the object on which the event occurred.
        element_ids (Union[Unset, List[str]]): A list of event object-associated element UUIDs, one of which must match
            for the EventFilter to match.  If the list is empty, any associated element UUID is matched.  Typically this
            will be the owning element UUID of the object on which the event occurred.
    """

    types: Union[Unset, List[int]] = UNSET
    codes: Union[Unset, List[int]] = UNSET
    object_ids: Union[Unset, List[str]] = UNSET
    user_ids: Union[Unset, List[str]] = UNSET
    element_ids: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "types": {"put": True, "post": None},
        "codes": {"put": True, "post": None},
        "object_ids": {"put": True, "post": None},
        "user_ids": {"put": True, "post": None},
        "element_ids": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        types: Union[Unset, List[int]] = UNSET
        if not isinstance(self.types, Unset):
            types = self.types

        codes: Union[Unset, List[int]] = UNSET
        if not isinstance(self.codes, Unset):
            codes = self.codes

        object_ids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.object_ids, Unset):
            object_ids = self.object_ids

        user_ids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.user_ids, Unset):
            user_ids = self.user_ids

        element_ids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.element_ids, Unset):
            element_ids = self.element_ids

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if types is not UNSET:
            field_dict["types"] = types
        if codes is not UNSET:
            field_dict["codes"] = codes
        if object_ids is not UNSET:
            field_dict["object_ids"] = object_ids
        if user_ids is not UNSET:
            field_dict["user_ids"] = user_ids
        if element_ids is not UNSET:
            field_dict["element_ids"] = element_ids

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        types = cast(List[int], d.pop("types", UNSET))

        codes = cast(List[int], d.pop("codes", UNSET))

        object_ids = cast(List[str], d.pop("object_ids", UNSET))

        user_ids = cast(List[str], d.pop("user_ids", UNSET))

        element_ids = cast(List[str], d.pop("element_ids", UNSET))

        event_filter = cls(
            types=types,
            codes=codes,
            object_ids=object_ids,
            user_ids=user_ids,
            element_ids=element_ids,
        )

        event_filter.additional_properties = d
        return event_filter

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
