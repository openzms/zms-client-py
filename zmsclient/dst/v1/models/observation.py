import datetime
from io import BytesIO
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, File, FileJsonType, Unset

if TYPE_CHECKING:
    from ..models.annotation import Annotation


T = TypeVar("T", bound="Observation")


@_attrs_define
class Observation:
    """Observation report from a monitor.

    Attributes:
        monitor_id (str): The monitor that produced this observation.
        types (str): Comma-separated list of the type(s) of monitor related to this observation. Example: ota,sweep.
        format_ (str): The data format (e.g. `csv`, `hdf5`, `sigmf`). Example: csv.
        min_freq (int): The start of the frequency range (inclusive) included in the observation.
        starts_at (datetime.datetime): Observation start time.
        id (Union[Unset, str]): The id of the spectrum grant.
        collection_id (Union[None, Unset, str]): The collection containing this observation, if any.  Some observations
            are part of multi-observation collections.
        description (Union[None, Unset, str]): A summary of the observation data.
        max_freq (Union[None, Unset, int]): The end of the frequency range (inclusive) included in the observation.
        freq_step (Union[None, Unset, int]): If the observation data is related to regular frequency steps in the
            (min_freq, max_freq) range, this is that step size.
        ends_at (Union[None, Unset, datetime.datetime]): Observation end time.
        data (Union[File, None, Unset]): The raw data encoded with base64.
        violation (Union[None, Unset, bool]): True if the observation represents a violation.
        violation_verified_at (Union[None, Unset, datetime.datetime]): Time at which violation claim was verified.
        interference (Union[None, Unset, bool]): True if the observation represents interference.
        interference_verified_at (Union[None, Unset, datetime.datetime]): Time at which interference claim was verified.
        element_id (Union[Unset, str]): The element id associated with the observation.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        violated_grant_id (Union[None, Unset, str]): The id of the grant that was violated, if any.
        annotations (Union[List['Annotation'], None, Unset]): A list of related annotations.
    """

    monitor_id: str
    types: str
    format_: str
    min_freq: int
    starts_at: datetime.datetime
    id: Union[Unset, str] = UNSET
    collection_id: Union[None, Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    max_freq: Union[None, Unset, int] = UNSET
    freq_step: Union[None, Unset, int] = UNSET
    ends_at: Union[None, Unset, datetime.datetime] = UNSET
    data: Union[File, None, Unset] = UNSET
    violation: Union[None, Unset, bool] = UNSET
    violation_verified_at: Union[None, Unset, datetime.datetime] = UNSET
    interference: Union[None, Unset, bool] = UNSET
    interference_verified_at: Union[None, Unset, datetime.datetime] = UNSET
    element_id: Union[Unset, str] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    violated_grant_id: Union[None, Unset, str] = UNSET
    annotations: Union[List["Annotation"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "monitor_id": {"put": True, "post": True},
        "collection_id": {"put": True, "post": True},
        "types": {"put": None, "post": None},
        "format": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "min_freq": {"put": None, "post": None},
        "max_freq": {"put": None, "post": None},
        "freq_step": {"put": None, "post": None},
        "starts_at": {"put": None, "post": None},
        "ends_at": {"put": None, "post": None},
        "data": {"put": None, "post": None},
        "violation": {"put": None, "post": None},
        "violation_verified_at": {"put": None, "post": None},
        "interference": {"put": None, "post": None},
        "interference_verified_at": {"put": None, "post": None},
        "element_id": {"put": True, "post": None},
        "creator_id": {"put": True, "post": None},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "violated_grant_id": {"put": True, "post": None},
        "annotations": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        monitor_id = self.monitor_id

        types = self.types

        format_ = self.format_

        min_freq = self.min_freq

        starts_at = self.starts_at.isoformat()

        id = self.id

        collection_id: Union[None, Unset, str]
        if isinstance(self.collection_id, Unset):
            collection_id = UNSET
        else:
            collection_id = self.collection_id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        max_freq: Union[None, Unset, int]
        if isinstance(self.max_freq, Unset):
            max_freq = UNSET
        else:
            max_freq = self.max_freq

        freq_step: Union[None, Unset, int]
        if isinstance(self.freq_step, Unset):
            freq_step = UNSET
        else:
            freq_step = self.freq_step

        ends_at: Union[None, Unset, str]
        if isinstance(self.ends_at, Unset):
            ends_at = UNSET
        elif isinstance(self.ends_at, datetime.datetime):
            ends_at = self.ends_at.isoformat()
        else:
            ends_at = self.ends_at

        data: Union[FileJsonType, None, Unset]
        if isinstance(self.data, Unset):
            data = UNSET
        elif isinstance(self.data, File):
            data = self.data.to_tuple()

        else:
            data = self.data

        violation: Union[None, Unset, bool]
        if isinstance(self.violation, Unset):
            violation = UNSET
        else:
            violation = self.violation

        violation_verified_at: Union[None, Unset, str]
        if isinstance(self.violation_verified_at, Unset):
            violation_verified_at = UNSET
        elif isinstance(self.violation_verified_at, datetime.datetime):
            violation_verified_at = self.violation_verified_at.isoformat()
        else:
            violation_verified_at = self.violation_verified_at

        interference: Union[None, Unset, bool]
        if isinstance(self.interference, Unset):
            interference = UNSET
        else:
            interference = self.interference

        interference_verified_at: Union[None, Unset, str]
        if isinstance(self.interference_verified_at, Unset):
            interference_verified_at = UNSET
        elif isinstance(self.interference_verified_at, datetime.datetime):
            interference_verified_at = self.interference_verified_at.isoformat()
        else:
            interference_verified_at = self.interference_verified_at

        element_id = self.element_id

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        violated_grant_id: Union[None, Unset, str]
        if isinstance(self.violated_grant_id, Unset):
            violated_grant_id = UNSET
        else:
            violated_grant_id = self.violated_grant_id

        annotations: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.annotations, Unset):
            annotations = UNSET
        elif isinstance(self.annotations, list):
            annotations = []
            for annotations_type_0_item_data in self.annotations:
                annotations_type_0_item = annotations_type_0_item_data.to_dict()
                annotations.append(annotations_type_0_item)

        else:
            annotations = self.annotations

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "monitor_id": monitor_id,
                "types": types,
                "format": format_,
                "min_freq": min_freq,
                "starts_at": starts_at,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if collection_id is not UNSET:
            field_dict["collection_id"] = collection_id
        if description is not UNSET:
            field_dict["description"] = description
        if max_freq is not UNSET:
            field_dict["max_freq"] = max_freq
        if freq_step is not UNSET:
            field_dict["freq_step"] = freq_step
        if ends_at is not UNSET:
            field_dict["ends_at"] = ends_at
        if data is not UNSET:
            field_dict["data"] = data
        if violation is not UNSET:
            field_dict["violation"] = violation
        if violation_verified_at is not UNSET:
            field_dict["violation_verified_at"] = violation_verified_at
        if interference is not UNSET:
            field_dict["interference"] = interference
        if interference_verified_at is not UNSET:
            field_dict["interference_verified_at"] = interference_verified_at
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if violated_grant_id is not UNSET:
            field_dict["violated_grant_id"] = violated_grant_id
        if annotations is not UNSET:
            field_dict["annotations"] = annotations

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.annotation import Annotation

        d = src_dict.copy()
        monitor_id = d.pop("monitor_id")

        types = d.pop("types")

        format_ = d.pop("format")

        min_freq = d.pop("min_freq")

        starts_at = isoparse(d.pop("starts_at"))

        id = d.pop("id", UNSET)

        def _parse_collection_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        collection_id = _parse_collection_id(d.pop("collection_id", UNSET))

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        def _parse_max_freq(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        max_freq = _parse_max_freq(d.pop("max_freq", UNSET))

        def _parse_freq_step(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        freq_step = _parse_freq_step(d.pop("freq_step", UNSET))

        def _parse_ends_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                ends_at_type_0 = isoparse(data)

                return ends_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        ends_at = _parse_ends_at(d.pop("ends_at", UNSET))

        def _parse_data(data: object) -> Union[File, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, bytes):
                    raise TypeError()
                data_type_0 = File(payload=BytesIO(data))

                return data_type_0
            except:  # noqa: E722
                pass
            return cast(Union[File, None, Unset], data)

        data = _parse_data(d.pop("data", UNSET))

        def _parse_violation(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        violation = _parse_violation(d.pop("violation", UNSET))

        def _parse_violation_verified_at(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                violation_verified_at_type_0 = isoparse(data)

                return violation_verified_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        violation_verified_at = _parse_violation_verified_at(
            d.pop("violation_verified_at", UNSET)
        )

        def _parse_interference(data: object) -> Union[None, Unset, bool]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, bool], data)

        interference = _parse_interference(d.pop("interference", UNSET))

        def _parse_interference_verified_at(
            data: object,
        ) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                interference_verified_at_type_0 = isoparse(data)

                return interference_verified_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        interference_verified_at = _parse_interference_verified_at(
            d.pop("interference_verified_at", UNSET)
        )

        element_id = d.pop("element_id", UNSET)

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_violated_grant_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        violated_grant_id = _parse_violated_grant_id(d.pop("violated_grant_id", UNSET))

        def _parse_annotations(data: object) -> Union[List["Annotation"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                annotations_type_0 = []
                _annotations_type_0 = data
                for annotations_type_0_item_data in _annotations_type_0:
                    annotations_type_0_item = Annotation.from_dict(
                        annotations_type_0_item_data
                    )

                    annotations_type_0.append(annotations_type_0_item)

                return annotations_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["Annotation"], None, Unset], data)

        annotations = _parse_annotations(d.pop("annotations", UNSET))

        observation = cls(
            monitor_id=monitor_id,
            types=types,
            format_=format_,
            min_freq=min_freq,
            starts_at=starts_at,
            id=id,
            collection_id=collection_id,
            description=description,
            max_freq=max_freq,
            freq_step=freq_step,
            ends_at=ends_at,
            data=data,
            violation=violation,
            violation_verified_at=violation_verified_at,
            interference=interference,
            interference_verified_at=interference_verified_at,
            element_id=element_id,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            violated_grant_id=violated_grant_id,
            annotations=annotations,
        )

        observation.additional_properties = d
        return observation

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
