import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.annotation_value import AnnotationValue


T = TypeVar("T", bound="Annotation")


@_attrs_define
class Annotation:
    """An annotation attached to an Observation.

    Attributes:
        observation_id (str): The observation id this annotation is associated with.
        type (str): The annotation type.
        name (str): The annotation name.
        value (AnnotationValue): The annotation value; any JSON object.
        id (Union[Unset, str]): The id of the annotation.
        description (Union[None, Unset, str]): A summary of the annotation data.
        creator_id (Union[Unset, str]): The user id of the creator.
        updater_id (Union[None, Unset, str]): The user id of the updater.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last modification time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        violated_grant_id (Union[None, Unset, str]): The id of the grant that was violated, if any.
    """

    observation_id: str
    type: str
    name: str
    value: "AnnotationValue"
    id: Union[Unset, str] = UNSET
    description: Union[None, Unset, str] = UNSET
    creator_id: Union[Unset, str] = UNSET
    updater_id: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    violated_grant_id: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "observation_id": {"put": True, "post": True},
        "type": {"put": None, "post": None},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "value": {"put": None, "post": None},
        "creator_id": {"put": True, "post": True},
        "updater_id": {"put": True, "post": True},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "violated_grant_id": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        observation_id = self.observation_id

        type = self.type

        name = self.name

        value = self.value.to_dict()

        id = self.id

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        creator_id = self.creator_id

        updater_id: Union[None, Unset, str]
        if isinstance(self.updater_id, Unset):
            updater_id = UNSET
        else:
            updater_id = self.updater_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        violated_grant_id: Union[None, Unset, str]
        if isinstance(self.violated_grant_id, Unset):
            violated_grant_id = UNSET
        else:
            violated_grant_id = self.violated_grant_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "observation_id": observation_id,
                "type": type,
                "name": name,
                "value": value,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if description is not UNSET:
            field_dict["description"] = description
        if creator_id is not UNSET:
            field_dict["creator_id"] = creator_id
        if updater_id is not UNSET:
            field_dict["updater_id"] = updater_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if violated_grant_id is not UNSET:
            field_dict["violated_grant_id"] = violated_grant_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.annotation_value import AnnotationValue

        d = src_dict.copy()
        observation_id = d.pop("observation_id")

        type = d.pop("type")

        name = d.pop("name")

        value = AnnotationValue.from_dict(d.pop("value"))

        id = d.pop("id", UNSET)

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        creator_id = d.pop("creator_id", UNSET)

        def _parse_updater_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        updater_id = _parse_updater_id(d.pop("updater_id", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        def _parse_violated_grant_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        violated_grant_id = _parse_violated_grant_id(d.pop("violated_grant_id", UNSET))

        annotation = cls(
            observation_id=observation_id,
            type=type,
            name=name,
            value=value,
            id=id,
            description=description,
            creator_id=creator_id,
            updater_id=updater_id,
            created_at=created_at,
            updated_at=updated_at,
            deleted_at=deleted_at,
            violated_grant_id=violated_grant_id,
        )

        annotation.additional_properties = d
        return annotation

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
