from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.observation import Observation


T = TypeVar("T", bound="ObservationList")


@_attrs_define
class ObservationList:
    """
    Attributes:
        observations (Union[Unset, List['Observation']]):
    """

    observations: Union[Unset, List["Observation"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {"observations": {"put": None, "post": None}}

    def to_dict(self) -> Dict[str, Any]:
        observations: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.observations, Unset):
            observations = []
            for observations_item_data in self.observations:
                observations_item = observations_item_data.to_dict()
                observations.append(observations_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if observations is not UNSET:
            field_dict["observations"] = observations

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.observation import Observation

        d = src_dict.copy()
        observations = []
        _observations = d.pop("observations", UNSET)
        for observations_item_data in _observations or []:
            observations_item = Observation.from_dict(observations_item_data)

            observations.append(observations_item)

        observation_list = cls(
            observations=observations,
        )

        observation_list.additional_properties = d
        return observation_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
