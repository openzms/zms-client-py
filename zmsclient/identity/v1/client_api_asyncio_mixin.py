import functools

from .api.element.create_element import (
    asyncio_detailed as create_element_asyncio_detailed,
)
from .api.element.delete_element import (
    asyncio_detailed as delete_element_asyncio_detailed,
)
from .api.element.get_element import asyncio_detailed as get_element_asyncio_detailed
from .api.element.list_elements import (
    asyncio_detailed as list_elements_asyncio_detailed,
)
from .api.element.update_element import (
    asyncio_detailed as update_element_asyncio_detailed,
)
from .api.health.get_alive import asyncio_detailed as get_alive_asyncio_detailed
from .api.health.get_ready import asyncio_detailed as get_ready_asyncio_detailed
from .api.role.list_roles import asyncio_detailed as list_roles_asyncio_detailed
from .api.role_binding.create_role_binding import (
    asyncio_detailed as create_role_binding_asyncio_detailed,
)
from .api.role_binding.delete_role_binding import (
    asyncio_detailed as delete_role_binding_asyncio_detailed,
)
from .api.role_binding.get_role_binding import (
    asyncio_detailed as get_role_binding_asyncio_detailed,
)
from .api.role_binding.list_role_bindings import (
    asyncio_detailed as list_role_bindings_asyncio_detailed,
)
from .api.role_binding.update_role_binding import (
    asyncio_detailed as update_role_binding_asyncio_detailed,
)
from .api.service.list_services import (
    asyncio_detailed as list_services_asyncio_detailed,
)
from .api.subscription.create_subscription import (
    asyncio_detailed as create_subscription_asyncio_detailed,
)
from .api.subscription.delete_subscription import (
    asyncio_detailed as delete_subscription_asyncio_detailed,
)
from .api.subscription.get_subscription_events import (
    asyncio_detailed as get_subscription_events_asyncio_detailed,
)
from .api.subscription.get_subscriptions import (
    asyncio_detailed as get_subscriptions_asyncio_detailed,
)
from .api.token.create_token import asyncio_detailed as create_token_asyncio_detailed
from .api.token.delete_token import asyncio_detailed as delete_token_asyncio_detailed
from .api.token.delete_token_this import (
    asyncio_detailed as delete_token_this_asyncio_detailed,
)
from .api.token.get_token import asyncio_detailed as get_token_asyncio_detailed
from .api.token.get_token_this import (
    asyncio_detailed as get_token_this_asyncio_detailed,
)
from .api.user.create_user import asyncio_detailed as create_user_asyncio_detailed
from .api.user.delete_user import asyncio_detailed as delete_user_asyncio_detailed
from .api.user.get_user import asyncio_detailed as get_user_asyncio_detailed
from .api.user.list_user_elements import (
    asyncio_detailed as list_user_elements_asyncio_detailed,
)
from .api.user.list_user_role_bindings import (
    asyncio_detailed as list_user_role_bindings_asyncio_detailed,
)
from .api.user.list_user_tokens import (
    asyncio_detailed as list_user_tokens_asyncio_detailed,
)
from .api.user.list_users import asyncio_detailed as list_users_asyncio_detailed
from .api.user.update_user import asyncio_detailed as update_user_asyncio_detailed
from .api.version.get_version import asyncio_detailed as get_version_asyncio_detailed


class ClientApiAsyncioMixin:
    def _call_api_func(self, func, *args, **kwargs):
        return func(*args, **kwargs, client=self)

    def get_version(self, *args, **kwargs):
        return self._call_api_func(get_version_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_version, wrapped=get_version_asyncio_detailed)

    def get_alive(self, *args, **kwargs):
        return self._call_api_func(get_alive_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_alive, wrapped=get_alive_asyncio_detailed)

    def get_ready(self, *args, **kwargs):
        return self._call_api_func(get_ready_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_ready, wrapped=get_ready_asyncio_detailed)

    def create_token(self, *args, **kwargs):
        return self._call_api_func(create_token_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(create_token, wrapped=create_token_asyncio_detailed)

    def delete_token_this(self, *args, **kwargs):
        return self._call_api_func(delete_token_this_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        delete_token_this, wrapped=delete_token_this_asyncio_detailed
    )

    def get_token_this(self, *args, **kwargs):
        return self._call_api_func(get_token_this_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_token_this, wrapped=get_token_this_asyncio_detailed)

    def get_token(self, *args, **kwargs):
        return self._call_api_func(get_token_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_token, wrapped=get_token_asyncio_detailed)

    def delete_token(self, *args, **kwargs):
        return self._call_api_func(delete_token_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(delete_token, wrapped=delete_token_asyncio_detailed)

    def list_role_bindings(self, *args, **kwargs):
        return self._call_api_func(list_role_bindings_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        list_role_bindings, wrapped=list_role_bindings_asyncio_detailed
    )

    def create_role_binding(self, *args, **kwargs):
        return self._call_api_func(
            create_role_binding_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_role_binding, wrapped=create_role_binding_asyncio_detailed
    )

    def get_role_binding(self, *args, **kwargs):
        return self._call_api_func(get_role_binding_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        get_role_binding, wrapped=get_role_binding_asyncio_detailed
    )

    def update_role_binding(self, *args, **kwargs):
        return self._call_api_func(
            update_role_binding_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        update_role_binding, wrapped=update_role_binding_asyncio_detailed
    )

    def delete_role_binding(self, *args, **kwargs):
        return self._call_api_func(
            delete_role_binding_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        delete_role_binding, wrapped=delete_role_binding_asyncio_detailed
    )

    def list_elements(self, *args, **kwargs):
        return self._call_api_func(list_elements_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(list_elements, wrapped=list_elements_asyncio_detailed)

    def create_element(self, *args, **kwargs):
        return self._call_api_func(create_element_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(create_element, wrapped=create_element_asyncio_detailed)

    def get_element(self, *args, **kwargs):
        return self._call_api_func(get_element_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_element, wrapped=get_element_asyncio_detailed)

    def update_element(self, *args, **kwargs):
        return self._call_api_func(update_element_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(update_element, wrapped=update_element_asyncio_detailed)

    def delete_element(self, *args, **kwargs):
        return self._call_api_func(delete_element_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(delete_element, wrapped=delete_element_asyncio_detailed)

    def list_users(self, *args, **kwargs):
        return self._call_api_func(list_users_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(list_users, wrapped=list_users_asyncio_detailed)

    def create_user(self, *args, **kwargs):
        return self._call_api_func(create_user_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(create_user, wrapped=create_user_asyncio_detailed)

    def get_user(self, *args, **kwargs):
        return self._call_api_func(get_user_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(get_user, wrapped=get_user_asyncio_detailed)

    def update_user(self, *args, **kwargs):
        return self._call_api_func(update_user_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(update_user, wrapped=update_user_asyncio_detailed)

    def delete_user(self, *args, **kwargs):
        return self._call_api_func(delete_user_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(delete_user, wrapped=delete_user_asyncio_detailed)

    def list_user_tokens(self, *args, **kwargs):
        return self._call_api_func(list_user_tokens_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        list_user_tokens, wrapped=list_user_tokens_asyncio_detailed
    )

    def list_user_elements(self, *args, **kwargs):
        return self._call_api_func(list_user_elements_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        list_user_elements, wrapped=list_user_elements_asyncio_detailed
    )

    def list_user_role_bindings(self, *args, **kwargs):
        return self._call_api_func(
            list_user_role_bindings_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        list_user_role_bindings, wrapped=list_user_role_bindings_asyncio_detailed
    )

    def list_services(self, *args, **kwargs):
        return self._call_api_func(list_services_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(list_services, wrapped=list_services_asyncio_detailed)

    def list_roles(self, *args, **kwargs):
        return self._call_api_func(list_roles_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(list_roles, wrapped=list_roles_asyncio_detailed)

    def get_subscriptions(self, *args, **kwargs):
        return self._call_api_func(get_subscriptions_asyncio_detailed, *args, **kwargs)

    functools.update_wrapper(
        get_subscriptions, wrapped=get_subscriptions_asyncio_detailed
    )

    def create_subscription(self, *args, **kwargs):
        return self._call_api_func(
            create_subscription_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        create_subscription, wrapped=create_subscription_asyncio_detailed
    )

    def delete_subscription(self, *args, **kwargs):
        return self._call_api_func(
            delete_subscription_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        delete_subscription, wrapped=delete_subscription_asyncio_detailed
    )

    def get_subscription_events(self, *args, **kwargs):
        return self._call_api_func(
            get_subscription_events_asyncio_detailed, *args, **kwargs
        )

    functools.update_wrapper(
        get_subscription_events, wrapped=get_subscription_events_asyncio_detailed
    )
