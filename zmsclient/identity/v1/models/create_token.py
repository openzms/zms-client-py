import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.create_token_method import CreateTokenMethod
from ..models.create_token_token_type import CreateTokenTokenType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.create_token_credential_idp import CreateTokenCredentialIdp
    from ..models.create_token_credential_password import CreateTokenCredentialPassword
    from ..models.create_token_credential_token import CreateTokenCredentialToken


T = TypeVar("T", bound="CreateToken")


@_attrs_define
class CreateToken:
    """
    Attributes:
        method (CreateTokenMethod):
        credential (Union['CreateTokenCredentialIdp', 'CreateTokenCredentialPassword', 'CreateTokenCredentialToken',
            Unset]):
        role_binding_ids (Union[Unset, List[str]]): A list of rolebinding IDs that the new token should include.  Must
            be either owned by the authenticating user in the password and idp method cases; or in the `token` method case,
            a subset of the rolebinding IDs associated with the token in the request body.
        expires_at (Union[Unset, datetime.datetime]): The expiration time as an ISO 8601 string, if any. Example:
            2023-06-30T22:11:05.242530Z.
        admin_if_bound (Union[Unset, bool]): By default, we do not add the admin role to any token unless this field is
            set `true`.  This field will add in an admin role if the user has one, but will not cause an error if the user
            does not have that role binding.  Finally, if this field is set `true`, it will augment the values in the
            `role_binding_ids`, but if it is `false` or unset, and if the user's `admin` role_binding ID is present in the
            `role_binding_ids` field, the user's admin role binding will be included in the new token.
        token_type (Union[Unset, CreateTokenTokenType]): Set the token type explicitly; may be ignored.  This option may
            be deprecated and removed in the future.  For now, API users not building a frontend should not set this option,
            or should always set `pat` Default: CreateTokenTokenType.PAT.
    """

    method: CreateTokenMethod
    credential: Union[
        "CreateTokenCredentialIdp",
        "CreateTokenCredentialPassword",
        "CreateTokenCredentialToken",
        Unset,
    ] = UNSET
    role_binding_ids: Union[Unset, List[str]] = UNSET
    expires_at: Union[Unset, datetime.datetime] = UNSET
    admin_if_bound: Union[Unset, bool] = UNSET
    token_type: Union[Unset, CreateTokenTokenType] = CreateTokenTokenType.PAT
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "method": {"put": None, "post": None},
        "credential": {"put": None, "post": None},
        "role_binding_ids": {"put": None, "post": None},
        "expires_at": {"put": None, "post": None},
        "admin_if_bound": {"put": None, "post": None},
        "token_type": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        from ..models.create_token_credential_password import (
            CreateTokenCredentialPassword,
        )
        from ..models.create_token_credential_token import CreateTokenCredentialToken

        method = self.method.value

        credential: Union[Dict[str, Any], Unset]
        if isinstance(self.credential, Unset):
            credential = UNSET
        elif isinstance(self.credential, CreateTokenCredentialPassword):
            credential = self.credential.to_dict()
        elif isinstance(self.credential, CreateTokenCredentialToken):
            credential = self.credential.to_dict()
        else:
            credential = self.credential.to_dict()

        role_binding_ids: Union[Unset, List[str]] = UNSET
        if not isinstance(self.role_binding_ids, Unset):
            role_binding_ids = self.role_binding_ids

        expires_at: Union[Unset, str] = UNSET
        if not isinstance(self.expires_at, Unset):
            expires_at = self.expires_at.isoformat()

        admin_if_bound = self.admin_if_bound

        token_type: Union[Unset, str] = UNSET
        if not isinstance(self.token_type, Unset):
            token_type = self.token_type.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "method": method,
            }
        )
        if credential is not UNSET:
            field_dict["credential"] = credential
        if role_binding_ids is not UNSET:
            field_dict["role_binding_ids"] = role_binding_ids
        if expires_at is not UNSET:
            field_dict["expires_at"] = expires_at
        if admin_if_bound is not UNSET:
            field_dict["admin_if_bound"] = admin_if_bound
        if token_type is not UNSET:
            field_dict["token_type"] = token_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.create_token_credential_idp import CreateTokenCredentialIdp
        from ..models.create_token_credential_password import (
            CreateTokenCredentialPassword,
        )
        from ..models.create_token_credential_token import CreateTokenCredentialToken

        d = src_dict.copy()
        method = CreateTokenMethod(d.pop("method"))

        def _parse_credential(
            data: object,
        ) -> Union[
            "CreateTokenCredentialIdp",
            "CreateTokenCredentialPassword",
            "CreateTokenCredentialToken",
            Unset,
        ]:
            if isinstance(data, Unset):
                return data
            _dval = method
            if str(_dval) == "password":
                if not isinstance(data, dict):
                    raise TypeError()
                credential_type_0 = CreateTokenCredentialPassword.from_dict(data)

                return credential_type_0
            if str(_dval) == "token":
                if not isinstance(data, dict):
                    raise TypeError()
                credential_type_1 = CreateTokenCredentialToken.from_dict(data)

                return credential_type_1
            if str(_dval) == "idp":
                if not isinstance(data, dict):
                    raise TypeError()
                credential_type_2 = CreateTokenCredentialIdp.from_dict(data)

                return credential_type_2
            raise TypeError("unmatched determinant")

        credential = _parse_credential(d.pop("credential", UNSET))

        role_binding_ids = cast(List[str], d.pop("role_binding_ids", UNSET))

        _expires_at = d.pop("expires_at", UNSET)
        expires_at: Union[Unset, datetime.datetime]
        if isinstance(_expires_at, Unset):
            expires_at = UNSET
        else:
            expires_at = isoparse(_expires_at)

        admin_if_bound = d.pop("admin_if_bound", UNSET)

        _token_type = d.pop("token_type", UNSET)
        token_type: Union[Unset, CreateTokenTokenType]
        if isinstance(_token_type, Unset):
            token_type = UNSET
        else:
            token_type = CreateTokenTokenType(_token_type)

        create_token = cls(
            method=method,
            credential=credential,
            role_binding_ids=role_binding_ids,
            expires_at=expires_at,
            admin_if_bound=admin_if_bound,
            token_type=token_type,
        )

        create_token.additional_properties = d
        return create_token

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
