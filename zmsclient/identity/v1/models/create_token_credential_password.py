from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="CreateTokenCredentialPassword")


@_attrs_define
class CreateTokenCredentialPassword:
    """
    Attributes:
        username (str): The username or email address.
        password (str): The user's password, for direct login.  `password` must be base64-encoded.
    """

    username: str
    password: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "username": {"put": None, "post": None},
        "password": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        username = self.username

        password = self.password

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "username": username,
                "password": password,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        username = d.pop("username")

        password = d.pop("password")

        create_token_credential_password = cls(
            username=username,
            password=password,
        )

        create_token_credential_password.additional_properties = d
        return create_token_credential_password

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
