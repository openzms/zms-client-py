from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="ElementAttribute")


@_attrs_define
class ElementAttribute:
    """
    Attributes:
        id (Union[Unset, str]): The id of the element attribute.
        element_id (Union[Unset, str]): The associated element id.
        kind (Union[Unset, str]): The attribute kind (e.g. link).
        name (Union[Unset, str]): The attribute name.
        value (Union[Unset, str]): The attribute value.
        description (Union[Unset, str]): A brief description of the attribute (e.g., summary of URL content or purpose).
    """

    id: Union[Unset, str] = UNSET
    element_id: Union[Unset, str] = UNSET
    kind: Union[Unset, str] = UNSET
    name: Union[Unset, str] = UNSET
    value: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "element_id": {"put": True, "post": True},
        "kind": {"put": None, "post": None},
        "name": {"put": None, "post": None},
        "value": {"put": None, "post": None},
        "description": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        element_id = self.element_id

        kind = self.kind

        name = self.name

        value = self.value

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if kind is not UNSET:
            field_dict["kind"] = kind
        if name is not UNSET:
            field_dict["name"] = name
        if value is not UNSET:
            field_dict["value"] = value
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        element_id = d.pop("element_id", UNSET)

        kind = d.pop("kind", UNSET)

        name = d.pop("name", UNSET)

        value = d.pop("value", UNSET)

        description = d.pop("description", UNSET)

        element_attribute = cls(
            id=id,
            element_id=element_id,
            kind=kind,
            name=name,
            value=value,
            description=description,
        )

        element_attribute.additional_properties = d
        return element_attribute

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
