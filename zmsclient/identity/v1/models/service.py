import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="Service")


@_attrs_define
class Service:
    """
    Attributes:
        id (Union[Unset, str]): The id of the service.
        name (Union[Unset, str]): The name of the service. Example: zms-identity.
        kind (Union[Unset, str]): The kind of service. Example: zms.
        endpoint (Union[Unset, str]): The service's gRPC endpoint.
        endpoint_api_uri (Union[Unset, str]): The services's RESTful API endpoint Example: https://localhost:3000.
        description (Union[Unset, str]): A brief description of the service.
        version (Union[Unset, str]): The service's version string.
        api_version (Union[Unset, str]): The service's RESTful API version string.
        enabled (Union[Unset, bool]): True if the service is enabled for use.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Last update time.
        heartbeat_at (Union[None, Unset, datetime.datetime]): Last heartbeat message time.
    """

    id: Union[Unset, str] = UNSET
    name: Union[Unset, str] = UNSET
    kind: Union[Unset, str] = UNSET
    endpoint: Union[Unset, str] = UNSET
    endpoint_api_uri: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    version: Union[Unset, str] = UNSET
    api_version: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    heartbeat_at: Union[None, Unset, datetime.datetime] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "name": {"put": True, "post": None},
        "kind": {"put": True, "post": None},
        "endpoint": {"put": None, "post": None},
        "endpoint_api_uri": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "version": {"put": None, "post": None},
        "api_version": {"put": None, "post": None},
        "enabled": {"put": None, "post": None},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "heartbeat_at": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        name = self.name

        kind = self.kind

        endpoint = self.endpoint

        endpoint_api_uri = self.endpoint_api_uri

        description = self.description

        version = self.version

        api_version = self.api_version

        enabled = self.enabled

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        heartbeat_at: Union[None, Unset, str]
        if isinstance(self.heartbeat_at, Unset):
            heartbeat_at = UNSET
        elif isinstance(self.heartbeat_at, datetime.datetime):
            heartbeat_at = self.heartbeat_at.isoformat()
        else:
            heartbeat_at = self.heartbeat_at

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if name is not UNSET:
            field_dict["name"] = name
        if kind is not UNSET:
            field_dict["kind"] = kind
        if endpoint is not UNSET:
            field_dict["endpoint"] = endpoint
        if endpoint_api_uri is not UNSET:
            field_dict["endpoint_api_uri"] = endpoint_api_uri
        if description is not UNSET:
            field_dict["description"] = description
        if version is not UNSET:
            field_dict["version"] = version
        if api_version is not UNSET:
            field_dict["api_version"] = api_version
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if heartbeat_at is not UNSET:
            field_dict["heartbeat_at"] = heartbeat_at

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id", UNSET)

        name = d.pop("name", UNSET)

        kind = d.pop("kind", UNSET)

        endpoint = d.pop("endpoint", UNSET)

        endpoint_api_uri = d.pop("endpoint_api_uri", UNSET)

        description = d.pop("description", UNSET)

        version = d.pop("version", UNSET)

        api_version = d.pop("api_version", UNSET)

        enabled = d.pop("enabled", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_heartbeat_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                heartbeat_at_type_0 = isoparse(data)

                return heartbeat_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        heartbeat_at = _parse_heartbeat_at(d.pop("heartbeat_at", UNSET))

        service = cls(
            id=id,
            name=name,
            kind=kind,
            endpoint=endpoint,
            endpoint_api_uri=endpoint_api_uri,
            description=description,
            version=version,
            api_version=api_version,
            enabled=enabled,
            created_at=created_at,
            updated_at=updated_at,
            heartbeat_at=heartbeat_at,
        )

        service.additional_properties = d
        return service

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
