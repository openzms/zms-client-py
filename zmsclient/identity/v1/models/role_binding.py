import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.role import Role


T = TypeVar("T", bound="RoleBinding")


@_attrs_define
class RoleBinding:
    """
    Attributes:
        id (Union[Unset, str]): The id of the rolebinding.
        role_id (Union[Unset, str]): The id of the role.
        user_id (Union[Unset, str]): The id of the user.
        element_id (Union[Unset, str]): The id of the element this binding is associated with, if any.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        approved_at (Union[None, Unset, datetime.datetime]): Approval time, if any.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time, if any.
        role (Union[None, Unset, Role]):
    """

    id: Union[Unset, str] = UNSET
    role_id: Union[Unset, str] = UNSET
    user_id: Union[Unset, str] = UNSET
    element_id: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    approved_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    role: Union[None, Unset, "Role"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "role_id": {"put": None, "post": None},
        "user_id": {"put": None, "post": None},
        "element_id": {"put": None, "post": None},
        "created_at": {"put": True, "post": True},
        "approved_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "role": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        role_id = self.role_id

        user_id = self.user_id

        element_id = self.element_id

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        approved_at: Union[None, Unset, str]
        if isinstance(self.approved_at, Unset):
            approved_at = UNSET
        elif isinstance(self.approved_at, datetime.datetime):
            approved_at = self.approved_at.isoformat()
        else:
            approved_at = self.approved_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        role: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.role is None:
            role = None
        elif not isinstance(self.role, Unset):
            role = self.role.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if id is not UNSET:
            field_dict["id"] = id
        if role_id is not UNSET:
            field_dict["role_id"] = role_id
        if user_id is not UNSET:
            field_dict["user_id"] = user_id
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if approved_at is not UNSET:
            field_dict["approved_at"] = approved_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if role is not UNSET:
            field_dict["role"] = role

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.role import Role

        d = src_dict.copy()
        id = d.pop("id", UNSET)

        role_id = d.pop("role_id", UNSET)

        user_id = d.pop("user_id", UNSET)

        element_id = d.pop("element_id", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_approved_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                approved_at_type_0 = isoparse(data)

                return approved_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        approved_at = _parse_approved_at(d.pop("approved_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        _role = d.pop("role", UNSET)
        role: Union[None, Unset, Role]
        if isinstance(_role, Unset):
            role = UNSET
        elif _role is None:
            role = None
        else:
            role = Role.from_dict(_role)

        role_binding = cls(
            id=id,
            role_id=role_id,
            user_id=user_id,
            element_id=element_id,
            created_at=created_at,
            approved_at=approved_at,
            deleted_at=deleted_at,
            role=role,
        )

        role_binding.additional_properties = d
        return role_binding

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
