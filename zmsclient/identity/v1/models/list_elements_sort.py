from enum import Enum


class ListElementsSort(str, Enum):
    APPROVED_AT = "approved_at"
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    ENABLED = "enabled"
    HTML_URL = "html_url"
    NAME = "name"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
