from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.element import Element


T = TypeVar("T", bound="ElementList")


@_attrs_define
class ElementList:
    """
    Attributes:
        elements (Union[Unset, List['Element']]):
    """

    elements: Union[Unset, List["Element"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {"elements": {"put": None, "post": None}}

    def to_dict(self) -> Dict[str, Any]:
        elements: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.elements, Unset):
            elements = []
            for elements_item_data in self.elements:
                elements_item = elements_item_data.to_dict()
                elements.append(elements_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if elements is not UNSET:
            field_dict["elements"] = elements

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.element import Element

        d = src_dict.copy()
        elements = []
        _elements = d.pop("elements", UNSET)
        for elements_item_data in _elements or []:
            elements_item = Element.from_dict(elements_item_data)

            elements.append(elements_item)

        element_list = cls(
            elements=elements,
        )

        element_list.additional_properties = d
        return element_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
