import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.token_role_binding import TokenRoleBinding


T = TypeVar("T", bound="Token")


@_attrs_define
class Token:
    """
    Attributes:
        user_id (str): The owning user id.
        token (str): The token value.
        issued_at (datetime.datetime): The issue time as an ISO 8601 string. Example: 2023-06-30T22:10:05.242530Z.
        id (Union[Unset, str]): The token id.
        expires_at (Union[None, Unset, datetime.datetime]): The expiration time as an ISO 8601 string, if any. Example:
            2023-06-30T22:11:05.242530Z.
        role_bindings (Union[Unset, List['TokenRoleBinding']]): A list of TokenRoleBindings this token is associated
            with.
    """

    user_id: str
    token: str
    issued_at: datetime.datetime
    id: Union[Unset, str] = UNSET
    expires_at: Union[None, Unset, datetime.datetime] = UNSET
    role_bindings: Union[Unset, List["TokenRoleBinding"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "user_id": {"put": True, "post": True},
        "token": {"put": True, "post": True},
        "expires_at": {"put": True, "post": True},
        "issued_at": {"put": True, "post": True},
        "role_bindings": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        user_id = self.user_id

        token = self.token

        issued_at = self.issued_at.isoformat()

        id = self.id

        expires_at: Union[None, Unset, str]
        if isinstance(self.expires_at, Unset):
            expires_at = UNSET
        elif isinstance(self.expires_at, datetime.datetime):
            expires_at = self.expires_at.isoformat()
        else:
            expires_at = self.expires_at

        role_bindings: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.role_bindings, Unset):
            role_bindings = []
            for role_bindings_item_data in self.role_bindings:
                role_bindings_item = role_bindings_item_data.to_dict()
                role_bindings.append(role_bindings_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "user_id": user_id,
                "token": token,
                "issued_at": issued_at,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if expires_at is not UNSET:
            field_dict["expires_at"] = expires_at
        if role_bindings is not UNSET:
            field_dict["role_bindings"] = role_bindings

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.token_role_binding import TokenRoleBinding

        d = src_dict.copy()
        user_id = d.pop("user_id")

        token = d.pop("token")

        issued_at = isoparse(d.pop("issued_at"))

        id = d.pop("id", UNSET)

        def _parse_expires_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                expires_at_type_0 = isoparse(data)

                return expires_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        expires_at = _parse_expires_at(d.pop("expires_at", UNSET))

        role_bindings = []
        _role_bindings = d.pop("role_bindings", UNSET)
        for role_bindings_item_data in _role_bindings or []:
            role_bindings_item = TokenRoleBinding.from_dict(role_bindings_item_data)

            role_bindings.append(role_bindings_item)

        token = cls(
            user_id=user_id,
            token=token,
            issued_at=issued_at,
            id=id,
            expires_at=expires_at,
            role_bindings=role_bindings,
        )

        token.additional_properties = d
        return token

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
