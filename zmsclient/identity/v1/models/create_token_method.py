from enum import Enum


class CreateTokenMethod(str, Enum):
    IDP = "idp"
    PASSWORD = "password"
    TOKEN = "token"

    def __str__(self) -> str:
        return str(self.value)
