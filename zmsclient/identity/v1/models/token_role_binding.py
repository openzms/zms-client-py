from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.role_binding import RoleBinding


T = TypeVar("T", bound="TokenRoleBinding")


@_attrs_define
class TokenRoleBinding:
    """An object that associates a RoleBinding with a Token to express a Many (RoleBinding) to One (Token) relationship.

    Attributes:
        token_id (str): The token id.
        role_binding_id (str): The role binding id.
        role_binding (Union[None, Unset, RoleBinding]):
    """

    token_id: str
    role_binding_id: str
    role_binding: Union[None, Unset, "RoleBinding"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "token_id": {"put": True, "post": True},
        "role_binding_id": {"put": True, "post": True},
        "role_binding": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        token_id = self.token_id

        role_binding_id = self.role_binding_id

        role_binding: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.role_binding is None:
            role_binding = None
        elif not isinstance(self.role_binding, Unset):
            role_binding = self.role_binding.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "token_id": token_id,
                "role_binding_id": role_binding_id,
            }
        )
        if role_binding is not UNSET:
            field_dict["role_binding"] = role_binding

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.role_binding import RoleBinding

        d = src_dict.copy()
        token_id = d.pop("token_id")

        role_binding_id = d.pop("role_binding_id")

        _role_binding = d.pop("role_binding", UNSET)
        role_binding: Union[None, Unset, RoleBinding]
        if isinstance(_role_binding, Unset):
            role_binding = UNSET
        elif _role_binding is None:
            role_binding = None
        else:
            role_binding = RoleBinding.from_dict(_role_binding)

        token_role_binding = cls(
            token_id=token_id,
            role_binding_id=role_binding_id,
            role_binding=role_binding,
        )

        token_role_binding.additional_properties = d
        return token_role_binding

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
