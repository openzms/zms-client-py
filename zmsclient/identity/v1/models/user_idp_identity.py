from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="UserIdpIdentity")


@_attrs_define
class UserIdpIdentity:
    """
    Attributes:
        sub (str): OAuth2/OIDC subject unique identifier
        iss (str): Token issuer
        idp (str): IdP identifier
        email_address (str): Email address.
        id (Union[Unset, str]): The id of the IdP identity.
        aud (Union[None, Unset, str]): Token audience (OIDC client_id)
        name (Union[None, Unset, str]): IdP user display name
    """

    sub: str
    iss: str
    idp: str
    email_address: str
    id: Union[Unset, str] = UNSET
    aud: Union[None, Unset, str] = UNSET
    name: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "sub": {"put": None, "post": None},
        "aud": {"put": None, "post": None},
        "iss": {"put": None, "post": None},
        "idp": {"put": None, "post": None},
        "email_address": {"put": None, "post": None},
        "name": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        sub = self.sub

        iss = self.iss

        idp = self.idp

        email_address = self.email_address

        id = self.id

        aud: Union[None, Unset, str]
        if isinstance(self.aud, Unset):
            aud = UNSET
        else:
            aud = self.aud

        name: Union[None, Unset, str]
        if isinstance(self.name, Unset):
            name = UNSET
        else:
            name = self.name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "sub": sub,
                "iss": iss,
                "idp": idp,
                "email_address": email_address,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if aud is not UNSET:
            field_dict["aud"] = aud
        if name is not UNSET:
            field_dict["name"] = name

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        sub = d.pop("sub")

        iss = d.pop("iss")

        idp = d.pop("idp")

        email_address = d.pop("email_address")

        id = d.pop("id", UNSET)

        def _parse_aud(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        aud = _parse_aud(d.pop("aud", UNSET))

        def _parse_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        name = _parse_name(d.pop("name", UNSET))

        user_idp_identity = cls(
            sub=sub,
            iss=iss,
            idp=idp,
            email_address=email_address,
            id=id,
            aud=aud,
            name=name,
        )

        user_idp_identity.additional_properties = d
        return user_idp_identity

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
