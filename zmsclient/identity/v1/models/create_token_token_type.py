from enum import Enum


class CreateTokenTokenType(str, Enum):
    PAT = "pat"
    USER = "user"

    def __str__(self) -> str:
        return str(self.value)
