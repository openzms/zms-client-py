from enum import Enum


class ListUsersSort(str, Enum):
    CREATED_AT = "created_at"
    DELETED_AT = "deleted_at"
    ENABLED = "enabled"
    FULL_NAME = "full_name"
    NAME = "name"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
