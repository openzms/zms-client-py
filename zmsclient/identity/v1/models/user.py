import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.role_binding import RoleBinding
    from ..models.user_email_address import UserEmailAddress
    from ..models.user_idp_identity import UserIdpIdentity


T = TypeVar("T", bound="User")


@_attrs_define
class User:
    """
    Attributes:
        name (str): The username.
        id (Union[Unset, str]): The id of the user.
        given_name (Union[None, Unset, str]): Given name. Example: John.
        family_name (Union[None, Unset, str]): Family name. Example: Doe.
        full_name (Union[None, Unset, str]): Full name. Example: John Doe.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        updated_at (Union[None, Unset, datetime.datetime]): Creation time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        enabled (Union[Unset, bool]): User account status.
        password (Union[None, Unset, str]): The user's password, if any; only changeable, never returned.  `password`
            must be base64-encoded.
        primary_email_address_id (Union[None, Unset, str]): The id of the User's primary email address.
        primary_email_address (Union[None, Unset, UserEmailAddress]):
        email_addresses (Union[List['UserEmailAddress'], None, Unset]): List of all email addresses.
        idp_identities (Union[List['UserIdpIdentity'], None, Unset]):
        role_bindings (Union[List['RoleBinding'], None, Unset]):
    """

    name: str
    id: Union[Unset, str] = UNSET
    given_name: Union[None, Unset, str] = UNSET
    family_name: Union[None, Unset, str] = UNSET
    full_name: Union[None, Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    enabled: Union[Unset, bool] = UNSET
    password: Union[None, Unset, str] = UNSET
    primary_email_address_id: Union[None, Unset, str] = UNSET
    primary_email_address: Union[None, Unset, "UserEmailAddress"] = UNSET
    email_addresses: Union[List["UserEmailAddress"], None, Unset] = UNSET
    idp_identities: Union[List["UserIdpIdentity"], None, Unset] = UNSET
    role_bindings: Union[List["RoleBinding"], None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "name": {"put": None, "post": None},
        "given_name": {"put": None, "post": None},
        "family_name": {"put": None, "post": None},
        "full_name": {"put": None, "post": None},
        "created_at": {"put": True, "post": True},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "enabled": {"put": None, "post": None},
        "password": {"put": None, "post": None},
        "primary_email_address_id": {"put": None, "post": True},
        "primary_email_address": {"put": None, "post": None},
        "email_addresses": {"put": None, "post": None},
        "idp_identities": {"put": True, "post": True},
        "role_bindings": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        id = self.id

        given_name: Union[None, Unset, str]
        if isinstance(self.given_name, Unset):
            given_name = UNSET
        else:
            given_name = self.given_name

        family_name: Union[None, Unset, str]
        if isinstance(self.family_name, Unset):
            family_name = UNSET
        else:
            family_name = self.family_name

        full_name: Union[None, Unset, str]
        if isinstance(self.full_name, Unset):
            full_name = UNSET
        else:
            full_name = self.full_name

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        enabled = self.enabled

        password: Union[None, Unset, str]
        if isinstance(self.password, Unset):
            password = UNSET
        else:
            password = self.password

        primary_email_address_id: Union[None, Unset, str]
        if isinstance(self.primary_email_address_id, Unset):
            primary_email_address_id = UNSET
        else:
            primary_email_address_id = self.primary_email_address_id

        primary_email_address: Union[None, Unset, Dict[str, Any]] = UNSET
        if self.primary_email_address is None:
            primary_email_address = None
        elif not isinstance(self.primary_email_address, Unset):
            primary_email_address = self.primary_email_address.to_dict()

        email_addresses: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.email_addresses, Unset):
            email_addresses = UNSET
        elif isinstance(self.email_addresses, list):
            email_addresses = []
            for email_addresses_type_0_item_data in self.email_addresses:
                email_addresses_type_0_item = email_addresses_type_0_item_data.to_dict()
                email_addresses.append(email_addresses_type_0_item)

        else:
            email_addresses = self.email_addresses

        idp_identities: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.idp_identities, Unset):
            idp_identities = UNSET
        elif isinstance(self.idp_identities, list):
            idp_identities = []
            for idp_identities_type_0_item_data in self.idp_identities:
                idp_identities_type_0_item = idp_identities_type_0_item_data.to_dict()
                idp_identities.append(idp_identities_type_0_item)

        else:
            idp_identities = self.idp_identities

        role_bindings: Union[List[Dict[str, Any]], None, Unset]
        if isinstance(self.role_bindings, Unset):
            role_bindings = UNSET
        elif isinstance(self.role_bindings, list):
            role_bindings = []
            for role_bindings_type_0_item_data in self.role_bindings:
                role_bindings_type_0_item = role_bindings_type_0_item_data.to_dict()
                role_bindings.append(role_bindings_type_0_item)

        else:
            role_bindings = self.role_bindings

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if given_name is not UNSET:
            field_dict["given_name"] = given_name
        if family_name is not UNSET:
            field_dict["family_name"] = family_name
        if full_name is not UNSET:
            field_dict["full_name"] = full_name
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if password is not UNSET:
            field_dict["password"] = password
        if primary_email_address_id is not UNSET:
            field_dict["primary_email_address_id"] = primary_email_address_id
        if primary_email_address is not UNSET:
            field_dict["primary_email_address"] = primary_email_address
        if email_addresses is not UNSET:
            field_dict["email_addresses"] = email_addresses
        if idp_identities is not UNSET:
            field_dict["idp_identities"] = idp_identities
        if role_bindings is not UNSET:
            field_dict["role_bindings"] = role_bindings

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.role_binding import RoleBinding
        from ..models.user_email_address import UserEmailAddress
        from ..models.user_idp_identity import UserIdpIdentity

        d = src_dict.copy()
        name = d.pop("name")

        id = d.pop("id", UNSET)

        def _parse_given_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        given_name = _parse_given_name(d.pop("given_name", UNSET))

        def _parse_family_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        family_name = _parse_family_name(d.pop("family_name", UNSET))

        def _parse_full_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        full_name = _parse_full_name(d.pop("full_name", UNSET))

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        enabled = d.pop("enabled", UNSET)

        def _parse_password(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        password = _parse_password(d.pop("password", UNSET))

        def _parse_primary_email_address_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        primary_email_address_id = _parse_primary_email_address_id(
            d.pop("primary_email_address_id", UNSET)
        )

        _primary_email_address = d.pop("primary_email_address", UNSET)
        primary_email_address: Union[None, Unset, UserEmailAddress]
        if isinstance(_primary_email_address, Unset):
            primary_email_address = UNSET
        elif _primary_email_address is None:
            primary_email_address = None
        else:
            primary_email_address = UserEmailAddress.from_dict(_primary_email_address)

        def _parse_email_addresses(
            data: object,
        ) -> Union[List["UserEmailAddress"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                email_addresses_type_0 = []
                _email_addresses_type_0 = data
                for email_addresses_type_0_item_data in _email_addresses_type_0:
                    email_addresses_type_0_item = UserEmailAddress.from_dict(
                        email_addresses_type_0_item_data
                    )

                    email_addresses_type_0.append(email_addresses_type_0_item)

                return email_addresses_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["UserEmailAddress"], None, Unset], data)

        email_addresses = _parse_email_addresses(d.pop("email_addresses", UNSET))

        def _parse_idp_identities(
            data: object,
        ) -> Union[List["UserIdpIdentity"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                idp_identities_type_0 = []
                _idp_identities_type_0 = data
                for idp_identities_type_0_item_data in _idp_identities_type_0:
                    idp_identities_type_0_item = UserIdpIdentity.from_dict(
                        idp_identities_type_0_item_data
                    )

                    idp_identities_type_0.append(idp_identities_type_0_item)

                return idp_identities_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["UserIdpIdentity"], None, Unset], data)

        idp_identities = _parse_idp_identities(d.pop("idp_identities", UNSET))

        def _parse_role_bindings(
            data: object,
        ) -> Union[List["RoleBinding"], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                role_bindings_type_0 = []
                _role_bindings_type_0 = data
                for role_bindings_type_0_item_data in _role_bindings_type_0:
                    role_bindings_type_0_item = RoleBinding.from_dict(
                        role_bindings_type_0_item_data
                    )

                    role_bindings_type_0.append(role_bindings_type_0_item)

                return role_bindings_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List["RoleBinding"], None, Unset], data)

        role_bindings = _parse_role_bindings(d.pop("role_bindings", UNSET))

        user = cls(
            name=name,
            id=id,
            given_name=given_name,
            family_name=family_name,
            full_name=full_name,
            created_at=created_at,
            updated_at=updated_at,
            deleted_at=deleted_at,
            enabled=enabled,
            password=password,
            primary_email_address_id=primary_email_address_id,
            primary_email_address=primary_email_address,
            email_addresses=email_addresses,
            idp_identities=idp_identities,
            role_bindings=role_bindings,
        )

        user.additional_properties = d
        return user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
