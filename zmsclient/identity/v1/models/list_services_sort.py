from enum import Enum


class ListServicesSort(str, Enum):
    CREATED_AT = "created_at"
    ENABLED = "enabled"
    KIND = "kind"
    NAME = "name"
    UPDATED_AT = "updated_at"

    def __str__(self) -> str:
        return str(self.value)
