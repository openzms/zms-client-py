from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="CreateTokenCredentialIdp")


@_attrs_define
class CreateTokenCredentialIdp:
    """
    Attributes:
        idp (str): The IdP name.
        access_token (str): An access token from an IdP.
        expires_in (Union[Unset, int]):
        refresh_token (Union[Unset, str]): An access token from an IdP.
        refresh_expires_in (Union[Unset, int]):
    """

    idp: str
    access_token: str
    expires_in: Union[Unset, int] = UNSET
    refresh_token: Union[Unset, str] = UNSET
    refresh_expires_in: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "idp": {"put": None, "post": None},
        "access_token": {"put": None, "post": None},
        "expires_in": {"put": None, "post": None},
        "refresh_token": {"put": None, "post": None},
        "refresh_expires_in": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        idp = self.idp

        access_token = self.access_token

        expires_in = self.expires_in

        refresh_token = self.refresh_token

        refresh_expires_in = self.refresh_expires_in

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "idp": idp,
                "access_token": access_token,
            }
        )
        if expires_in is not UNSET:
            field_dict["expires_in"] = expires_in
        if refresh_token is not UNSET:
            field_dict["refresh_token"] = refresh_token
        if refresh_expires_in is not UNSET:
            field_dict["refresh_expires_in"] = refresh_expires_in

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        idp = d.pop("idp")

        access_token = d.pop("access_token")

        expires_in = d.pop("expires_in", UNSET)

        refresh_token = d.pop("refresh_token", UNSET)

        refresh_expires_in = d.pop("refresh_expires_in", UNSET)

        create_token_credential_idp = cls(
            idp=idp,
            access_token=access_token,
            expires_in=expires_in,
            refresh_token=refresh_token,
            refresh_expires_in=refresh_expires_in,
        )

        create_token_credential_idp.additional_properties = d
        return create_token_credential_idp

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
