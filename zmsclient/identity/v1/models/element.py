import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.element_attribute import ElementAttribute


T = TypeVar("T", bound="Element")


@_attrs_define
class Element:
    """
    Attributes:
        name (str): The name of the element. Example: POWDER.
        id (Union[Unset, str]): The id of the element.
        creator_user_id (Union[Unset, str]): The originating user id.
        description (Union[Unset, str]): A brief description of the element.
        created_at (Union[Unset, datetime.datetime]): Creation time.
        approved_at (Union[None, Unset, datetime.datetime]): Approval time.
        updated_at (Union[None, Unset, datetime.datetime]): Last update time.
        deleted_at (Union[None, Unset, datetime.datetime]): Deletion time.
        enabled (Union[Unset, bool]): Element is enabled to operate in OpenZMS. Default: False.
        html_url (Union[None, Unset, str]): A URL associated with this element that is meaningful to the requestor;
            opaque to OpenZMS.
        attributes (Union[Unset, List['ElementAttribute']]): A list of attributes associated with the element.
    """

    name: str
    id: Union[Unset, str] = UNSET
    creator_user_id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    created_at: Union[Unset, datetime.datetime] = UNSET
    approved_at: Union[None, Unset, datetime.datetime] = UNSET
    updated_at: Union[None, Unset, datetime.datetime] = UNSET
    deleted_at: Union[None, Unset, datetime.datetime] = UNSET
    enabled: Union[Unset, bool] = False
    html_url: Union[None, Unset, str] = UNSET
    attributes: Union[Unset, List["ElementAttribute"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "creator_user_id": {"put": True, "post": True},
        "name": {"put": None, "post": None},
        "description": {"put": None, "post": None},
        "created_at": {"put": True, "post": True},
        "approved_at": {"put": None, "post": None},
        "updated_at": {"put": True, "post": True},
        "deleted_at": {"put": True, "post": True},
        "enabled": {"put": None, "post": None},
        "html_url": {"put": None, "post": None},
        "attributes": {"put": True, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        id = self.id

        creator_user_id = self.creator_user_id

        description = self.description

        created_at: Union[Unset, str] = UNSET
        if not isinstance(self.created_at, Unset):
            created_at = self.created_at.isoformat()

        approved_at: Union[None, Unset, str]
        if isinstance(self.approved_at, Unset):
            approved_at = UNSET
        elif isinstance(self.approved_at, datetime.datetime):
            approved_at = self.approved_at.isoformat()
        else:
            approved_at = self.approved_at

        updated_at: Union[None, Unset, str]
        if isinstance(self.updated_at, Unset):
            updated_at = UNSET
        elif isinstance(self.updated_at, datetime.datetime):
            updated_at = self.updated_at.isoformat()
        else:
            updated_at = self.updated_at

        deleted_at: Union[None, Unset, str]
        if isinstance(self.deleted_at, Unset):
            deleted_at = UNSET
        elif isinstance(self.deleted_at, datetime.datetime):
            deleted_at = self.deleted_at.isoformat()
        else:
            deleted_at = self.deleted_at

        enabled = self.enabled

        html_url: Union[None, Unset, str]
        if isinstance(self.html_url, Unset):
            html_url = UNSET
        else:
            html_url = self.html_url

        attributes: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.attributes, Unset):
            attributes = []
            for attributes_item_data in self.attributes:
                attributes_item = attributes_item_data.to_dict()
                attributes.append(attributes_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if creator_user_id is not UNSET:
            field_dict["creator_user_id"] = creator_user_id
        if description is not UNSET:
            field_dict["description"] = description
        if created_at is not UNSET:
            field_dict["created_at"] = created_at
        if approved_at is not UNSET:
            field_dict["approved_at"] = approved_at
        if updated_at is not UNSET:
            field_dict["updated_at"] = updated_at
        if deleted_at is not UNSET:
            field_dict["deleted_at"] = deleted_at
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if html_url is not UNSET:
            field_dict["html_url"] = html_url
        if attributes is not UNSET:
            field_dict["attributes"] = attributes

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.element_attribute import ElementAttribute

        d = src_dict.copy()
        name = d.pop("name")

        id = d.pop("id", UNSET)

        creator_user_id = d.pop("creator_user_id", UNSET)

        description = d.pop("description", UNSET)

        _created_at = d.pop("created_at", UNSET)
        created_at: Union[Unset, datetime.datetime]
        if isinstance(_created_at, Unset):
            created_at = UNSET
        else:
            created_at = isoparse(_created_at)

        def _parse_approved_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                approved_at_type_0 = isoparse(data)

                return approved_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        approved_at = _parse_approved_at(d.pop("approved_at", UNSET))

        def _parse_updated_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                updated_at_type_0 = isoparse(data)

                return updated_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        updated_at = _parse_updated_at(d.pop("updated_at", UNSET))

        def _parse_deleted_at(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                deleted_at_type_0 = isoparse(data)

                return deleted_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        deleted_at = _parse_deleted_at(d.pop("deleted_at", UNSET))

        enabled = d.pop("enabled", UNSET)

        def _parse_html_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        html_url = _parse_html_url(d.pop("html_url", UNSET))

        attributes = []
        _attributes = d.pop("attributes", UNSET)
        for attributes_item_data in _attributes or []:
            attributes_item = ElementAttribute.from_dict(attributes_item_data)

            attributes.append(attributes_item)

        element = cls(
            name=name,
            id=id,
            creator_user_id=creator_user_id,
            description=description,
            created_at=created_at,
            approved_at=approved_at,
            updated_at=updated_at,
            deleted_at=deleted_at,
            enabled=enabled,
            html_url=html_url,
            attributes=attributes,
        )

        element.additional_properties = d
        return element

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
