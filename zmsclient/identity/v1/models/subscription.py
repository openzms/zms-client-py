from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.event_filter import EventFilter


T = TypeVar("T", bound="Subscription")


@_attrs_define
class Subscription:
    """
    Attributes:
        id (str): The id of the subscription.
        filters (Union[Unset, List['EventFilter']]): A list of event filters that define the subscription.
        endpoint (Union[Unset, str]): The subscribed endpoint (e.g., `<host>:<port>`).
        endpoint_type (Union[Unset, str]): The subscribed endpoint type.
    """

    id: str
    filters: Union[Unset, List["EventFilter"]] = UNSET
    endpoint: Union[Unset, str] = UNSET
    endpoint_type: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "filters": {"put": True, "post": None},
        "endpoint": {"put": True, "post": True},
        "endpoint_type": {"put": True, "post": True},
    }

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        filters: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.filters, Unset):
            filters = []
            for filters_item_data in self.filters:
                filters_item = filters_item_data.to_dict()
                filters.append(filters_item)

        endpoint = self.endpoint

        endpoint_type = self.endpoint_type

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
            }
        )
        if filters is not UNSET:
            field_dict["filters"] = filters
        if endpoint is not UNSET:
            field_dict["endpoint"] = endpoint
        if endpoint_type is not UNSET:
            field_dict["endpoint_type"] = endpoint_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.event_filter import EventFilter

        d = src_dict.copy()
        id = d.pop("id")

        filters = []
        _filters = d.pop("filters", UNSET)
        for filters_item_data in _filters or []:
            filters_item = EventFilter.from_dict(filters_item_data)

            filters.append(filters_item)

        endpoint = d.pop("endpoint", UNSET)

        endpoint_type = d.pop("endpoint_type", UNSET)

        subscription = cls(
            id=id,
            filters=filters,
            endpoint=endpoint,
            endpoint_type=endpoint_type,
        )

        subscription.additional_properties = d
        return subscription

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
