from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.role_binding import RoleBinding


T = TypeVar("T", bound="RoleBindingList")


@_attrs_define
class RoleBindingList:
    """
    Attributes:
        role_bindings (Union[Unset, List['RoleBinding']]):
    """

    role_bindings: Union[Unset, List["RoleBinding"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {"role_bindings": {"put": None, "post": None}}

    def to_dict(self) -> Dict[str, Any]:
        role_bindings: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.role_bindings, Unset):
            role_bindings = []
            for role_bindings_item_data in self.role_bindings:
                role_bindings_item = role_bindings_item_data.to_dict()
                role_bindings.append(role_bindings_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if role_bindings is not UNSET:
            field_dict["role_bindings"] = role_bindings

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.role_binding import RoleBinding

        d = src_dict.copy()
        role_bindings = []
        _role_bindings = d.pop("role_bindings", UNSET)
        for role_bindings_item_data in _role_bindings or []:
            role_bindings_item = RoleBinding.from_dict(role_bindings_item_data)

            role_bindings.append(role_bindings_item)

        role_binding_list = cls(
            role_bindings=role_bindings,
        )

        role_binding_list.additional_properties = d
        return role_binding_list

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
