from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.any_object import AnyObject
    from ..models.element import Element
    from ..models.event_header import EventHeader
    from ..models.role import Role
    from ..models.role_binding import RoleBinding
    from ..models.service import Service
    from ..models.token import Token
    from ..models.user import User


T = TypeVar("T", bound="Event")


@_attrs_define
class Event:
    """A per-service Event data type that utilizes the generic EventHeader type and associates per-service objects.

    Attributes:
        header (EventHeader):
        object_ (Union['AnyObject', 'Element', 'Role', 'RoleBinding', 'Service', 'Token', 'User', Unset]): The object on
            which the event occurred, if any.
    """

    header: "EventHeader"
    object_: Union[
        "AnyObject", "Element", "Role", "RoleBinding", "Service", "Token", "User", Unset
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "header": {"put": None, "post": None},
        "object": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        from ..models.element import Element
        from ..models.role import Role
        from ..models.role_binding import RoleBinding
        from ..models.service import Service
        from ..models.token import Token
        from ..models.user import User

        header = self.header.to_dict()

        object_: Union[Dict[str, Any], Unset]
        if isinstance(self.object_, Unset):
            object_ = UNSET
        elif isinstance(self.object_, User):
            object_ = self.object_.to_dict()
        elif isinstance(self.object_, Element):
            object_ = self.object_.to_dict()
        elif isinstance(self.object_, Role):
            object_ = self.object_.to_dict()
        elif isinstance(self.object_, RoleBinding):
            object_ = self.object_.to_dict()
        elif isinstance(self.object_, Token):
            object_ = self.object_.to_dict()
        elif isinstance(self.object_, Service):
            object_ = self.object_.to_dict()
        else:
            object_ = self.object_.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "header": header,
            }
        )
        if object_ is not UNSET:
            field_dict["object"] = object_

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.any_object import AnyObject
        from ..models.element import Element
        from ..models.event_header import EventHeader
        from ..models.role import Role
        from ..models.role_binding import RoleBinding
        from ..models.service import Service
        from ..models.token import Token
        from ..models.user import User

        d = src_dict.copy()
        header = EventHeader.from_dict(d.pop("header"))

        def _parse_object_(
            data: object,
        ) -> Union[
            "AnyObject",
            "Element",
            "Role",
            "RoleBinding",
            "Service",
            "Token",
            "User",
            Unset,
        ]:
            if isinstance(data, Unset):
                return data
            _dval = header.code
            if str(_dval) == "1001":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_0 = User.from_dict(data)

                return object_type_0
            if str(_dval) == "1002":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_1 = Element.from_dict(data)

                return object_type_1
            if str(_dval) == "1003":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_2 = Role.from_dict(data)

                return object_type_2
            if str(_dval) == "1004":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_3 = RoleBinding.from_dict(data)

                return object_type_3
            if str(_dval) == "1005":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_4 = Token.from_dict(data)

                return object_type_4
            if str(_dval) == "1006":
                if not isinstance(data, dict):
                    raise TypeError()
                object_type_5 = Service.from_dict(data)

                return object_type_5
            if not isinstance(data, dict):
                raise TypeError()
                object_type_6 = AnyObject.from_dict(data)

            return object_type_6

        object_ = _parse_object_(d.pop("object", UNSET))

        event = cls(
            header=header,
            object_=object_,
        )

        event.additional_properties = d
        return event

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
