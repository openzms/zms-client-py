from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="CreateUser")


@_attrs_define
class CreateUser:
    """
    Attributes:
        name (str): The username.
        password (str): The user's password, for direct login, encoded in base64 (standard encoding).
        primary_email_address (str): User's primary email address.
        full_name (Union[Unset, str]): The user's full name.
        given_name (Union[Unset, str]): The user's given name.
        family_name (Union[Unset, str]): The user's family name.
        secondary_email_addresses (Union[Unset, List[str]]): List of additional email addresses.
    """

    name: str
    password: str
    primary_email_address: str
    full_name: Union[Unset, str] = UNSET
    given_name: Union[Unset, str] = UNSET
    family_name: Union[Unset, str] = UNSET
    secondary_email_addresses: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "name": {"put": None, "post": None},
        "full_name": {"put": None, "post": None},
        "given_name": {"put": None, "post": None},
        "family_name": {"put": None, "post": None},
        "password": {"put": None, "post": None},
        "primary_email_address": {"put": None, "post": None},
        "secondary_email_addresses": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        password = self.password

        primary_email_address = self.primary_email_address

        full_name = self.full_name

        given_name = self.given_name

        family_name = self.family_name

        secondary_email_addresses: Union[Unset, List[str]] = UNSET
        if not isinstance(self.secondary_email_addresses, Unset):
            secondary_email_addresses = self.secondary_email_addresses

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "password": password,
                "primary_email_address": primary_email_address,
            }
        )
        if full_name is not UNSET:
            field_dict["full_name"] = full_name
        if given_name is not UNSET:
            field_dict["given_name"] = given_name
        if family_name is not UNSET:
            field_dict["family_name"] = family_name
        if secondary_email_addresses is not UNSET:
            field_dict["secondary_email_addresses"] = secondary_email_addresses

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        password = d.pop("password")

        primary_email_address = d.pop("primary_email_address")

        full_name = d.pop("full_name", UNSET)

        given_name = d.pop("given_name", UNSET)

        family_name = d.pop("family_name", UNSET)

        secondary_email_addresses = cast(
            List[str], d.pop("secondary_email_addresses", UNSET)
        )

        create_user = cls(
            name=name,
            password=password,
            primary_email_address=primary_email_address,
            full_name=full_name,
            given_name=given_name,
            family_name=family_name,
            secondary_email_addresses=secondary_email_addresses,
        )

        create_user.additional_properties = d
        return create_user

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
