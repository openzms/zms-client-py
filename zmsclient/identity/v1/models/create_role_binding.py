from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="CreateRoleBinding")


@_attrs_define
class CreateRoleBinding:
    """
    Attributes:
        user_id (str): The user ID.
        role_id (str): The role ID.
        element_id (Union[Unset, str]): The element ID, if any.
        element_name (Union[Unset, str]): An element name to search for, e.g. if user does not know the ID.
        approved (Union[Unset, bool]): Immediate approve new RoleBinding if set `true`.  Requires caller to have Admin
            or >Operator roles in target element.
    """

    user_id: str
    role_id: str
    element_id: Union[Unset, str] = UNSET
    element_name: Union[Unset, str] = UNSET
    approved: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "user_id": {"put": None, "post": None},
        "role_id": {"put": None, "post": None},
        "element_id": {"put": None, "post": None},
        "element_name": {"put": None, "post": None},
        "approved": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        user_id = self.user_id

        role_id = self.role_id

        element_id = self.element_id

        element_name = self.element_name

        approved = self.approved

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "user_id": user_id,
                "role_id": role_id,
            }
        )
        if element_id is not UNSET:
            field_dict["element_id"] = element_id
        if element_name is not UNSET:
            field_dict["element_name"] = element_name
        if approved is not UNSET:
            field_dict["approved"] = approved

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        user_id = d.pop("user_id")

        role_id = d.pop("role_id")

        element_id = d.pop("element_id", UNSET)

        element_name = d.pop("element_name", UNSET)

        approved = d.pop("approved", UNSET)

        create_role_binding = cls(
            user_id=user_id,
            role_id=role_id,
            element_id=element_id,
            element_name=element_name,
            approved=approved,
        )

        create_role_binding.additional_properties = d
        return create_role_binding

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
