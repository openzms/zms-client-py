from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="UserEmailAddress")


@_attrs_define
class UserEmailAddress:
    """
    Attributes:
        email_address (str): Email address.
        id (Union[Unset, str]): The id of the email address.
        user_id (Union[Unset, str]): The user_id of the email address.
    """

    email_address: str
    id: Union[Unset, str] = UNSET
    user_id: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)
    _immutable_on = {
        "id": {"put": True, "post": True},
        "user_id": {"put": True, "post": True},
        "email_address": {"put": None, "post": None},
    }

    def to_dict(self) -> Dict[str, Any]:
        email_address = self.email_address

        id = self.id

        user_id = self.user_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "email_address": email_address,
            }
        )
        if id is not UNSET:
            field_dict["id"] = id
        if user_id is not UNSET:
            field_dict["user_id"] = user_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        email_address = d.pop("email_address")

        id = d.pop("id", UNSET)

        user_id = d.pop("user_id", UNSET)

        user_email_address = cls(
            email_address=email_address,
            id=id,
            user_id=user_id,
        )

        user_email_address.additional_properties = d
        return user_email_address

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
