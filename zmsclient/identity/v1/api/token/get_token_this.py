from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.token import Token
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/tokens/this",
    }

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, Token]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Token.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, Token]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Token]]:
    """Display calling token's (X-Api-Token header value) metadata.

    Args:
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Token]]
    """

    kwargs = _get_kwargs(
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Token]]:
    """Display calling token's (X-Api-Token header value) metadata.

    Args:
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Token]
    """

    return sync_detailed(
        client=client,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Token]]:
    """Display calling token's (X-Api-Token header value) metadata.

    Args:
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Token]]
    """

    kwargs = _get_kwargs(
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Token]]:
    """Display calling token's (X-Api-Token header value) metadata.

    Args:
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Token]
    """

    return (
        await asyncio_detailed(
            client=client,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
        )
    ).parsed
