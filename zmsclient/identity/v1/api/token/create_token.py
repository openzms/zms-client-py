from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.create_token import CreateToken
from ...models.error import Error
from ...models.token import Token
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    body: CreateToken,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    if not isinstance(x_api_token, Unset):
        headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    if not isinstance(authorization, Unset):
        headers["Authorization"] = authorization

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": "/tokens",
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, Token]]:
    if response.status_code == HTTPStatus.CREATED:
        response_201 = Token.from_dict(response.json())

        return response_201
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR:
        response_500 = Error.from_dict(response.json())

        return response_500
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, Token]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: CreateToken,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Token]]:
    """Create a new API token.  You can created a token in three primary ways: by authenticating via
    username/password, an existing token, or via SSO credentials from a trusted identity provider.  To
    signal this choice, you must specify a `method` (one of `password`, `token`, `idp`).  If you specify
    `password`, you must set the `credential` field to a struct with two fields, `username` (a valid
    OpenZMS username) and `password` (the password associated with that OpenZMS user).  `password` must
    be base64-encoded.  If you specify `token`, you may send the source token either in the X-Api-Token
    header as for other API invocations, *or* in the `credential` field of the request as a struct with
    a single `token` field set to the value of your existing API token.  (We do not document the SSO/IdP
    option here; that is useful only if you are building a trusted front end.)  Three additional options
    control the scope of the generated token, but they can only reduce the scope authorized by the
    source authentication method (e.g. password or source token).  If you specify `method` as
    `password`, and *do not set* a value for the `role_binding_ids` field, your token will be associated
    will all the approved, undeleted roles associated with your account.  If you set `admin_if_bound` to
    `true`, your admin role will be included in the new token, if your source auth method is includes an
    admin role binding.  If `admin_if_bound` is `null` or `false`, your new token will not include your
    admin role even if your source auth method includes one.  (The goal of `admin_if_bound` is to force
    callers to explicitly ask for admin privileges.  If you do specify `role_binding_ids`, it must be a
    subset of the `role_binding_ids` to which your account is entitled.  If you specify `method` as
    `token`, and *do not set* a value for `role_binding_ids` field, your token will be associated with
    all the roles included with the source token you have provider, modulo the admin role and setting of
    `admin_if_bound`, just as for the `password` case.  Finally, for `token_type`, all readers of this
    document should simply set `null` or `pat` (personal access token).`

    Args:
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):
        body (CreateToken):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Token]]
    """

    kwargs = _get_kwargs(
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: Union[AuthenticatedClient, Client],
    body: CreateToken,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Token]]:
    """Create a new API token.  You can created a token in three primary ways: by authenticating via
    username/password, an existing token, or via SSO credentials from a trusted identity provider.  To
    signal this choice, you must specify a `method` (one of `password`, `token`, `idp`).  If you specify
    `password`, you must set the `credential` field to a struct with two fields, `username` (a valid
    OpenZMS username) and `password` (the password associated with that OpenZMS user).  `password` must
    be base64-encoded.  If you specify `token`, you may send the source token either in the X-Api-Token
    header as for other API invocations, *or* in the `credential` field of the request as a struct with
    a single `token` field set to the value of your existing API token.  (We do not document the SSO/IdP
    option here; that is useful only if you are building a trusted front end.)  Three additional options
    control the scope of the generated token, but they can only reduce the scope authorized by the
    source authentication method (e.g. password or source token).  If you specify `method` as
    `password`, and *do not set* a value for the `role_binding_ids` field, your token will be associated
    will all the approved, undeleted roles associated with your account.  If you set `admin_if_bound` to
    `true`, your admin role will be included in the new token, if your source auth method is includes an
    admin role binding.  If `admin_if_bound` is `null` or `false`, your new token will not include your
    admin role even if your source auth method includes one.  (The goal of `admin_if_bound` is to force
    callers to explicitly ask for admin privileges.  If you do specify `role_binding_ids`, it must be a
    subset of the `role_binding_ids` to which your account is entitled.  If you specify `method` as
    `token`, and *do not set* a value for `role_binding_ids` field, your token will be associated with
    all the roles included with the source token you have provider, modulo the admin role and setting of
    `admin_if_bound`, just as for the `password` case.  Finally, for `token_type`, all readers of this
    document should simply set `null` or `pat` (personal access token).`

    Args:
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):
        body (CreateToken):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Token]
    """

    return sync_detailed(
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    ).parsed


async def asyncio_detailed(
    *,
    client: Union[AuthenticatedClient, Client],
    body: CreateToken,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Token]]:
    """Create a new API token.  You can created a token in three primary ways: by authenticating via
    username/password, an existing token, or via SSO credentials from a trusted identity provider.  To
    signal this choice, you must specify a `method` (one of `password`, `token`, `idp`).  If you specify
    `password`, you must set the `credential` field to a struct with two fields, `username` (a valid
    OpenZMS username) and `password` (the password associated with that OpenZMS user).  `password` must
    be base64-encoded.  If you specify `token`, you may send the source token either in the X-Api-Token
    header as for other API invocations, *or* in the `credential` field of the request as a struct with
    a single `token` field set to the value of your existing API token.  (We do not document the SSO/IdP
    option here; that is useful only if you are building a trusted front end.)  Three additional options
    control the scope of the generated token, but they can only reduce the scope authorized by the
    source authentication method (e.g. password or source token).  If you specify `method` as
    `password`, and *do not set* a value for the `role_binding_ids` field, your token will be associated
    will all the approved, undeleted roles associated with your account.  If you set `admin_if_bound` to
    `true`, your admin role will be included in the new token, if your source auth method is includes an
    admin role binding.  If `admin_if_bound` is `null` or `false`, your new token will not include your
    admin role even if your source auth method includes one.  (The goal of `admin_if_bound` is to force
    callers to explicitly ask for admin privileges.  If you do specify `role_binding_ids`, it must be a
    subset of the `role_binding_ids` to which your account is entitled.  If you specify `method` as
    `token`, and *do not set* a value for `role_binding_ids` field, your token will be associated with
    all the roles included with the source token you have provider, modulo the admin role and setting of
    `admin_if_bound`, just as for the `password` case.  Finally, for `token_type`, all readers of this
    document should simply set `null` or `pat` (personal access token).`

    Args:
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):
        body (CreateToken):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Token]]
    """

    kwargs = _get_kwargs(
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: Union[AuthenticatedClient, Client],
    body: CreateToken,
    x_api_token: Union[Unset, str] = UNSET,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Token]]:
    """Create a new API token.  You can created a token in three primary ways: by authenticating via
    username/password, an existing token, or via SSO credentials from a trusted identity provider.  To
    signal this choice, you must specify a `method` (one of `password`, `token`, `idp`).  If you specify
    `password`, you must set the `credential` field to a struct with two fields, `username` (a valid
    OpenZMS username) and `password` (the password associated with that OpenZMS user).  `password` must
    be base64-encoded.  If you specify `token`, you may send the source token either in the X-Api-Token
    header as for other API invocations, *or* in the `credential` field of the request as a struct with
    a single `token` field set to the value of your existing API token.  (We do not document the SSO/IdP
    option here; that is useful only if you are building a trusted front end.)  Three additional options
    control the scope of the generated token, but they can only reduce the scope authorized by the
    source authentication method (e.g. password or source token).  If you specify `method` as
    `password`, and *do not set* a value for the `role_binding_ids` field, your token will be associated
    will all the approved, undeleted roles associated with your account.  If you set `admin_if_bound` to
    `true`, your admin role will be included in the new token, if your source auth method is includes an
    admin role binding.  If `admin_if_bound` is `null` or `false`, your new token will not include your
    admin role even if your source auth method includes one.  (The goal of `admin_if_bound` is to force
    callers to explicitly ask for admin privileges.  If you do specify `role_binding_ids`, it must be a
    subset of the `role_binding_ids` to which your account is entitled.  If you specify `method` as
    `token`, and *do not set* a value for `role_binding_ids` field, your token will be associated with
    all the roles included with the source token you have provider, modulo the admin role and setting of
    `admin_if_bound`, just as for the `password` case.  Finally, for `token_type`, all readers of this
    document should simply set `null` or `pat` (personal access token).`

    Args:
        x_api_token (Union[Unset, str]):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):
        body (CreateToken):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Token]
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
            authorization=authorization,
        )
    ).parsed
