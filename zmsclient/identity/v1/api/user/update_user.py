from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.user import User
from ...types import UNSET, Response, Unset


def _get_kwargs(
    user_id: str,
    *,
    body: User,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    if not isinstance(authorization, Unset):
        headers["Authorization"] = authorization

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/users/{user_id}".format(
            user_id=user_id,
        ),
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, User]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = User.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, User]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    user_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: User,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Response[Union[Error, User]]:
    """Update user details.

    Args:
        user_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):  Example: Basic Zm9vOmJhcgo=.
        body (User):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, User]]
    """

    kwargs = _get_kwargs(
        user_id=user_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    user_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: User,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, User]]:
    """Update user details.

    Args:
        user_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):  Example: Basic Zm9vOmJhcgo=.
        body (User):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, User]
    """

    return sync_detailed(
        user_id=user_id,
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    ).parsed


async def asyncio_detailed(
    user_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: User,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Response[Union[Error, User]]:
    """Update user details.

    Args:
        user_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):  Example: Basic Zm9vOmJhcgo=.
        body (User):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, User]]
    """

    kwargs = _get_kwargs(
        user_id=user_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        authorization=authorization,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    user_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: User,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    authorization: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, User]]:
    """Update user details.

    Args:
        user_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        authorization (Union[Unset, str]):  Example: Basic Zm9vOmJhcgo=.
        body (User):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, User]
    """

    return (
        await asyncio_detailed(
            user_id=user_id,
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
            authorization=authorization,
        )
    ).parsed
