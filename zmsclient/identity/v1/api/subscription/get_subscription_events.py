from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.event import Event
from ...types import UNSET, Response, Unset


def _get_kwargs(
    subscription_id: str,
    *,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    connection: str,
    upgrade: str,
    sec_web_socket_version: str,
    sec_web_socket_key: str,
    x_delete_on_close: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    headers["Connection"] = connection

    headers["Upgrade"] = upgrade

    headers["Sec-WebSocket-Version"] = sec_web_socket_version

    headers["Sec-WebSocket-Key"] = sec_web_socket_key

    if not isinstance(x_delete_on_close, Unset):
        headers["X-Delete-On-Close"] = x_delete_on_close

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/subscriptions/{subscription_id}/events".format(
            subscription_id=subscription_id,
        ),
    }

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, Event]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = Event.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR:
        response_500 = Error.from_dict(response.json())

        return response_500
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, Event]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    subscription_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    connection: str,
    upgrade: str,
    sec_web_socket_version: str,
    sec_web_socket_key: str,
    x_delete_on_close: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Event]]:
    """Stream events corresponding to a subscription over a WebSocket upgrade of the invoking HTTP
    connection.  Only one event stream may be associated with each subscription.  Matching events are
    returned as JSON-formatted `Event` objects, as described in this schema.

    Args:
        subscription_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        connection (str):  Example: Upgrade.
        upgrade (str):  Example: websocket.
        sec_web_socket_version (str):  Example: 13.
        sec_web_socket_key (str):  Example: MDAwMDAwMDAwMDAwMDAwMA==.
        x_delete_on_close (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Event]]
    """

    kwargs = _get_kwargs(
        subscription_id=subscription_id,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        connection=connection,
        upgrade=upgrade,
        sec_web_socket_version=sec_web_socket_version,
        sec_web_socket_key=sec_web_socket_key,
        x_delete_on_close=x_delete_on_close,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    subscription_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    connection: str,
    upgrade: str,
    sec_web_socket_version: str,
    sec_web_socket_key: str,
    x_delete_on_close: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Event]]:
    """Stream events corresponding to a subscription over a WebSocket upgrade of the invoking HTTP
    connection.  Only one event stream may be associated with each subscription.  Matching events are
    returned as JSON-formatted `Event` objects, as described in this schema.

    Args:
        subscription_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        connection (str):  Example: Upgrade.
        upgrade (str):  Example: websocket.
        sec_web_socket_version (str):  Example: 13.
        sec_web_socket_key (str):  Example: MDAwMDAwMDAwMDAwMDAwMA==.
        x_delete_on_close (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Event]
    """

    return sync_detailed(
        subscription_id=subscription_id,
        client=client,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        connection=connection,
        upgrade=upgrade,
        sec_web_socket_version=sec_web_socket_version,
        sec_web_socket_key=sec_web_socket_key,
        x_delete_on_close=x_delete_on_close,
    ).parsed


async def asyncio_detailed(
    subscription_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    connection: str,
    upgrade: str,
    sec_web_socket_version: str,
    sec_web_socket_key: str,
    x_delete_on_close: Union[Unset, str] = UNSET,
) -> Response[Union[Error, Event]]:
    """Stream events corresponding to a subscription over a WebSocket upgrade of the invoking HTTP
    connection.  Only one event stream may be associated with each subscription.  Matching events are
    returned as JSON-formatted `Event` objects, as described in this schema.

    Args:
        subscription_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        connection (str):  Example: Upgrade.
        upgrade (str):  Example: websocket.
        sec_web_socket_version (str):  Example: 13.
        sec_web_socket_key (str):  Example: MDAwMDAwMDAwMDAwMDAwMA==.
        x_delete_on_close (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, Event]]
    """

    kwargs = _get_kwargs(
        subscription_id=subscription_id,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
        connection=connection,
        upgrade=upgrade,
        sec_web_socket_version=sec_web_socket_version,
        sec_web_socket_key=sec_web_socket_key,
        x_delete_on_close=x_delete_on_close,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    subscription_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
    connection: str,
    upgrade: str,
    sec_web_socket_version: str,
    sec_web_socket_key: str,
    x_delete_on_close: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, Event]]:
    """Stream events corresponding to a subscription over a WebSocket upgrade of the invoking HTTP
    connection.  Only one event stream may be associated with each subscription.  Matching events are
    returned as JSON-formatted `Event` objects, as described in this schema.

    Args:
        subscription_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        connection (str):  Example: Upgrade.
        upgrade (str):  Example: websocket.
        sec_web_socket_version (str):  Example: 13.
        sec_web_socket_key (str):  Example: MDAwMDAwMDAwMDAwMDAwMA==.
        x_delete_on_close (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, Event]
    """

    return (
        await asyncio_detailed(
            subscription_id=subscription_id,
            client=client,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
            connection=connection,
            upgrade=upgrade,
            sec_web_socket_version=sec_web_socket_version,
            sec_web_socket_key=sec_web_socket_key,
            x_delete_on_close=x_delete_on_close,
        )
    ).parsed
