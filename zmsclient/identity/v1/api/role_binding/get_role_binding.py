from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.role_binding import RoleBinding
from ...types import Response


def _get_kwargs(
    rolebinding_id: str,
    *,
    x_api_token: str,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/rolebindings/{rolebinding_id}".format(
            rolebinding_id=rolebinding_id,
        ),
    }

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, RoleBinding]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = RoleBinding.from_dict(response.json())

        return response_200
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, RoleBinding]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
) -> Response[Union[Error, RoleBinding]]:
    """Retrieve a specific rolebinding's metadata.

    Args:
        rolebinding_id (str):
        x_api_token (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RoleBinding]]
    """

    kwargs = _get_kwargs(
        rolebinding_id=rolebinding_id,
        x_api_token=x_api_token,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
) -> Optional[Union[Error, RoleBinding]]:
    """Retrieve a specific rolebinding's metadata.

    Args:
        rolebinding_id (str):
        x_api_token (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RoleBinding]
    """

    return sync_detailed(
        rolebinding_id=rolebinding_id,
        client=client,
        x_api_token=x_api_token,
    ).parsed


async def asyncio_detailed(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
) -> Response[Union[Error, RoleBinding]]:
    """Retrieve a specific rolebinding's metadata.

    Args:
        rolebinding_id (str):
        x_api_token (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RoleBinding]]
    """

    kwargs = _get_kwargs(
        rolebinding_id=rolebinding_id,
        x_api_token=x_api_token,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    x_api_token: str,
) -> Optional[Union[Error, RoleBinding]]:
    """Retrieve a specific rolebinding's metadata.

    Args:
        rolebinding_id (str):
        x_api_token (str):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RoleBinding]
    """

    return (
        await asyncio_detailed(
            rolebinding_id=rolebinding_id,
            client=client,
            x_api_token=x_api_token,
        )
    ).parsed
