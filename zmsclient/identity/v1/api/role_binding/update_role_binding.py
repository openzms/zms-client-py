from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.error import Error
from ...models.role_binding import RoleBinding
from ...types import UNSET, Response, Unset


def _get_kwargs(
    rolebinding_id: str,
    *,
    body: RoleBinding,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}
    headers["X-Api-Token"] = x_api_token

    if not isinstance(x_api_elaborate, Unset):
        headers["X-Api-Elaborate"] = x_api_elaborate

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/rolebindings/{rolebinding_id}".format(
            rolebinding_id=rolebinding_id,
        ),
    }

    _body = body.to_dict()

    _kwargs["json"] = _body
    headers["Content-Type"] = "application/json"

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[Union[Error, RoleBinding]]:
    if response.status_code == HTTPStatus.CREATED:
        response_201 = RoleBinding.from_dict(response.json())

        return response_201
    if response.status_code == HTTPStatus.BAD_REQUEST:
        response_400 = Error.from_dict(response.json())

        return response_400
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        response_401 = Error.from_dict(response.json())

        return response_401
    if response.status_code == HTTPStatus.FORBIDDEN:
        response_403 = Error.from_dict(response.json())

        return response_403
    if response.status_code == HTTPStatus.NOT_FOUND:
        response_404 = Error.from_dict(response.json())

        return response_404
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[Union[Error, RoleBinding]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RoleBinding,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, RoleBinding]]:
    """Edit a new rolebinding: change associated `role_id` before approval, or approve the rolebinding by
    setting `approved_at`.  Admins can change and approve any rolebinding.  Managers or owners in the
    associated `element_id` may approve or change rolebindings whose role is less than or equal to their
    role in the `element_id`.

    Args:
        rolebinding_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RoleBinding):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RoleBinding]]
    """

    kwargs = _get_kwargs(
        rolebinding_id=rolebinding_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RoleBinding,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, RoleBinding]]:
    """Edit a new rolebinding: change associated `role_id` before approval, or approve the rolebinding by
    setting `approved_at`.  Admins can change and approve any rolebinding.  Managers or owners in the
    associated `element_id` may approve or change rolebindings whose role is less than or equal to their
    role in the `element_id`.

    Args:
        rolebinding_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RoleBinding):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RoleBinding]
    """

    return sync_detailed(
        rolebinding_id=rolebinding_id,
        client=client,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    ).parsed


async def asyncio_detailed(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RoleBinding,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Response[Union[Error, RoleBinding]]:
    """Edit a new rolebinding: change associated `role_id` before approval, or approve the rolebinding by
    setting `approved_at`.  Admins can change and approve any rolebinding.  Managers or owners in the
    associated `element_id` may approve or change rolebindings whose role is less than or equal to their
    role in the `element_id`.

    Args:
        rolebinding_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RoleBinding):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[Union[Error, RoleBinding]]
    """

    kwargs = _get_kwargs(
        rolebinding_id=rolebinding_id,
        body=body,
        x_api_token=x_api_token,
        x_api_elaborate=x_api_elaborate,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    rolebinding_id: str,
    *,
    client: Union[AuthenticatedClient, Client],
    body: RoleBinding,
    x_api_token: str,
    x_api_elaborate: Union[Unset, str] = UNSET,
) -> Optional[Union[Error, RoleBinding]]:
    """Edit a new rolebinding: change associated `role_id` before approval, or approve the rolebinding by
    setting `approved_at`.  Admins can change and approve any rolebinding.  Managers or owners in the
    associated `element_id` may approve or change rolebindings whose role is less than or equal to their
    role in the `element_id`.

    Args:
        rolebinding_id (str):
        x_api_token (str):
        x_api_elaborate (Union[Unset, str]):
        body (RoleBinding):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Union[Error, RoleBinding]
    """

    return (
        await asyncio_detailed(
            rolebinding_id=rolebinding_id,
            client=client,
            body=body,
            x_api_token=x_api_token,
            x_api_elaborate=x_api_elaborate,
        )
    ).parsed
