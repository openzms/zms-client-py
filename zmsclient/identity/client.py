
from .v1.client import AuthenticatedClient
from .v1.client_api_mixin import ClientApiMixin

class ZmsIdentityClient(AuthenticatedClient, ClientApiMixin):

    def _call_api_func(self, func, *args, **kwargs):
        if 'x_api_elaborate' not in kwargs and 'elaborate' in kwargs:
            kwargs['x_api_elaborate'] = str(kwargs['elaborate'])
            del kwargs['elaborate']
        kwargs['x_api_token'] = self.token
        return ClientApiMixin._call_api_func(self, func, *args, **kwargs)

    def __init__(self, base_url: str, token: str, **kwargs):
        super(ZmsIdentityClient, self).__init__(base_url=base_url, token=token, prefix="", auth_header_name="X-Api-Token", **kwargs)
