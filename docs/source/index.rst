.. zmsclient documentation master file, created by
   sphinx-quickstart on Thu Oct 24 16:33:01 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

zmsclient documentation (|version|)
===================================

The `OpenZMS software <https://gitlab.flux.utah.edu/openzms>`_ is a
prototype automatic spectrum-sharing and -management system for radio
dynamic zones.

The ``zmsclient`` package provides a OpenZMS Python 3 API client library
based on API bindings (models and API function wrappers) automatically
generated from each OpenZMS service's ``openapi`` specification.  Bindings
are generated using `our fork
<https://gitlab.flux.utah.edu/openzms/openapi-python-client>`_ of
``openapi-python-client``, which has additional support for some of the
``openapi`` extension attributes we provide to help generators create better
code.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   autoapi/index.html

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
