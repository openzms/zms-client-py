# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
project = 'zmsclient'
copyright = '2024-%Y, University of Utah'
author = 'David M. Johnson'

from setuptools_scm import get_version
release = version = get_version("../..")

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autodoc.typehints",
    'sphinx.ext.napoleon',
    "sphinx_autodoc_typehints",
    'autoapi.extension',
    'sphinx.ext.intersphinx',
]

templates_path = ['_templates']
exclude_patterns = [
    '*test*',
    '*cli/*',
]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
html_theme_options = {
    'description': 'OpenZMS Python API client library and CLI tools',
    'font_size': '14px',
    'code_font_size': '14px',
    'caption_font_size': '14px',
}
html_logo = 'https://openzms.net/images/dark-name-icon.svg'
html_favicon = 'https://openzms.net/favicon.ico'

autodoc_typehints = 'description'
always_document_param_types = True
#autoapi_python_class_content = "class"

autoapi_dirs = [
    "../../zmsclient",
]
autoapi_ignore = [
    '*test*',
    '*cli/*',
    #'*__main__*',
    #'*__init__*'
]
intersphinx_mapping = {
    # https://github.com/encode/httpx/discussions/3091
    #'httpx': ('https://www.python-httpx.org/api/', None),
    'python': ('https://docs.python.org/3/', None),
    'click': ('https://click.palletsprojects.com/en/stable/', None),
    'typing_extensions': ('https://typing-extensions.readthedocs.io/en/latest/', None),
}

