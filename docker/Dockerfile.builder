FROM python:3.12-slim-bookworm

RUN apt-get update -y \
  && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-suggests \
      git ca-certificates \
  && apt-get clean all \
  && rm -rfv /var/lib/apt/lists/*

COPY . /tmp/zms-client-py/

#
# No way to install only requirements with pyproject.yml .
#
RUN cd /tmp/zms-client-py \
  && pip install .'[cli]' \
  && pip install .'[dev]' \
  && pip uninstall -y zms-client
